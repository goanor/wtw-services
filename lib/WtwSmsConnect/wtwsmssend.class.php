<?php

class WtwSmsSend {

  private static $gatewaysSms = array(

				      '2470' => 'https://mobil.inatur.no/sendsms/sendsms_mt.php',
				      '41716150' => 'https://mobil.inatur.no/sendsms/sendsms_mt.php'
				      );

  private static $useragent = 'Mozilla/5.0 (WtwSmsConnect; WTW AS)';

  public static function sendSms($gateway, $code, $apikey, $originator,
				 $cc, $to, $text, 
				 $tariff, $contentDescription = '',
				 $textDescription = '') {
    $url = "";
    $gOk = false;
    foreach (self::$gatewaysSms as $gId => $gUrl) {
      if ($gateway == $gId) {
	$gOk = true;
	$url = $gUrl;
	break;
      }
    }
    if (!$gOk) {
      throw new Exception("Invalid gateway ($gateway)");
    }


    $header = array();
    $header[] = "Connection: close";

    $url .= '?type=text';
    $url .= '&tocc='.$cc;
    $url .= '&tonum='.$to;
    $url .= '&fromnum='.$gateway;
    $url .= '&originator='.urlencode($originator);
    $url .= '&txt='.urlencode($text);
    $url .= '&tariff='.$tariff;
    $url .= '&cw='.$code;
    $url .= '&apikey='.urlencode($apikey);
    if ($contentDescription != '') {
      $url .= '&cd='.$contentDescription;
    }
    if ($textDescription != '') {
      $url .= '&td='.$textDescription;
    }

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_USERAGENT, self::$useragent);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

    $result = curl_exec($ch);
    $result = trim($result);
    $info = curl_getinfo($ch);
    curl_close($ch);

    if($info['http_code'] != 200) {
      throw new Exception('Http code is ' . $info['http_code'] . "(url: $url)");
    }

    if ($result == '') {
      throw new Exception("Received empty response (url: $url)");
    }
        
    return $result;
  }

}

?>