<?php

class Fine
{

  public $title = '';
  public $documentType = '';
  public $credit_debit = '';
  public $date = '';
  public $sum = '';
  public $description = '';

  function __construct($title,
		       $imprint,
		       $credit_debit,
		       $date,
		       $description,
		       $sum)
  {
    $this->title = $title;
    $this->documentType = $imprint;
    $this->credit_debit = $credit_debit;
    $this->date = strtotime($date);
    $this->description = AXS_Utils::utf8_smart_decode($description);
    $this->sum = $sum;
  }
}



?>