<?php

class FindResult {

	public $set_number = '';
	public $no_records = 0;
	public $no_entries = 0;
	public $session_id = '';
	public $request = null;

	function __construct($set_number, $no_records, $no_entries, $session_id, $findrequest) {
		$this->set_number = $set_number;
		$this->no_records = $no_records;
		$this->no_entries = $no_entries;
		$this->session_id = $session_id;
		$this->request = $findrequest;	
	}



}

?>