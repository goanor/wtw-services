<?php
if (!defined('CONFIG_LOADED')) die('Access forbidden');

require_once(CLASS_PATH . 'AXS_Utils.class.php');
require_once(CLASS_PATH . 'Log.class.php');
require_once(CLASS_PATH . 'InnsynElement.class.php');


/*
1) Hent Innsyn data basert p� tittelnummer

   Alle titler i databasen kan identifiseres med en n�kkel som
   kalles for tittelnummer. Dette er et nummer med 9 siffer. I
   noen tilfeller vil dette v�re lettere tilgjengelig enn ISBN.

   Vi har et skript som tar i mot en tittelnummer og viser
   Innsyn data knyttet til tittelen:

   http://aleph.stavanger.kommune.no/cgi-bin/innsyn/innsyn.pl?titnr=000515019&bibbase=nor01

2) innsyn2.pl som jeg skrev om i g�r har muligheter for � ta i mot
   mer enn en ISBN om gangen. ISBN skilles med ;-tegnet, f.eks:

   http://aleph.stavanger.kommune.no/cgi-bin/innsyn/innsyn2.pl?isbn=82-05-32603-7;82-421-1382-3

   Vi pleier � sende 10 ISBN om gangen fra trefflisten i ALEPH. Jeg er
   usikker hvis det finnes noen grense for hvor mange ISBN som kan sendes.
*/
class Bibits
{
  
  private static $url = "http://aleph.stavanger.kommune.no/cgi-bin/innsyn/innsyn2.pl?isbn=";
  
  /*
   
  $isbn int or array of ints
  */
  public static function innsyn($isbn)
  {	
    if (empty($isbn)) {
      Log::error("ISBN is empty", __LINE__, __FILE__, __METHOD__, __CLASS__);
      return false;
    }
    // Validate ISBN
    $isbn_string = '';
    if (is_array($isbn)) {
      $isbn_string = implode(';', $isbn);
    }
    else {
      $isbn_string = $isbn;
    }
    
    $action = self::$url . $isbn_string;
    
    $xml_response = AXS_Utils::getXML($action);
    
    $xml = simplexml_load_string($xml_response);
    
    $list = array();
    
    foreach ($xml->RECORD as $r) {
      //print_r($r);
      // Return records as objects in a assosiative array with ISDN number as key
      if (!empty($r['ID'])) {
	$id = (string) $r['ID'];
	$list[$id] = new InnsynElement((double) $id,
				       (string) $r->THUMB_COVER_PICTURE,
				       (string) $r->SMALL_COVER_PICTURE,
				       (string) $r->LARGE_COVER_PICTURE);
      }
    }
    return $list;
  }
  
}

?>