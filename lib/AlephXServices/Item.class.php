<?php


class Item
{
  public $rec_key = '';
  public $barcode = '';
  public $sub_library = '';
  public $collection = '';
  public $item_status = '';
  public $library = '';
  public $on_hold = '';
  public $requested = '';
  public $expected = '';
  public $loan_status = '';
  public $loan_in_transit = '';
  public $loan_due = '';

  function __construct($rec_key, $barcode,
		       $sub_library, $collection,
		       $item_status, $library, $on_hold,
		       $requested, $expected, $loan_status,			   
		       $loan_in_transit, $loan_due_date, $loan_due_hour)
  {
    $this->rec_key = (string) $rec_key;
    $this->barcode = (string) $barcode;
    $this->sub_library = (string) $sub_library;
    $this->collection = (string) $collection;
    $this->item_status = (string) $item_status;
    $this->library = (string) $library;
    $this->on_hold = (string) $on_hold;
    $this->requested = (string) $requested;
    $this->expected = (string) $expected;
    $this->loan_status = (string) $loan_status;
    $this->loan_due = (string) strtotime("{$loan_due_date}{$loan_due_hour}");
  }

}


?>