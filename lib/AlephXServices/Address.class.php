<?php

class Address
{
  public $address0 = '';
  public $address1 = '';
  public $address2 = '';  
  public $address3 = '';
  public $email = '';
  public $telephone = '';
  public $mobilephone = '';      


  function __construct($address0, $address1, $address2,
					   $address3, $email, $telephone, $mobilephone)
  {
    $this->address0 = AXS_Utils::utf8_smart_decode((string) $address0);
    $this->address1 = AXS_Utils::utf8_smart_decode((string) $address1);
    $this->address2 = AXS_Utils::utf8_smart_decode((string) $address2);
    $this->address3 = AXS_Utils::utf8_smart_decode((string) $address3);
    $this->email = AXS_Utils::utf8_smart_decode((string) $email);	
    $this->telephone = (string) $telephone;
    $this->mobilephone = (string) $mobilephone;
  }

}


?>