<?php


class DocumentElement
{
  public $title = '';
  public $author = '';
  public $publishingyear = '';
  public $documentNumber = 0;
  public $documentType = '';
  public $isbn = 0;

  function __construct($docnumber, $title,
		       $author, $publishingyear,
		       $documentType, $isbn)
  {
    $this->documentNumber = (int) $docnumber;
    $this->title = (string) $title;
    $this->author = (string) $author;
    $this->publishingyear = (string) $publishingyear;
    $this->documentType = AXS_Utils::cleanDocumentType((string) $documentType);
	// $this->isbn = $isbn;
    $this->isbn = preg_replace('/[^\d]/','', $isbn);
	
  }

}


?>