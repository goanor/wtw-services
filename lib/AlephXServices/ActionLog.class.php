<?php

require_once(CLASS_PATH . 'DBAction.class.php');

class ActionLog extends DBAction {

	private $dbTable = 'action_log';
	
	private function log($document_id, $action = 'RENEW')
	{
		if (!$this->dbConnect()) return false;
	
		$action = strtoupper($action);
		if (!in_array($action, array('RENEW', 'HOLD', 'HOLDDELETE'))) return false; 
		if (empty($document_id)) return false;
		$document_id = $this->escapeString($document_id);
		
		$session_id = session_id();
		if (empty($session_id)) return false;
		$session_id = $this->escapeString($session_id);
		
		$cookie = 'NULL';
		if (!empty($_COOKIE['PocketNews']) && AXS_Utils::isMD5($_COOKIE['PocketNews'])) {
			$cookie = "'" . $this->escapeString($_COOKIE['PocketNews']) . "'";
		}
	
		$q = "INSERT INTO {$this->dbTable} " .
			"(action_time, session_id, cookie, document_id, action) " .
			" VALUES (NOW(), '$session_id', $cookie, '$document_id', '$action')";

		$this->executeQuery($q);
		
	}

	public function renew($document_id)
	{
		$this->log($document_id,'RENEW');	
	}
	
	public function holdRequest($document_id)
	{
		$this->log($document_id, 'HOLD');	
	}
	
	public function cancelHoldRequest($document_id)
	{
		$this->log($document_id, 'HOLDDELETE');	
	}		

	
}
?>