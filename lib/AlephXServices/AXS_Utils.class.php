<?php

class AXS_Utils
{
  
  public static function getXML($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.7) Gecko/20070914 Firefox/2.0.0.7");
    curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
    $data = curl_exec($ch);
    curl_close($ch);
    
    return $data;
  }
  
  public static function getXMLFromCache($url, $timeout = 3600)
  {
	if ($xml = self::getFromCache($url, $timeout)) {
		return $xml;
	}
  
	$xml = self::getXML($url);
    if (!empty($xml)) {
	  self::storeToCache($url, $xml);
	  return $xml;
    }
	
	return false;
  }
  
  private static function getFromCache($url, $timeoutInSecs = 3600)
  { 
	$file = self::cacheFileName($url);
    if (is_file($file) && (filemtime($file) + $timeoutInSecs) >= time()) {
      // echo "Read cache";
      return file_get_contents($file);
    }
    return false;
  }
  
  private static function storeToCache($url, $data)
  {
     // echo "Write cache";
     file_put_contents(self::cacheFileName($url), $data);
  }  
  
  public static function cacheFileName($url)
  {
	return XML_CACHE_DIR . strtr(base64_encode($url), '+/', '-_');
  }
  
  public static function isUTF8 ($chaine) {
    // From http://w3.org/International/questions/qa-forms-utf-8.html
    return preg_match('%^(?:
	         [\x09\x0A\x0D\x20-\x7E]            # ASCII
	       | [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte
	       |  \xE0[\xA0-\xBF][\x80-\xBF]        # excluding overlongs
	       | [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
	       |  \xED[\x80-\x9F][\x80-\xBF]        # excluding surrogates
	       |  \xF0[\x90-\xBF][\x80-\xBF]{2}     # planes 1-3
	       | [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
	       |  \xF4[\x80-\x8F][\x80-\xBF]{2}     # plane 16
	   )*$%xs', $chaine);
  }	
  
  public static function utf8_smart_encode($string)
  {
    if (!self::isUTF8($string))
      return utf8_encode($string);
    else
      return $string;
  }	

  public static function utf8_smart_decode($string)
  {
    if (self::isUTF8($string))
      return utf8_decode($string);
    else
      return $string;
  }
  
  public static function addSessionId($url, $session_id)
  {
	if (strpos($url, 'PHPSESSID') === false) {
		if (strpos($url, '?') !== false) {
			$url .= "&PHPSESSID=$session_id";
		}
		else {
			$url .= "?PHPSESSID=$session_id";
		}
	} 
	return $url;
  }
  
  public static function redirect($url, $session_id = '')
  {
	if (empty($url)) {
		$url = DEFAULT_REDIRECT_URL;
	}
	
	if (!empty($session_id)) {
	  // Strip session_id from $url
	  $url = preg_replace('/&PHPSESSID=([a-zA-Z0-9]{32})/',
			      '', $url);
	  $url = self::addSessionId($url, $session_id);
	}

	header("Location: $url");
	header('Content-type: text/vnd.wap.wml');
	exit;
  }
  
  
  public static function mktime($time)
  {
  	$mktime = 0;
	
	if (!empty($time)) {
		if (strlen($time) == 8) {
			$d = substr($time, 0, 2);
			$m = substr($time, 2, 2);
			$Y = substr($time, 4, 4);
			$mktime = mktime(0, 0, 0, $m, $d, $Y);
		}
		else if (strlen($time) == 12) {
			$d = substr($time, 0, 2);
			$m = substr($time, 2, 2);
			$Y = substr($time, 4, 4);
			$h = substr($time, 8, 2);			
			$i = substr($time, 10, 2);
			$mktime = mktime($h, $i, 0, $m, $d, $Y);
		}

	}
  
	return $mktime;
  }
  
  public static function cleanDocumentType($documentType)
  {
  	if ($documentType == 'dcdi' ||
		$documentType == 'dc,di') {
		$documentType = 'di';
	}
	else {
	    $komma = strpos($documentType, ',');
	    if (($komma = strpos($documentType, ',')) !== false) {
	      $documentType = (string) substr($documentType, 0, $komma);
	    }
	}
  
	return $documentType;
  }
  
  public static function isMD5($var) {
    return preg_match('/^[A-Fa-f0-9]{32}$/',$var);
  }
  
}

?>