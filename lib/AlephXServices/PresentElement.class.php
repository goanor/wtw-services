<?php


class PresentElement
{
  public $title = '';
  public $author = '';
  public $publishingyear = '';
  public $documentNumber = 0;
  public $documentType = '';

  function __construct($docnumber, $title,
		       $author, $publishingyear, $documentType)
  {
    $this->documentNumber = (int) $docnumber;
    $this->title = (string) $title;
    $this->author = (string) $author;
    $this->publishingyear = (string) $publishingyear;
    $this->documentType = (string) $documentType;
    $komma = strpos($documentType, ',');
    if (($komma = strpos($documentType, ',')) !== false) {
      $this->documentType = (string) substr($documentType, 0, $komma);
    }
  }

}


?>