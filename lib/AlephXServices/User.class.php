<?php

class User
{
  public $loanerNumber = '';
  public $id = '';
  public $name = '';
  public $dummy = '';

  function __construct($lnr, $id, $name)
  {
    $this->loanerNumber = (string) $loanerNumber;
    $this->id = (string) $id;
    $this->name  = (string) $name;
  }

  public static function isLoggedIn()
  {
	if (!empty($_SESSION[SITENAME]['user']))
		return true;
	else
		return false;
  }
  
  public static function getUserObj()
  {
	$obj = unserialize($_SESSION[SITENAME]['user']);
	return $obj;
  }
  
    public static function createSession($user)
	{
		$_SESSION[SITENAME]['user'] = serialize($user);
	}
}

?>