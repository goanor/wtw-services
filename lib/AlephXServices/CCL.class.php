<?php

class CCL
{
	const TransactionDate = '005';
	const LCCD = '010';
	const ISBN = '020';
	const ISSN = '022';
	const SICICode = 'SICI';
	const OCLCNumber = '035';
	const MLISRecordNumber = 'MLS';
	const Barcode = 'BAR';
	const SystemNo = 'SID';
	
	// Browse/Headings Indexes
	const Author = 'AUT';
	const TitleContents = 'TIT';
	const Title = 'TTL';
	const Series = 'SRS';
	const Subject = 'SUB';
	const LCSubject = 'SUL';
	const Imprint = 'IMP';
	const LanguageCode = 'PUB';
	const DocumentType = 'LNG';
	const SubLibrary = 'SBL';
	const LocatnCollectn = 'COL';
	const CallNumOnItem = 'CAL';
	const LCCallNums = 'LCC';
	const DeweyNumber = 'CLD';
	const SuDOCNumber = 'SUD';
	const Type = 'TYP';
	const MaterialType = 'MAT';
	const Format = 'FRM';
	const Status = 'STA';
	const ProcStatus = 'PRC';
	const SubjectsVxyz = 'XYZ';
	const LCSubjectsVxyz = 'SULX';	

	// Find/Word indexes
	const Word = 'WRD'; //Fritekst
	const WAuthor = 'WAU';
	const WTitleContents = 'WTI';
	const WTitle = 'WTT';
	const WSeries = 'WSE';
	const WSubject = 'WSU';
	const WLCSubject = 'WSB';
	const WImprint = 'WPU';
	const WDocumentType = 'WDT';
	const WLanguageCode = 'WLN';	
	const WYear = 'WYR';
	const WFormat = 'WFM';
	const WSubLibrary = 'WSL';
	const WLocatnCollectn = 'WCL';
	const WCallNumOnItem = 'WCA'; //*
	const WStatus = 'WST';
	const WLCCallNums = 'WLC';
	const WSuDOCNumber = 'WSD';
	const WMaterialType = 'WMT';	
	const WType = 'WTP';
	const WProcStatus = 'WPR';
	const WRecordType = 'WRT';
	const WBIBLevel = 'WBL';
	const WUrl = 'WUR';

}


?>