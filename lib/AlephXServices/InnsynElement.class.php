<?php


class InnsynElement
{
	public $isbn = 0;
	public $thumbnail = '';
	public $small_picture = '';
	public $large_picture = '';
	
	function __construct($isbn, $thumbnail, $small_picture, $large_picture)
	{
		$this->isbn = $isbn;
		$this->thumbnail = $thumbnail;
		$this->small_picture = $small_picture;
		$this->large_picture = $large_picture;
	}

}