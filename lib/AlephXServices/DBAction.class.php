<?php

class DBAction
{	

	protected $dbHost = DBHOST;
	protected $dbBase = DBBASE;
	protected $dbUser = DBUSER;
	protected $dbPass = DBPASS;
	protected $dbLink = null;

	protected function dbConnect()
	{
		if (!$this->dbLink) {
			$this->dbLink = @mysql_connect($this->dbHost, $this->dbUser, $this->dbPass);
			if ($this->dbLink) {
				mysql_select_db($this->dbBase, $this->dbLink);
			}
		}
		
		if (!$this->dbLink) return false;
		
		return true;
	}

	protected function executeQuery($query)
	{	
		if (!$this->dbLink) return false;
	
		return mysql_query($query, $this->dbLink);
	
	}
	
	protected function escapeString($string)
	{
		if ($this->dbLink) {
			return mysql_real_escape_string($string, $this->dbLink);
		}
		else {
			return mysql_escape_string($string);
		}
	}
	
	
	protected function getLastInsertID()
	{
		if ($this->dbLink) {
			$q = "SELECT LAST_INSERT_ID() as id";
			$result = mysql_query($q, $this->dbLink);
			$row = mysql_fetch_assoc($result);
			return $row['id'];
		}
	}
}	

?>