<?php


class Holding
{
  public $documentNumber = '';
  public $item_sequence  = '';
  public $sequence  = '';
  public $request_date  = '';
  public $barcode  = '';
  public $title = '';
  public $documentType = '';
  public $item_status = '';

  function __construct($doc_number, $item_sequence, $sequence, $request_date,
		       $item_status, $barcode, $title, $imprint)
  {
    $this->documentNumber = $doc_number;
    $this->item_sequence = $item_sequence;
    $this->sequence = $sequence;
    $this->request_date = strtotime($request_date); 
    $this->barcode = $barcode;
    $this->title = AXS_Utils::utf8_smart_decode($title);
    $this->item_status = $item_status;
    $this->documentType = AXS_Utils::cleanDocumentType($imprint);
  }
}
?>