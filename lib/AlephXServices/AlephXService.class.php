<?php
// if (!defined('CONFIG_LOADED')) die('Access forbidden');

require_once(dirname(__FILE__) . '/FindResult.class.php');
require_once(dirname(__FILE__) . '/FindRequest.class.php');
require_once(dirname(__FILE__) . '/AXS_Utils.class.php');
require_once(dirname(__FILE__) . '/CCL.class.php');
require_once(dirname(__FILE__) . '/RequestBuilder.class.php');
require_once(dirname(__FILE__) . '/PresentElement.class.php');
require_once(dirname(__FILE__) . '/DocumentElement.class.php');
require_once(dirname(__FILE__) . '/User.class.php');
require_once(dirname(__FILE__) . '/Loan.class.php');
require_once(dirname(__FILE__) . '/Fine.class.php');
require_once(dirname(__FILE__) . '/Holding.class.php');
require_once(dirname(__FILE__) . '/Item.class.php');
require_once(dirname(__FILE__) . '/Address.class.php');


// http://www.stavanger-kulturhus.no/xml_teaser?teaser=5

class AlephXService
{

  private $xservice_url = 'http://91.243.69.14/X';
  
  /*
   code: WTI, WRD
   
   returns null or FindResult object
  */
  public function find(FindRequest $findrequest)
  {
    $result = null;
    
    $adjacent = 'N';
    if ($findrequest->matchfrace) $adjacent = 'Y';
    
    $searchstring = rawurlencode(AXS_Utils::utf8_smart_encode($findrequest->searchstring));
    // EKS: request=WTI=tittel and wni=j
    $action = "{$this->xservice_url}?op=find";
    $action .= "&translate=N";
    $action .= "&base={$findrequest->base}";
    $action .= "&adjacent=$adjacent";
    $action .= "&request=";
    
    
    if (!empty($findrequest->code)) {
      $action .= "{$findrequest->code}=$searchstring";
    }
    else {
      $action .= "WAU=$searchstring"; // Forfatter
      $action .= "+OR+WTI=$searchstring"; // Tittel
      $action .= "+OR+WSU=$searchstring"; // Emne
    }

    // $searchstring = rawurlencode("WLA=sme");
      
    
    $xml_response = AXS_Utils::getXML($action);
    
    $xml = simplexml_load_string($xml_response);		
    
    if ($xml) {
      if (!empty($xml->error)) {
	return false;
      }
      $result = new FindResult((int) $xml->set_number,
			       (int) $xml->no_records,
			       (int) $xml->no_entries,
			       trim((string) $xml->{'session-id'}),
			       $findrequest);
    }
    return $result;
  }
  
  /*
   Returns XML
  */
  public function present(FindResult $findresult, $offset, $limit)
  {	
    $first = $offset;
    $last = ($offset + $limit) - 1;
    $no_records = $findresult->no_records;
    if ($last > $findresult->no_records) {
      $last = $findresult->no_records;
    }
    
    $setEntry = $first == $last ? sprintf("%09d", $first) : sprintf("%09d-%09d", $first, $last);
    

    // Sort
    $this->sort($findresult->set_number, $findresult->request->base);


    $action = "{$this->xservice_url}?op=present&translate=N" .
      "&set_entry=$setEntry&set_number={$findresult->set_number}";
    
    // header('Content-Type: text/xml');
    $xml_response = AXS_Utils::getXML($action);


    $result = array();
    $xml = simplexml_load_string($xml_response);
    foreach ($xml->xpath('/present/record') as $record) {
      $doc_number = (int) $record->doc_number;

      // Tittel: 245$a
      list($title) = $record->xpath('metadata/oai_marc/varfield[@id="245"]/subfield[@label="a"]');
      
      // Forfatter: 100$a fallback til 245$c
      list($author) = $record->xpath('metadata/oai_marc/varfield[@id="100"]/subfield[@label="a"]');
      if (empty($author)) {
	list($author) = $record->xpath('metadata/oai_marc/varfield[@id="245"]/subfield[@label="c"]');
      }
      
      // Utgivelses�r: 260$c
      list($publishingyear) = $record->xpath('metadata/oai_marc/varfield[@id="260"]/subfield[@label="c"]');
      
      // DokumentType: 019$b
      list($documentType) = $record->xpath('metadata/oai_marc/varfield[@id="019"]/subfield[@label="b"]');

      // ISBN - 020$a
      list($isbn) = $record->xpath('metadata/oai_marc/varfield[@id="020"]/subfield[@label="a"]');
      
      $title = AXS_Utils::utf8_smart_decode((string) $title);
      $author = AXS_Utils::utf8_smart_decode((string) $author);
      $publishingyear = AXS_Utils::utf8_smart_decode((string) $publishingyear);
      $documentType =  AXS_Utils::utf8_smart_decode((string) $documentType);

      $result[] = new DocumentElement($doc_number, $title,
				     $author, $publishingyear,
				      $documentType, $isbn, $barcode);
    }
    
    return $result;
  }
  

  public function sort($set_number, $base,
		       $sort_code_1 = '01', $sort_order_1 = 'D',
		       $sort_code_2 = '03', $sort_order_2 = 'A')
  {
    $action = "{$this->xservice_url}?op=sort-set";
    $action .= "&translate=N&library=$base";
    $action .= "&set_number={$set_number}";

    // Innhold i tab_sort:
    // 01 - Year - 008 -> 260$c
    // 02 - Forfatter - 100$a -> Korporasjonsnavn 110$a
    // 03 - Title - 245$a -> 240($a)
    
    if (!empty($sort_code_1)) {
      $action .= "&sort_code_1=$sort_code_1";
    }
    else {
      $action .= "&sort_code_1=01"; // Year
    }
    
    if ($sort_order_1 == 'D') {
      $action .= "&sort_order_1=D"; // A or D
    }     
    else {
      $action .= "&sort_order_1=A";
    }
    
    
    if (!empty($sort_code_2)) {
      $action .= "&sort_code_2=$sort_code_2";
    }
    else {
      $action .= "&sort_code_2=01"; // Year
    }

    if ($sort_order_2 == 'D') {
      $action .= "&sort_order_2=D"; // A or D
    }     
    else {
      $action .= "&sort_order_2=A";
    }
 

    // echo $action;
    $xml_response = AXS_Utils::getXML($action);

  }


  public function findDocument($documentNumber, $base)
  {
    // http://aleph.stavanger.kommune.no/X?op=find-doc&doc_number=000553159&base=NOR01
    $documentNumber = sprintf("%09d", $documentNumber);
    
    $action = "{$this->xservice_url}?op=find-doc&translate=N&base=$base" .
      "&doc_number=$documentNumber";
    
    $xml_response = AXS_Utils::getXML($action);
    
    $xml = simplexml_load_string($xml_response);
    
    // Could not get document
    if (empty($xml) || !$xml || !empty($xml->error)) return null;
    
    if (is_object($xml) && method_exists($xml, 'xpath')) {
      list($record) = $xml->xpath('/find-doc/record');
    }

    // Tittel: 245$a
    if (is_object($record) && method_exists($record, 'xpath')) {
      list($title) = $record->xpath('metadata/oai_marc/varfield[@id="245"]/subfield[@label="a"]');
    
      // Forfatter: 100$a fallback til 245$c
      list($author) = $record->xpath('metadata/oai_marc/varfield[@id="100"]/subfield[@label="a"]');
      if (empty($author)) {
	list($author) = $record->xpath('metadata/oai_marc/varfield[@id="245"]/subfield[@label="c"]');
      }
    
      // Utgivelses�r: 260$c
      list($publishingyear) = $record->xpath('metadata/oai_marc/varfield[@id="260"]/subfield[@label="c"]');
      
      // DokumentType: 019$b
      list($documentType) = $record->xpath('metadata/oai_marc/varfield[@id="019"]/subfield[@label="b"]');
      
      // ISBN - 020$a
      list($isbn) = $record->xpath('metadata/oai_marc/varfield[@id="020"]/subfield[@label="a"]');
    }

    $title = AXS_Utils::utf8_smart_decode((string) $title);
    $author = AXS_Utils::utf8_smart_decode((string) $author);
    $publishingyear = AXS_Utils::utf8_smart_decode((string) $publishingyear);
    $documentType =  AXS_Utils::utf8_smart_decode((string) $documentType);
    
    return new DocumentElement($documentNumber, $title,
			       $author, $publishingyear,
			       $documentType, $isbn);
    
  }
  
  public function findDocumentLKR($documentNumber, $base) {

    $documentNumber = sprintf("%09d", $documentNumber);

    $action = "{$this->xservice_url}?op=find-doc&translate=N&base=$base" .
       "&doc_number=$documentNumber";

    $xml_response = AXS_Utils::getXML($action);

    $xml = simplexml_load_string($xml_response);

    // Could not get document
    if (!empty($xml->error)) return null;
    if (!method_exists($xml, 'xpath')) return null;

    list($record) = $xml->xpath('/find-doc/record');

    list($a) = $record->xpath('metadata/oai_marc/varfield[@id="LKR"]/subfield[@label="a"]');

    list($l) = $record->xpath('metadata/oai_marc/varfield[@id="LKR"]/subfield[@label="l"]');

    list($b) = $record->xpath('metadata/oai_marc/varfield[@id="LKR"]/subfield[@label="b"]');

    return array('a' => $a, 'b' => $b, 'l' => $l);
  }

  public function borInfo($bor_id, $bor_pass = '', $base)
  {
    // http://aleph.stavanger.kommune.no/X?op=bor_info&bor_library=nor50&bor_id=120949&verification=
    
    $action = "{$this->xservice_url}?op=bor-info&translate=N";
    $action .= "&bor_id=$bor_id";
    $action .= "&verification=$bor_pass";
    $action .= "&library=$base";
    
    $xml_response = AXS_Utils::getXML($action);
    
    $xml = simplexml_load_string($xml_response);		
    
    if ($xml) {
      if (!empty($xml->error)) {
	return false;
      }
    }  
    
    $id = (string) $xml->z303->{'z303-id'};
    $name = (string) $xml->z303->{'z303-name'};
    
    $user = new User($bor_id, $id, $name);
    
    $address = new Address($xml->z304->{'z304-address-0'},
			   $xml->z304->{'z304-address-1'},
			   $xml->z304->{'z304-address-2'},
			   $xml->z304->{'z304-address-3'},
			   $xml->z304->{'z304-email-address'},
			   $xml->z304->{'z304-telephone'},
			   $xml->z304->{'z304-telephone-3'});

    $loans = array();
    foreach ($xml->{'item-l'} as $l) {

      // z13-imprint - ee 
      // z36-no-renewal
      // z36-item-status
      // 

      $loans[] = new Loan((string) $l->z36->{'z36-doc-number'},
			  (string) $l->z36->{'z36-item-sequence'},
			  (string) $l->z36->{'z36-loan-date'},
			  (string) $l->z36->{'z36-loan-hour'},
			  (string) $l->z36->{'z36-due-date'},
			  (string) $l->z36->{'z36-no-renewal'},
			  (string) $l->z36->{'z36-item-status'},
			  (string) $l->z30->{'z30-barcode'},
			  (string) $l->z13->{'z13-title'},
			  (string) $l->z13->{'z13-imprint'}
			  );
    }

    $fines = array();
    if (is_object($xml)) {
      $fine_arr = $xml->xpath("/bor-info/fine/z31[z31-status='O']");
      if (!empty($fine_arr) && is_array($fine_arr) && count($fine_arr) > 0) {
	foreach ($fine_arr as $f) {
	  $fines[] = new Fine((string) $f->{'z13-title'},
			      (string) $f->{'z13-imprint'},
			      (string) $f->{'z31-credit-debit'},
			      (string) $f->{'z31-date'},
			      (string) $f->{'z31-description'},
			      (string) $f->{'z31-sum'});
	}
      }
    }

    $holdings = array();
    $holdings_docnr = array();
    foreach ($xml->{'item-h'} as $h) {
      
      // Vi �nsker ikke dobbeltoppf�ringer
      if (!in_array((string) $h->z37->{'z37-doc-number'}, $holdings_docnr)) {
	$holdings[] = new Holding((string) $h->z37->{'z37-doc-number'},
				  (string) $h->z37->{'z37-item-sequence'},
				  (string) $h->z37->{'z37-sequence'},
				  (string) $h->z37->{'z37-request-date'},
				  (string) $h->z37->{'z37-item-status'},
				  (string) $h->z30->{'z30-barcode'},
				  (string) $h->z13->{'z13-title'},
				  (string) $h->z13->{'z13-imprint'}
				  );
	$holdings_docnr[] = (string) $h->z37->{'z37-doc-number'};
      }
    }

    $user->holdings = $holdings;
    $user->loans = $loans;
    $user->fines = $fines;
    $user->address = $address;
    return $user;

  }
  
 public function borAuth($bor_id, $bor_pass, $base)
  {
    // http://aleph.stavanger.kommune.no/X?op=bor-auth&library=nor50&bor_id=120949&verification=1234
    $action = "{$this->xservice_url}?op=bor-auth&translate=N";
    $action .= "&bor_id=$bor_id";
    $action .= "&verification=$bor_pass";
    $action .= "&library=$base";

    $xml_response = AXS_Utils::getXML($action);
    
    $xml = simplexml_load_string($xml_response);		
    
    if ($xml) {
      if (!empty($xml->error)) {
	return false;
      }
    }

    $id = (string) $xml->z303->{'z303-id'};
    $name = (string) $xml->z303->{'z303-name'};

    $user = new User($bor_id, $id, $name);

    return $user;
    
  }


  public function getItemData($documentNumber, $base)
  {
    // http://aleph.stavanger.kommune.no/X?op=item-data&doc_number=000553159&base=nor01

    $documentNumber = sprintf("%09d", $documentNumber);

    $action = "{$this->xservice_url}?op=item-data&translate=N";    
    $action .= "&doc_number=$documentNumber";
    $action .= "&base=$base";
    
    $xml_response = AXS_Utils::getXML($action);
    
    // Aleph XML sucks - do some hacking to fix this
    $xml_response = str_replace('<rec-key>',
				'</item><item><rec-key>',
				$xml_response);
    $xml_response = str_replace("<item-data>\n</item><item><rec-key>",
				'<item-data><item><rec-key>',
				$xml_response);
    $xml_response = str_replace('<session-id>',
				'</item><session-id>',
				$xml_response);


    $xml = simplexml_load_string($xml_response);

    $status = array('04', // b�ker 4 uker
		    '13', // cd
		    '29', // dvd
		    '30', // dvd
		    '31', // dvd
		    '32'  // lydb�ker
		    );

    $items_selector = '';
    $num = count($status);
    for ($i=0; $i<$num; $i++) {
      $items_selector .= "item-status='{$status[$i]}'";
      if ($i!=($num-1)) {
	$items_selector .= ' or ';
      }
    }

    $lib_filter = "sub-library = 'HOVED' or sub-library = 'MADLA'"; 

    $item_status = $xml->xpath("/item-data/item[$items_selector]");
    // print_r($item_status);
    $loanedout = $xml->xpath("/item-data/item[($items_selector) and (child::loan-due-date or requested = 'Y')]");

    $result['antall'] = count($item_status);
    $result['tilgjengelig'] = $result['antall'] - count($loanedout);
    
    $items = array();
    foreach ($xml->xpath("/item-data/item[$items_selector]") as $i) {      
      $items[] = new Item((string) $i->{'rec-key'},
			  (string) $i->barcode,
			  (string) $i->{'sub-library'},
			  (string) $i->collection,
			  (string) $i->{'item-status'},
			  (string) $i->library,
			  (string) $i->on_hold,
			  (string) $i->requested,
			  (string) $i->expected,
			  (string) $i->{'loan-status'},
			  (string) $i->{'loan-in-transit'},
			  (string) $i->{'loan-due-date'},
			  (string) $i->{'loan-due-hour'});
    }
    $result['items'] = $items;
    return $result;
  }


  public function holdRequest($barcode, $bor_id, $base)
  {
    // http://aleph.stavanger.kommune.no/X?op=hold-req&item_barcode=1234&library=nor50

    $action = "{$this->xservice_url}?op=hold-req&translate=N";    
    $action .= "&item_barcode=$barcode";
    $action .= "&bor_id=$bor_id";
    $action .= "&library=$base";     

    $xml_response = AXS_Utils::getXML($action);

    $xml = simplexml_load_string($xml_response);

    if (!empty($xml->error)) {
      // echo $xml_response;
      return false;
    }
      
    return true;
  }

  public function cancelHoldRequest($doc_number, $item_sequence,
				    $sequence, $base)
  {
    // http://aleph.stavanger.kommune.no/X?op=hold-req-cancel&doc_number=000558133&item_sequence=000010&sequence=0001&library=nor50

    $action = "{$this->xservice_url}?op=hold-req-cancel&translate=N";    
    $action .= "&doc_number=$doc_number";
    $action .= "&item_sequence=$item_sequence";
    $action .= "&sequence=$sequence";
    $action .= "&library=$base";  
    
    $xml_response = AXS_Utils::getXML($action);

    $xml = simplexml_load_string($xml_response);

    if (!empty($xml->error)) {
      // echo $xml_response;
      return false;
    }
    
    return true;

  }

  public function renew($item_barcode, $bor_id, $base)
  {
    // http://aleph.stavanger.kommune.no/X?op=renew&item_barcode=&bor_id=
    
    $action = "{$this->xservice_url}?op=renew&translate=N";    
    $action .= "&item_barcode=$item_barcode";
    $action .= "&bor_id=$bor_id";
    $action .= "&library=$base"; 

    $xml_response = AXS_Utils::getXML($action);

    $xml = simplexml_load_string($xml_response);

    if (!empty($xml->error)) {
      // echo $xml_response;
      return false;
    }
    else {
      return strtotime($xml->{'due-date'});
    }

  }


  public function renewIsPossible($user, $item_status,
				  $due_date, $no_renew)
  {
    // item-status 11 er fjernl�n, og skal ikke kunne fornyes
    if ($item_status == 11) return false;
    
    // Vi m� legge inn eventuell bel�psgrense her hvis
    // vi �nsker � begrense fornying for folk med gebyr
    
    if (date('dmY', $due_date) == date('dmY', strtotime('now + 4 weeks'))) {
      return false;
    }

    return true;

  }
  
}

?>