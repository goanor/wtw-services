<?php
require_once(CLASS_PATH . 'AXS_Utils.class.php');
require_once(CLASS_PATH . 'CalendarEvent.class.php');

class Calendar
{
	// Denne m� legges i cache, da den er alt for treg til � laste hver gang
	// Foresl�r cronjob p� denne
	private static $url = 'http://www.stavanger-kulturhus.no/xml_kalender?month=%d&year=%d';
	private static $timeout = 3600;

	public static function getEvents($month, $year, $limit = 0)
	{
		$xml_response = AXS_Utils::getXMLFromCache(sprintf(self::$url, $month, $year), self::$timeout);
    
		$xml = simplexml_load_string($xml_response);
    
		$events = array();
		
		$i = 0;
	    foreach ($xml->event as $e) {
			if ($i == $limit && $limit != 0) break;
			$events[] = new CalendarEvent($e['id'], $e['starttime'], $e['endtime'], $e['title']);
			$i++;
		}
		
		return $events;
	}
}
?>