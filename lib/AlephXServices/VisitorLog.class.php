<?php

require_once(CLASS_PATH . 'DBAction.class.php');

class VisitorLog extends DBAction {

	private $dbTable = 'visitor_log';
	private $site_id = 1;
	
	public function write()
	{
		if (!$this->dbConnect()) return false;

		$error = '';

		$session_id = session_id();
		if (!AXS_Utils::isMD5($session_id)) {
		  $error = 'Session id is not MD5';
		}

		$document_id = $this->setGET('document_id');
		$page = $this->setGET('page');

		// User agent
		$user_agent = '';
		if (!empty($_SERVER['HTTP_USER_AGENT'])) {
		  $user_agent = $_SERVER['HTTP_USER_AGENT'];
		}

		// Operator alias
		$alias = Operator::getAlias();
		$operator = Operator::getOperatorName();

		// HTTP accept language
		// Eks: nb,no;q=0.8,en-us;q=0.5,en;q=0.3
		$lang = $this->getPrefLang();

		// IP
		$remote_addr = $_SERVER['REMOTE_ADDR'];
		$client_ip = $this->getClientIP();

		// Host
		$remote_host = $_SERVER['REMOTE_HOST'];


		// Request time from Apache
		$request_time = time();
		if (!empty($_SERVER['REQUEST_TIME']) &&
		    is_numeric($_SERVER['REQUEST_TIME'])
		    && $_SERVER['REQUEST_TIME'] > 0
		    && $_SERVER['REQUEST_TIME'] <= $request_time) {
		  $request_time = $_SERVER['REQUEST_TIME'];
		}

		// Referer
		$referer = '';
		if (!empty($_SERVER['HTTP_REFERER'])) {
		  $referer = $_SERVER['HTTP_REFERER'];
		}		
		
		if (!empty($_COOKIE['PocketNews']) && AXS_Utils::isMD5($_COOKIE['PocketNews'])) {
		  $uid = $_COOKIE['PocketNews'];
		  $setcookie = 'N';
		}
		else {
		  $uid = md5(uniqid(rand(), true));
		  setcookie('PocketNews', $uid, time()+3600*24*365*10, '/', 'wtw.no');
		  $setcookie = 'Y';
		}		
	
		if (empty($error)) {
		  $q = "INSERT INTO %s (site_id, request_time, user_agent, session_id, " .
		     "operator, operator_alias, page, document_id, " .
		     "pref_lang, remote_addr, remote_host, " .
		     "client_ip, referer, prev_log_id, " .
		     "cookie, setcookie) " .
		     "VALUES (%s, %s, %s, %s, %s, %s, %s, %s," .
		     "%s, %s, %s, %s, %s, %s, %s, '%s')";
		    
		  $q = sprintf($q,
		               $this->dbTable,
		               $this->quote_smart($this->site_id),
		               "FROM_UNIXTIME($request_time)",
		               $this->quote_smart($user_agent),
		               $this->quote_smart($session_id),
		               $this->quote_smart($operator),
		               $this->quote_smart($alias),
		               $this->quote_smart($page),
		               $this->quote_smart($document_id),
		               $this->quote_smart($lang),
		               $this->quote_smart($remote_addr),
		               $this->quote_smart($remote_host),             
		               $this->quote_smart($client_ip),
		               $this->quote_smart($referer),
		               $this->quote_smart($_SESSION[md5($this->site_id)]['prev_log_id']),
		               $this->quote_smart($uid),
		               $setcookie);
		    
		  if ($this->executeQuery($q)) {
		    // Get last insert it and set it in session
		    $_SESSION[md5($this->site_id)]['prev_log_id'] = $this->getLastInsertID();
		  }
		  
		}			
		
	}
	
	public function outputGIF()
	{
	  // Output image
	  $im = imagecreate(1, 1);
	  $bgcolor = imagecolorallocate($im, 255, 255, 255);
	  imagecolortransparent($im, $bgcolor);
	  
	  header("Content-type: image/gif");
	  imagegif($im);
	  imagedestroy($im);	
	}
	

	private function getPrefLang()
	{
	  $pref = array();
	  foreach(split(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']) as $lang) {
	    if (preg_match('/^([a-z]+).*?(?:;q=([0-9.]+))?/i',
	                   $lang.';q=1.0', $split)) {
	      $pref[sprintf("%f%d", $split[2], rand(0,9999))] =
	         strtolower($split[1]);       
	    }
	  }
	  
	  if (count($pref) > 0) {
	    krsort($pref);
	    return array_shift($pref);
	  }
	  
	  // Empty header
	  return '';
	}	

	private function getClientIP()
	{
	  if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
	    $ip = $_SERVER['HTTP_CLIENT_IP'];
	  }
	  else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	  }
	  else if (!empty($_SERVER['HTTP_X_FORWARDED'])) {
	    $ip = $_SERVER['HTTP_X_FORWARDED'];
	  }
	  elseif (!empty($_SERVER['HTTP_FORWARDED_FOR'])) {
	    $ip = $_SERVER['HTTP_FORWARDED_FOR'];
	  }
	  elseif (!empty($_SERVER['HTTP_FORWARDED'])) {
	    $ip = $_SERVER['HTTP_FORWARDED'];
	  }
	  else if (!empty($_SERVER['HTTP_X_NOKIA_IPADDRESS'])) {
	    $ip = $_SERVER['HTTP_X_NOKIA_IPADDRESS'];
	  }
	  else if (Operator::isTele2()) {
	    $ip_pattern = "/((25[1-5]|2[1-4]\\d|[01]\\d{2}|" .
	       "(?<=[^\\d]|^)\\d{1,2})\\.){3}" .
	       "(25[1-5]|2[1-4]\\d|[01]\\d{2}|\\d{1,2})(?=[^\\d]|$)/";
	    
	    preg_match($ip_pattern,
	               $_SERVER['HTTP_COOKIE'], $matches);
	    $ip = $matches[0];
	  }
	  else {
	    $ip = '';
	  }

	  return $ip;
	}
	
	private function setGET($fieldname, $defaultvalue = '')
	{
	  if (!empty($_GET[$fieldname])) {
	    return $_GET[$fieldname];
	  }
	  else {
	    return $defaultvalue;
	  }
	}

	private function quote_smart($value)
	{
	  if(is_array($value)) {
	    return array_map("quote_smart", $value);
	  }
	  else {
	    if(get_magic_quotes_gpc()) {
	      $value = stripslashes($value);
	    }

	    if( $value == '' ) {
	      $value = 'NULL';
	    }
	    else if(!is_numeric($value) || $value[0] == '0') {
	      $value = "'" . $this->escapeString($value) . "'";
	    }
	    return $value;
	  }
	}
	
}
?>