<?php



class CalendarEvent
{
  public $id = '';
  public $starttime = '';
  public $endtime = '';
  public $title = 0;


  function __construct($id, $starttime,
					   $endtime, $title)
  {
    $this->id = (int) $id;

    $this->starttime = AXS_Utils::mktime($starttime);
    $this->endtime =  AXS_Utils::mktime($endtime);
    $this->title = (string) AXS_Utils::utf8_smart_decode($title);
  }

}


?>