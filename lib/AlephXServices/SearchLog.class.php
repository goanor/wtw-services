<?php

require_once(CLASS_PATH . 'DBAction.class.php');

class SearchLog extends DBAction {

	private $dbTable = 'search_log';	

	public function search($search_string, $search_hits)
	{
		if (!$this->dbConnect()) return false;

		if (empty($search_string)) return false;
		
		$search_string = $this->escapeString($search_string);
		$search_hits = $this->escapeString($search_hits);
				
		$session_id = session_id();
		if (empty($session_id)) return false;
		$session_id = $this->escapeString($session_id);
		
		$cookie = 'NULL';
		if (!empty($_COOKIE['PocketNews']) && AXS_Utils::isMD5($_COOKIE['PocketNews'])) {
			$cookie = "'" . $this->escapeString($_COOKIE['PocketNews']) . "'";
		}
	
		$q = "INSERT INTO {$this->dbTable} " .
			"(search_time, session_id, cookie, search_string, search_hits) " .
			" VALUES (NOW(), '$session_id', $cookie, '$search_string', $search_hits)";

		$this->executeQuery($q);
		
	}
	
}
?>