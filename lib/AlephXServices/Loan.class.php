<?php


class Loan
{
  public $documentNumber = '';
  public $item_sequence  = '';
  public $loan_datetime  = '';
  public $due_date  = '';
  public $barcode  = '';
  public $title = '';
  public $num_renewal = 0;
  public $documentType = '';
  public $item_status = '';

  function __construct($doc_number, $item_sequence, $loan_date,
		       $loan_hour, $due_date,  $num_renewal,
		       $item_status, $barcode, $title, $imprint)
  {
    $this->documentNumber = $doc_number;
    $this->item_sequence = $item_sequence;
    $this->loan_datetime = strtotime("{$loan_date}{$loan_hour}");
    $this->due_date = strtotime($due_date); 
    $this->barcode = $barcode;
    $this->title = AXS_Utils::utf8_smart_decode($title);
    $this->num_renewal = $num_renewal;
    $this->item_status = $item_status;
	
    $this->documentType = AXS_Utils::cleanDocumentType($imprint);
  }
}
?>