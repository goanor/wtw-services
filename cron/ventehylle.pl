#!/usr/bin/perl
use strict;
use warnings;
use IO::Socket;
use POSIX qw(strftime);

my $sock = IO::Socket::INET->new(Listen    => 5,
                                #LocalAddr => 'localhost',
                                LocalPort => 31235,
                                ReuseAddr => 1,
                                Proto     => 'tcp',
                                 );
die "Unable to listen: $!" unless $sock && $sock->sockport();

my $hour;
while (1)
{
    sleep(15);
    $hour = strftime("%H", localtime);
    # wait 15 seconds
    if ($hour > '08' && $hour <= '18') {
	system("/usr/local/wtw/bin/ventehylle.php");
	#exit;
    }
    else {
	#print("Off duty\n");
    }
}
