<?php
require_once('/usr/local/wtw/config.inc.php');
require_once(CLASS_PATH . 'WTW_Aleph.class.php');

$msisdn = $_GET['msisdn'];
if (!empty($msisdn)) {

  $dbConn = ocilogon(ORACLE_USERNAME, ORACLE_PASSWORD);
  
  $wtw = new WTW_Aleph($dbConn);

  $loans = $wtw->getLastNotifiedLoansByMsisdn($msisdn);
  echo implode(';', $loans);

  oci_close($dbConn);
}

?>