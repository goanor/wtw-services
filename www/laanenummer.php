<html>
<head>
<title>Hent l&aring;nenummer</title>
</head>
<body>
<h1>Hent l&aring;nenummer</h1>
<?php
require_once('/usr/local/wtw/config.inc.php');
require_once(CLASS_PATH . 'WTW_Aleph.class.php');
require_once(CLASS_PATH . 'SMSRespons.class.php');

$dbConn = ocilogon(ORACLE_USERNAME, ORACLE_PASSWORD);

$wtw = new WTW_Aleph($dbConn);

$sent = $notfound = false;

if (!empty($_POST['bor_nr'])) {
  // Send laanenummer

  $result = $wtw->forgotBorrowID($_POST['bor_nr']);

  if (!empty($result)) {
    $count = count($result);
    
    $sr = new SMSRespons(SMSRESPONS_USERNAME, SMSRESPONS_PASSWORD);

    $message = "L�nenummer hos S�lvberget:\n\n";

    foreach (array_reverse($result) as $bor_nr) {
      $message .= $bor_nr . "\n";
    }
    if($count>1)
      $message .= "\n$count l�nenummer registrert.";

    $sms_id = $sr->sendSMS($_POST['bor_nr'],
		 $message,
		 0,
		 true);
     
    $sent = true;

    //print_r($message);
  } else {
    $notfound = true;
  }
}

if (!$sent) {
?>
<p>Fyll inn ditt mobilnummer og du vil f&aring; tilsendt l&aring;nenummer p&aring; SMS. Kr.1</p>
<form action="<?=$_SERVER['PHP_SELF']?>" method="post">
<p><input type="text" name="bor_nr"></p>
<p><input type="submit" name="submit" value="Send l&aring;nenummer"></p>
<input type="hidden" name="sid" value="<?=$_REQUEST['sid']?>">
</form>
<?php
  if($notfound) {
    echo '<b style="color:red;">Ugyldig nummer</b>';
  }
}
else
{
  echo "<p>L&aring;nenummer er sendt p&aring; sms.</p>";
}
oci_close($dbConn);
?>
</body>
</html>
