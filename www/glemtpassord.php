<html>
<head>
<title>Glemt passord</title>
</head>
<body>
<h1>Glemt passord</h1>
<?php
require_once('/usr/local/wtw/config.inc.php');
require_once(CLASS_PATH . 'WTW_Aleph.class.php');
require_once(CLASS_PATH . 'SMSRespons.class.php');

$dbConn = ocilogon(ORACLE_USERNAME, ORACLE_PASSWORD);

$wtw = new WTW_Aleph($dbConn);

$sent = false;
if (!empty($_POST['bor_id'])) {
  // Send passord

  $pass = $wtw->forgotPassword($_POST['bor_id']);

  if (!empty($pass)) {
    $sr = new SMSRespons(SMSRESPONS_USERNAME, SMSRESPONS_PASSWORD);

    $message = "Ditt passord hos S�lvberget er: {$pass['Z308_VERIFICATION']}";

    $sms_id = $sr->sendSMS($pass['Z304_TELEPHONE_3'],
		 $message,
		 0,
		 true);
    $sent = true;
    // print_r($pass);
  }
}

if (!$sent) {
?>
<p>Fyll inn ditt l&aring;nenummer og du vil f&aring; tilsendt passord p&aring; SMS. Kr.1</p>
<form action="<?=$_SERVER['PHP_SELF']?>" method="post">
<p><input type="text" name="bor_id"></p>
<p><input type="button" value="Tilbake"> <input type="submit" name="submit" value="Send meg passord"></p>
<input type="hidden" name="sid" value="<?=$_REQUEST['sid']?>">
</form>
<?php
}
else
{
  echo "<p>Passordet er sendt p&aring; sms.</p>";
}
oci_close($dbConn);
?>
</body>
</html>
