<?php

require_once(dirname(__FILE__) . '/../../config.inc.php');
require_once(CLASS_PATH . 'WTW_Aleph.class.php');
require_once(CLASS_PATH . 'Logger.class.php');

require_once(CLASS_PATH . 'SMSRenewLogger.class.php');

$log = new Logger(LOG_DIR . basename(__FILE__, '.php') . '.log');


$db_conn = ocilogon(ORACLE_USERNAME, ORACLE_PASSWORD);

$wtw = new WTW_Aleph($db_conn);
$renew_logger = new SMSRenewLogger($db_conn);

$sms_id       = !empty($_POST['sms_id']) ? $_POST['sms_id'] : '';
$recipient    = !empty($_POST['recipient']) ? $_POST['recipient'] : '';
$tariff       = !empty($_POST['tariff']) ? $_POST['tariff'] : 0;
$message_text = !empty($_POST['message_text']) ? $_POST['message_text'] : '';
$renews       = !empty($_POST['renews']) ? $_POST['renews'] : '';
$log->write($renews);
// FIXME: serialized array is a magic quotes victim. 
// Make this code portable by checking the magic quotes setting or something
// not just always applying stripslashes
$renews = unserialize(stripslashes($renews));


$log->write("Received sms_id: $sms_id, recipient: $recipient, tariff: $tariff, message_text: $message_text, renews: $renews");

if(!empty($renews)) {
  $bor_id = $wtw->getBorIdByLoanNr($renews[0]['doc_number'] . $renews[0]['item_sequence']);
  $log->write("Bor id: $bor_id");
  
  foreach($renews as &$r) {
    $r['loan_number'] = $wtw->getLoanNrByRecKey($r['doc_number'] . $r['item_sequence']);
    $log->write("Found loan_number: " . $r['loan_number'] . " from " . $r['doc_number'] . $r['item_sequence']);
  }
} else {
  $log->write("Renews is empty");
}


$renew_logger->logSMSRenew($sms_id,$recipient,$tariff,$message_text, $bor_id, $renews);


?>
