<?php
/*
Aleph environment variables are found in:
/exlibris/aleph/a16_1/alephe/www_server.conf
*/
require_once('/usr/local/wtw/config.inc.php');
require_once(CLASS_PATH . 'WTW_Aleph.class.php');

function daysBefore($days, $type = 'SMS')
{
  global $notifications;

  $ntype = 'SMS_PRE_DUE_DAYS_BEFORE';
  if ($type == 'EMAIL') {
    $ntype = 'EMAIL_PRE_DUE_DAYS_BEFORE';
  }
  
  if ((empty($notifications[$ntype]) && $days == 3) ||
      $notifications[$ntype] == $days) {
    return ' selected';
  }
}

$dbConn = ocilogon(ORACLE_USERNAME, ORACLE_PASSWORD);

$wtw = new WTW_Aleph($dbConn);

// Get borrower id by session_id
$bor_id = $wtw->getBorIdBySessionId($_REQUEST['sid']);

// If bor_id is ALEPH the user is not logged in.
if (!empty($bor_id) && $bor_id != 'ALEPH') {

  // TODO: Check to see if user has valid Z304_TELEPHONE_3
  
  // Update notifications if requested
  if ($_POST['submit']) {
    $wtw->setBorNotifications($bor_id, $_POST);
  }
  $notifications = $wtw->getBorNotifications($bor_id);

  $sms_pre_due_checked = '';
  $email_pre_due_checked = '';
  $sms_hold_shelf_checked = '';
  if (is_array($notifications)) {
    // print_r($notifications);
    if ($notifications['SMS_PRE_DUE'] == 'Y') {
      $sms_pre_due_checked = ' checked';
    }

    if ($notifications['EMAIL_PRE_DUE'] == 'Y') {
      $email_pre_due_checked = ' checked';
    }

    if ($notifications['SMS_HOLD_SHELF'] == 'Y') {
      $sms_hold_shelf_checked = ' checked';
    }

  }
  else {
    $email_pre_due_checked = '  checked';
  }
?>
<html>
<head>
<title>Mine varslinger</title>
</head>
<body>
<h1>Mine varslinger</h1>
<p>Her kan du sette opp hvilke varslinger du &oslash;nsker &aring; motta.</p>
<form action="<?=$_SERVER['PHP_SELF']?>" method="post">
<p><input type="checkbox" name="sms_pre_due" value="Y"<?=$sms_pre_due_checked?>> Ja, takk. Jeg vil ha <b>SMS</b> med p&aring;minnelse
<select name="sms_pre_due_days_before">
<option value="1"<?=daysBefore(1);?>>1</option>
<option value="2"<?=daysBefore(2);?>>2</option>
<option value="3"<?=daysBefore(3);?>>3</option>
</select>
dager f&oslash;r innlevering (kr 1 pr mottatt mld)</p>
<p><input type="checkbox" name="email_pre_due" value="Y"<?=$email_pre_due_checked?>> Ja, takk. Jeg vil ha <b>epost</b> med p&aring;minnelse
<select name="email_pre_due_days_before">
<option value="1"<?=daysBefore(1, 'EMAIL');?>>1</option>
<option value="2"<?=daysBefore(2, 'EMAIL');?>>2</option>
<option value="3"<?=daysBefore(3, 'EMAIL');?>>3</option>
</select>
dager f&oslash;r innlevering</p>
<p><input type="checkbox" name="sms_hold_shelf" value="Y"<?=$sms_hold_shelf_checked?>> Ja, takk. Jeg vil ha beskjed p&aring; <b>SMS</b> n&aring;r reservert materiale er klart til henting (st&aring;r p&aring; ventehylle) (kr 1 pr mottatt mld).</p>
<p><input type="button" value="Tilbake"> <input type="submit" name="submit" value="Lagre innstillinger"></p>
<input type="hidden" name="sid" value="<?=$_REQUEST['sid']?>">
</form>
</body>
</html>
<?php
}
else {
?>
Du er ikke logget inn.
<?php
}
oci_close($dbConn);
?>
