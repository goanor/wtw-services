#!/root/php/php-5.2.6/sapi/cli/php
<?php
require_once(dirname(__FILE__) . '/../config.inc.php');
require_once(CLASS_PATH . 'WTW_Aleph.class.php');
require_once(CLASS_PATH . 'SMSRespons.class.php');

$db_conn = ocilogon(ORACLE_USERNAME, ORACLE_PASSWORD);

$wtw = new WTW_Aleph($db_conn);
$sr = new SMSRespons(SMSRESPONS_USERNAME, SMSRESPONS_PASSWORD);

$bors = $wtw->getOverDueBors();
print_r($bors);

$num_overdue = count($bors);
echo "$num_overdue\n";

oci_close($db_conn);
?>