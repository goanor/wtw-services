#!/usr/local/bin/php
<?php
require_once(dirname(__FILE__) . '/../config.inc.php');
require_once(CLASS_PATH . 'WTW_Aleph.class.php');
require_once(CLASS_PATH . 'SMSRespons.class.php');
require_once(CLASS_PATH . 'Smarty_Aleph.class.php');
require_once(ALEPH_XSERVICES_LIB);
require_once(SWIFT_LIB);

$for_bor_status = explode(',',FOR_BOR_STATUS);

$db_conn = ocilogon(ORACLE_USERNAME, ORACLE_PASSWORD);
if (!$db_conn) exit('No connection');

$wtw = new WTW_Aleph($db_conn);
$aleph = new AlephXService();

$start_time = microtime(true);
$bors_email = $wtw->getPreDueBors('EMAIL', $for_bor_status);
print_r($bors_email);
echo count($bors_email) . "\n";
if ($bors_email && count($bors_email) > 0) {

  $stmt = $wtw->preparePreDueLoansQuery('EMAIL', $for_bor_status);
  $stmt_ill = $wtw->preparePreDueLoansQueryIll('EMAIL', $for_bor_status);

  $i=0;
  $num_sent=0;
  foreach ($bors_email as $b) {
    echo "{$b['Z36_ID']}\n";
    
    $xservice_bor = $aleph->borInfo($b['Z36_ID'], '', ALEPH_ADM_DB);

    $to_email = $xservice_bor->address->email;
    // Validate email
    if (!WTW_Aleph::isEmail($to_email)) continue; 
    echo $to_email . "\n";

    $loans_local = $wtw->getPreDueLoans($b['Z36_ID'], $stmt);
    $loans_ill = $wtw->getPreDueLoans($b['Z36_ID'], $stmt_ill);
    print_r($loans_local);
    print_r($loans_ill);

    $loans = array_merge($loans_local, $loans_ill);

    $num_loans = count($loans);

    $due_in_days = $b['EMAIL_PRE_DUE_DAYS_BEFORE'];
    if (empty($due_in_days)) {
      $due_in_days = DAYS_BEFORE_DUE;
    }

    // Convert to unixtime
    for ($j=0; $j<count($loans); $j++) {
      $loans[$j]['Z36_DUE_DATE_UNIXTIME'] = strtotime($loans[$j]['Z36_DUE_DATE']);
    }

    $bor_nr = $wtw->getBorNrFromBorId($b['Z36_ID']);

    $i++;
  }
  echo "Num: $i\n";
  
}
else {
  echo "Nothing to do...";
}

echo "Time: " . ((float) microtime(true) - (float) $start_time);

oci_close($db_conn);

?>
