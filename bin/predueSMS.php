#!/usr/local/bin/php
<?php
/**
*
* predueSMS.php
* Sends SMS to remind borrowers that their loans are soon due
*
*/
require_once(dirname(__FILE__) . '/../config.inc.php');
require_once(CLASS_PATH . 'WTW_Aleph.class.php');
require_once(dirname(__FILE__) . '/../lib/WtwSmsConnect/wtwsmssend.class.php');
require_once(CLASS_PATH . 'Lock.class.php');
require_once(CLASS_PATH . 'Logger.class.php');
require_once(ALEPH_XSERVICES_LIB);
require_once(CLASS_PATH . 'Smarty_Aleph.class.php');


$log = new Logger(LOG_DIR . basename(__FILE__, '.php') . '.log');

// Lock mechanism to prevent multiple instances of this job 
// running in parallell
$lock = new Lock(basename(__FILE__, '.php'));
if ($lock->isActive()) {
  $log->write("Lock is set. Another process is running. Aborting.");
  exit;
}
$lock->set();

$db_conn = ocilogon(ORACLE_USERNAME, ORACLE_PASSWORD);

$wtw = new WTW_Aleph($db_conn);
$aleph = new AlephXService();
$smarty = new Smarty_Aleph();

// Fetch all borrowers with predue loans
$bors_sms = $wtw->getPreDueBors();
if ($bors_sms && count($bors_sms) > 0) {
  $log->write("Processing: " . count($bors_sms));
  
  // Prepare statement for fetching all predue local loans 
  $stmt_local = $wtw->preparePreDueLoansQuery();
  // Prepare statement for fetching all predue remote loans
  $stmt_ill = $wtw->preparePreDueLoansQueryIll();

  foreach ($bors_sms as $b) {
    
    // Fetch borrower information
    $xservice_bor = $aleph->borInfo($b['Z36_ID'], '', ALEPH_ADM_DB);
    echo $phone = (string) $xservice_bor->address->mobilephone;

    // Validate phone
    if (!WTW_Aleph::isMobile($phone)) {
      if (!empty($to_email)) {
        $log->write("Invalid mobilenumber for {$b['Z36_ID']}: $phone");
      }
      continue;
    }
    
    // Retrieve all local loans
    $loans_local = $wtw->getPreDueLoans($b['Z36_ID'], $stmt_local);
    // Retrieve all remote loans
    $loans_ill = $wtw->getPreDueLoans($b['Z36_ID'], $stmt_ill);

    $loans = array_merge($loans_local, $loans_ill);

    print_r($loans);

    // Prepare message
    $smarty->assign('days_to_due',$b['SMS_PRE_DUE_DAYS_BEFORE']);
    $smarty->assign('loans',$loans);
    $message = $smarty->fetch('sms/preduemessage_1.plain'); 
    $log->write("$phone : $message");
    
    
    if ($message) {
      
      // Try to send message
      $serviceCode = SMS_GAS_CODE_ZERO;
      if (PREDUE_SMS_TARIFF > 0) {
	$serviceCode = SMS_GAS_CODE;
      }

      if ($sms_id = WtwSmsSend::sendSms(SMS_GATEWAY, SMS_CODEWORD,
					SMS_API_KEY, '',
					'47', $phone,
					$message,
					PREDUE_SMS_TARIFF,
					$serviceCode, SMS_TEXT_DESC)) {
	// Insert into SMS log
	$sms_log_id = $wtw->logSMS($sms_id, $phone, $message);
	$log->write("$phone, SMS-Id: $sms_id, Log-Id: $sms_log_id");
	
	// Insert into notification log
	if ($sms_log_id) {
	  $wtw->tagSMSPreDueNotificationSent($sms_log_id, $b, $loans);
	}

      }
    }
    
  }
  $wtw->freePreDueLoansQuery($stmt_local);
  $wtw->freePreDueLoansQuery($stmt_ill);
  $log->write("Done");
}
else {
  echo "Nothing to do...\n";
  $log->write("Nothing to do...");
}

oci_close($db_conn);

$lock->release();
?>
