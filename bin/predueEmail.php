#!/usr/local/bin/php
<?php
require_once(dirname(__FILE__) . '/../config.inc.php');
require_once(CLASS_PATH . 'WTW_Aleph.class.php');
require_once(CLASS_PATH . 'SMSRespons.class.php');
require_once(SWIFT_LIB);
require_once(CLASS_PATH . 'Smarty_Aleph.class.php');
require_once(ALEPH_XSERVICES_LIB);
require_once(CLASS_PATH . 'Lock.class.php');
require_once(CLASS_PATH . 'Logger.class.php');

$log = new Logger(LOG_DIR . basename(__FILE__, '.php') . '.log');
$log_mails = new Logger(LOG_DIR . basename(__FILE__, '.php') .
			'_mails_' . date('Y-m-d') . '.log');

// Only send reminders to borrowers with one of the following statuses:
$for_bor_status = explode(',',FOR_BOR_STATUS);

$lock = new Lock(basename(__FILE__, '.php'));
if ($lock->isActive()) {
  $log->write("Lock is set. Another process is running. Aborting.");
  exit;
}
$lock->set();

$transport = Swift_MailTransport::newInstance();
$mailer = Swift_Mailer::newInstance($transport);

// Could not get ��� to work in subject and adress with utf-8
// Possible Swift bug?
Swift_Preferences::getInstance()->setCharset('iso-8859-1');

$db_conn = ocilogon(ORACLE_USERNAME, ORACLE_PASSWORD);

$wtw = new WTW_Aleph($db_conn);
$aleph = new AlephXService();

$start_time = microtime(true);
$bors_email = $wtw->getPreDueBors('EMAIL', $for_bor_status);

if ($bors_email && count($bors_email) > 0) {
  $log->write("Starting to process " . count($bors_email) . " records.");
  if (EMAIL_PRE_DUE_DISABLE_SENDING) {
    $log->write("Sending is disabled.");
  }
  print_r($bors_email);
  echo count($bors_email) . "\n";

  $stmt = $wtw->preparePreDueLoansQuery('EMAIL', $for_bor_status);
  $stmt_ill = $wtw->preparePreDueLoansQueryIll('EMAIL', $for_bor_status);

  $smarty = new Smarty_Aleph();
  $i=0;
  $num_sent=0;
  foreach ($bors_email as $b) {
    echo "{$b['Z36_ID']}\n";

    $xservice_bor = $aleph->borInfo($b['Z36_ID'], '', ALEPH_ADM_DB);
    $to_email = (string) $xservice_bor->address->email;

    // Validate email
    if (!WTW_Aleph::isEmail($to_email)) {
      if (!empty($to_email)) {
	$log->write("Invalid email for {$b['Z36_ID']}: $to_email");
      }
      continue;
    }

    $loans_local = $wtw->getPreDueLoans($b['Z36_ID'], $stmt);
    $loans_ill = $wtw->getPreDueLoans($b['Z36_ID'], $stmt_ill);
    print_r($loans_local);
    print_r($loans_ill);

    $loans = array_merge($loans_local, $loans_ill);

    $num_loans = count($loans);

    $due_in_days = $b['EMAIL_PRE_DUE_DAYS_BEFORE'];
    if (empty($due_in_days)) {
      $due_in_days = DAYS_BEFORE_DUE;
    }

    // Convert to unixtime
    for ($j=0; $j<count($loans); $j++) {
      $loans[$j]['Z36_DUE_DATE_UNIXTIME'] = strtotime($loans[$j]['Z36_DUE_DATE']);
    }

    $bor_nr = $wtw->getBorNrFromBorId($b['Z36_ID']);
    $smarty->assign('bor_nr', $bor_nr);
    $smarty->assign('bor', $b);
    $smarty->assign('user', $xservice_bor);
    $smarty->assign('loans', $loans); 
    $html_body = $smarty->fetch('email/preduemessage.html');
    echo "$html_body\n";

    

    try {
      $message = Swift_Message::newInstance();
      $message->setFrom(array(EMAIL_RENEW_ADDRESS =>
			      EMAIL_GENERAL_ADDRESS_NAME));
      $message->setReturnPath(EMAIL_RETURN_PATH);
      $message->setSubject("Du har $num_loans l�n som forfaller om $due_in_days dag(er)");
      $message->setTo($to_email);
      
      $msgId = $message->getHeaders()->get('Message-ID');
      
      //$message_id = time() . '.' . uniqid('thing') . '@stavanger-kulturhus.no'
      // $msgId->setId($message_id);
      $message_id = $msgId->getId();
      
      $message->setBody($html_body, 'text/html');
      $plain_body = html_entity_decode(strip_tags($html_body));
      $message->addPart($plain_body, 'text/plain');
      
      $log_mails->write($message->toString());
      
      if (!EMAIL_PRE_DUE_DISABLE_SENDING) {
	$sent_result = $mailer->send($message);
	
	if ($sent_result) {
	  // Log this email
	  $email_log_id = $wtw->logEmail($message_id, $to_email);
	  
	  // Insert into notification log
	  if ($email_log_id) {
	    $wtw->tagEmailPreDueNotificationSent($email_log_id, $b, $loans);
	  }
	  $num_sent++;
	}
	
   
      }
      
      echo "Til: $to_email\n";
      echo "Message-id: $message_id\n";
      echo "Sent: $sent_result\n";
      $log->write("To: $to_email, Message-id: $message_id, Sent: $sent_result");

    }
    catch (Exception $e) {
      $log->write("FAILED! To: $to_email, Message-id: $message_id, Sent: $sent_result");
      echo $e->getTraceAsString();
    }

    // Free the message object
    unset($message);

    // break;
    $i++;
  }
  echo "Num: $i\n";
  
  $log->write("Done. $num_sent of $i messages sent. Time used: " . ((float) microtime(true) - (float) $start_time));
}
else {
  echo "Nothing to do...";
  $log->write("Nothing to do...");
}

echo "Time: " . ((float) microtime(true) - (float) $start_time);

oci_close($db_conn);

$lock->release();
?>
