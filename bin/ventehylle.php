#!/usr/local/bin/php
<?php
/**
* ventehylle.php
* 
* This job handles letters from the hold shelf (ventehylle)
* Each letter represents an item (book, magazine,..)
*
*/
require_once(dirname(__FILE__) . '/../config.inc.php');
require_once(CLASS_PATH . 'WTW_Aleph.class.php');
require_once(CLASS_PATH . 'SMSRespons.class.php');
require_once(CLASS_PATH . 'Lock.class.php');
require_once(CLASS_PATH . 'Logger.class.php');
require_once(CLASS_PATH . 'Smarty_Aleph.class.php');
require_once(ALEPH_XSERVICES_LIB);
require_once(SWIFT_LIB);
require_once(CLASS_PATH . 'EmailLogger.class.php');
require_once(CLASS_PATH . 'HoldShelfLogger.class.php');

$lock = new Lock(basename(__FILE__, '.php'));
$log = new Logger(LOG_DIR . basename(__FILE__, '.php') . '_' . date('Y-m-d') . '.log');

if ($lock->isActive()) {
  $log->write("Lock is set. Another process is running. Aborting.");
  exit;
}

$lock->set();

$email_logger = new EmailLogger(EMAIL_LOG_DIR . date('Y/m/d'));
$hs_logger = new HoldShelfLogger(HOLD_SHELF_LETTERS_DIR,
				 HOLD_SHELF_LETTERS_LOG_DIR . date('Y/m/d'));

$db_conn = ocilogon(ORACLE_USERNAME, ORACLE_PASSWORD);
$wtw = new WTW_Aleph($db_conn);
$sr = new SMSRespons(SMSRESPONS_USERNAME, SMSRESPONS_PASSWORD);
$smarty = new Smarty_Aleph();
$aleph = new AlephXService();

$transport = Swift_MailTransport::newInstance();
$mailer = Swift_Mailer::newInstance($transport);

Swift_Preferences::getInstance()->setCharset('iso-8859-1');

// Fetch all items in hold shelf directory
$letter_files = WTW_Aleph::getHoldShelfLetterFiles();

if (count($letter_files) > 0) {
  $log->write("Processing " . count($letter_files) . " letter files");

  $letters = $wtw->getHoldShelfLetters($letter_files);
  
  foreach($letters as $letter) {
    // This is used to decide if we should copy and unlink the letter file
    $letterSent = false;

    $xservice_bor = $aleph->borInfo($letter['bor_id'], '', ALEPH_ADM_DB);
    $smarty->assign('user', $xservice_bor);
    $bor_nr = $wtw->getBorNrFromBorId($letter['bor_id']);
    $smarty->assign('bor_nr', $bor_nr);
    $smarty->assign('l', $letter);
    print_r($letter);

    $sentEmail = false;
    $sentSMS   = false;
    
    if($wtw->holdShelfLetterProcessed($letter['letter_file'])) {
        $log->write("Letter " . $letter['letter_file'] . " already processed, skipping ...");
        echo "Letter " . $letter['letter_file'] . " already processed, skipping ...\n";

	// Move file and delete
	if (!$hs_logger->copyFile($letter['letter_file'])) {
	  $log->write("Could not copy letter file " .
		      "{$letter['letter_file']} to " .
		      HOLD_SHELF_LETTERS_LOG_DIR . date('Y/m/d'));
	}
	
	// We don't care if file is not copied. We unlink anyway.
	if (!$hs_logger->unlinkSourceFile($letter['letter_file'])) {
	  $log->write("Could not delete letter file " .
		      "{$letter['letter_file']} from " .
		      HOLD_SHELF_LETTERS_DIR);
	}
        continue;
    }

    if (!empty($letter['email']) && WTW_Aleph::isEmail($letter['email'])) {
      // Send hold shelf notification by email
      echo "Sending email to " . $letter['email'] . " for title " . $letter['title'] . "\n";

      $log->write("Sending email to " . $letter['email'] . " for title " . $letter['title']);

      $html_body = $smarty->fetch('email/holdshelf.html');
      $plain_body = $smarty->fetch('email/holdshelf.plain');
      
      $message = Swift_Message::newInstance();
      $message->setSubject($letter['title'] . " er klar til henting");
      
      $message->setFrom(array(HOLD_SHELF_FROM_ADDRESS => HOLD_SHELF_FROM_NAME));
      $message->setTo($letter['email']);
      $message->setReturnPath(HOLD_SHELF_RETURN_PATH);

      $msgId = $message->getHeaders()->get('Message-ID');
      $message_id = $msgId->getId();     

      $message->setBody($html_body, 'text/html');
      $message->addPart($plain_body, 'text/plain');
     
      // Write email to file
      if ($email_logger->writeToFile($message_id,$html_body) > 0) {
        $log->write("Wrote email body to file: $message_id");
      }
     
      $sent_result = $mailer->send($message);
      if ($sent_result) {
	$wtw->logHoldShelfEmail($letter['letter_file'],
				$letter['hold_shelf_id'],
				$letter['bor_id'],
				$letter['email'],$message_id);

	$sentEmail = true;
	$letterSent = true;
      }

      echo "Sent: " . $sent_result . "\n";
    }
    
    if (!empty($letter['mobnr']) && WTW_Aleph::isMobile($letter['mobnr'])) {
      // Send hold shelf notification by SMS
      // Getting preferences
      $prefs = $wtw->getBorNotifications($letter['bor_id']);
      if ($prefs['SMS_HOLD_SHELF'] == 'Y') {

	$sms_message = $smarty->fetch('sms/holdshelf.plain');

	echo "Sending SMS to " . $letter['mobnr'] . " for title " . $letter['title'] . "\n";
	$log->write("Sending SMS to " . $letter['mobnr'] . " for title " . $letter['title']);
	
	if (is_object($sr)) {
	  if ($sms_id = $sr->sendSMS($letter['mobnr'],
				     $sms_message,
				     HOLDSHELF_SMS_TARIFF,
				     true)) {
	    echo "SMS id: " . $sms_id . "\n";
	    $log->write("SMS id: $sms_id");

	    $wtw->logHoldShelfSMS($letter['letter_file'], $letter['hold_shelf_id'], $letter['bor_id'], $letter['mobnr'], $sms_id, $sms_message);
	    $sentSMS = true;
	    $letterSent = true;
	  }
	}
      }
    }
    
    if(!$sentEmail && !$sentSMS) {
    
      // Send hold shelf notification by snail mail
      echo "POST\n";
      echo "Sending POST for title " . $letter['title'] . "\n";
      $log->write("Sending POST for title " . $letter['title']);

      // Rename letter to pc43[unique].resbrev e.g.: pc4336691.resbrev
      // And move it to /exlibris/aleph/u16_1/nor50/print/
      // remember to insert ## - XML_XSL as the first line
      // <form-format/> must be set to 00: <form-format>00</form-format>
      // to make letter print at returns department
      $filename = HOLD_SHELF_LETTERS_SEND_DIR . uniqid('wtw-') . '.resbrev';
      echo "New file: $filename\n";
      $log->write("New file: $filename");

      $pattern[] = '/<form-format>(.+)<\/form-format>/Us';
      $replacement[] = '<form-format>00</form-format>';

      $xml_data = file_get_contents(HOLD_SHELF_LETTERS_DIR . $letter['letter_file']);
      $xml_data = preg_replace($pattern, $replacement, $xml_data);

      $result = file_put_contents($filename, "## - XML_XSL\n$xml_data");
      echo "File created: $result";

      // Chown
      chown($filename, 'aleph');
      // Chgrp
      chgrp($filename, 'exlibris');
      // Chmod file
      chmod($filename, 0755);
      
      $wtw->logHoldShelfPost($letter['letter_file'], $letter['hold_shelf_id'], $letter['bor_id']);
      $letterSent = true;
    }

    // Should we unlink and copy the letter file
    if ($letterSent) {
      if (!$hs_logger->copyFile($letter['letter_file'])) {
	$log->write("Could not copy letter file {$letter['letter_file']} to " . HOLD_SHELF_LETTERS_LOG_DIR . date('Y/m/d'));
      }
      // We don't care if file is not copied. We unlink anyway.
      if (!$hs_logger->unlinkSourceFile($letter['letter_file'])) {
	$log->write("Could not delete letter file {$letter['letter_file']} from " . HOLD_SHELF_LETTERS_DIR);
      }
    }

  }

  oci_close($db_conn);

} else {
  echo "Nothing to do.\n";
}

$lock->release();
?>
