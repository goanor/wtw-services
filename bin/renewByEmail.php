#!/usr/local/bin/php
<?php
require_once(dirname(__FILE__) . '/../config.inc.php');
require_once(CLASS_PATH . 'WTW_Aleph.class.php');
require_once(ALEPH_XSERVICES_LIB);
require_once(SWIFT_LIB);
require_once(CLASS_PATH . 'Smarty_Aleph.class.php');
require_once(CLASS_PATH . 'Lock.class.php');
require_once(CLASS_PATH . 'Logger.class.php');
require_once(CLASS_PATH . 'EmailLogger.class.php');
require_once(CLASS_PATH . 'EmailRenewLogger.class.php');

// FIXME: Users who have forwarded to another mail address will not
// be able to renew loans. Send user a message.
//

$log = new Logger(LOG_DIR . basename(__FILE__, '.php') . '.log');
$log_mails = new Logger(LOG_DIR . basename(__FILE__, '.php') .
                        '_mails_' . date('Y-m-d') . '.log');

$lock = new Lock(basename(__FILE__, '.php'));
if ($lock->isActive()) {
  $log->write("Lock is set. Another process is running. Aborting.");
  exit;
}
$lock->set();

// Could not get ��� to work in subject and adress with utf-8
// Possible Swift bug?
Swift_Preferences::getInstance()->setCharset('iso-8859-1');

$transport = Swift_MailTransport::newInstance();
$mailer = Swift_Mailer::newInstance($transport);

$message = Swift_Message::newInstance();
$message->setFrom(array(EMAIL_GENERAL_ADDRESS =>
                        EMAIL_GENERAL_ADDRESS_NAME));

$message->setReturnPath(EMAIL_RETURN_PATH);

$email_logger = new EmailLogger(EMAIL_LOG_DIR . date('Y/m/d'));

$db_conn = ocilogon(ORACLE_USERNAME, ORACLE_PASSWORD);

$wtw = new WTW_Aleph($db_conn);
$renew_logger = new EmailRenewLogger($db_conn);
$aleph = new AlephXService();

// Mailkotoer
$epostkonto = array('mailserver' => EMAIL_RENEW_IMAP_SERVER,
		    'brukernavn' => EMAIL_RENEW_IMAP_USER,
		    'passord' => EMAIL_RENEW_IMAP_PASS);


imap_timeout(IMAP_OPENTIMEOUT, 10);

$mbox = imap_open('{' . $epostkonto['mailserver'] . '}INBOX',
		  $epostkonto['brukernavn'],
		  $epostkonto['passord'],
		  OP_DEBUG);

if (!$mbox) {
  $lock->release();
  exit("can't connect: " . imap_last_error());
}
		       

$smarty = new Smarty_Aleph();
$messages = imap_sort($mbox, SORTFROM, false);
if (count($messages) > 0) {
  $log->write("\nProcessing " . count($messages));
}

foreach ($messages as $msgno) {
  $header = imap_header($mbox, $msgno);
  $struct = imap_fetchstructure($mbox, $msgno);

  $from_email_address = "{$header->from[0]->mailbox}@{$header->from[0]->host}";
  $log->write("From: $from_email_address");
  // Lets find the loaner. We use the following methods to try to
  // locate the right record
  // 1. In-Reply-To header
  // 2. ID inside of mail
  // 3. Mail address
  //
  // If all of them fail send notice to user

  $loans = array();
  // 1. In-Reply-To header
  if (!empty($header->in_reply_to)) {
    $email_id = str_replace(array('<', '>', ' '), '', $header->in_reply_to);
    $loans = $wtw->getLastNotifiedLoansByEmailId($email_id);
    echo "$email_id\n";
  }

  // 2. Message-ID inside of mail


  // 3. Mail address
  if (count($loans) == 0) {
    $loans = $wtw->getLastNotifiedLoansByEmailAddress($from_email_address);
    echo "$email_address\n";
  }
  
  print_r($loans);

  $num_loans = count($loans);
  $log->write("Loans: $num_loans");
  if ($num_loans > 0) {

    // Get user information
    $bor_id = $wtw->getBorIdByLoanNr($loans[0]);
    $xservice_bor = $aleph->borInfo($bor_id, '', ALEPH_ADM_DB);
    $bor_nr = $wtw->getBorNrFromBorId($bor_id);
    $smarty->assign('bor_nr', $bor_nr);
    $smarty->assign('user', $xservice_bor);

    $log->write("Bor id/nr: $bor_id / $bor_nr");

    $renewed_loans = 0;
    $processed_loans = array();
    $doc = null;
    foreach ($loans as $l) {
      $doc_number = substr($l, 0, 9);
      $item_sequence = substr($l, 9);
      
      $current_loan = array();
      $current_loan['loan_number'] = $wtw->getLoanNrByRecKey($l);
      $current_loan['doc_number'] = $doc_number;
      $current_loan['item_sequence'] =  $item_sequence;

      // Get the doc number for Z13 (NOR01) from NOR50
      // If ILL-loan use NOR20
      /*
      $item_status = $wtw->getItemStatusByLoanNr($l);
      $lnk_lib = ALEPH_ADM_DB;
      if ($item_status == TAB15_ILL_ITEM_STATUS) {
	$lnk_lib = ALEPH_ILL_DB;
      }
      */
      $doc_norXX = $aleph->findDocumentLKR($doc_number, ALEPH_ADM_DB);      
      $doc = $aleph->findDocument($doc_norXX['b'], $doc_norXX['l']);
      $current_loan['document'] = $doc;
      $current_loan['due'] = strtotime($wtw->getDueDateByLoanNr($l));
      print_r($doc);

      // exit;
      // continue;
      // Try to renew
      if ($xmldata = file_get_contents("http://91.243.69.14/X?op=renew&library=" . ALEPH_ADM_DB . "&doc_number=$doc_number&item_sequence=$item_sequence")) {
	
	$xml = new SimpleXMLElement($xmldata);
	
	// Build array with Z36_REC_ID and STATUS
	if (empty($xml->error) && 
	    !empty($xml->reply) && !empty($xml->{'due-date'})) {
	  $due = strtotime((string) $xml->{'due-date'});
	  $current_loan['due'] = $due;
	  $current_loan['result'] = ALEPH_OK;
	  $renewed_loans++;

	  // We should consider updating the
	  // Z36_RENEW_MODE to EMAIL
	  

	}
	else {
	  // Her er det noe feil
	  if (!empty($xml->error) &&
	      stripos($xml->error, 'can not be found') !== false) {
	    // Not found
	    $current_loan['result'] = ALEPH_ITEM_NOT_FOUND;
	  }
	  else if (!empty($xml->error) &&
		   stripos($xml->error, 'date must be bigger') !== false) {
	    // Ny dato blir samme som innleveringdato
	    $current_loan['result'] = ALEPH_RENEW_WILL_BE_SAME_DATE;
	  }
	  else if (!empty($xml->{'error-text-1'})) {
	    // Lanet kan ikke fornyes flere ganger
	    $current_loan['result'] = ALEPH_RENEW_MAX_LIMIT_REACHED;
	  }
	  else if (!empty($xml->{'error-text-2'})) {
	    // Kan ikke fornyes
	    $current_loan['result'] = ALEPH_RENEW_ITEM_DECLARED_LOST;
	  }
	  else {
	    // Kan ikke fornyes
	    $current_loan['result'] = ALEPH_RENEW_GENERAL_ERROR;
	    echo $xml->error . "\n";
	  } 
	}
      }
      else {
	// No XML-data
	$current_loan['result'] = ALEPH_RENEW_GENERAL_ERROR;
      }
    
      $processed_loans[] = $current_loan;
    }

    // Get info for the document IDs.
    

    // Build and send email
    $message->setSubject("$renewed_loans av $num_loans ble fornyet");
    $message->setTo($from_email_address);

    $log->write("$renewed_loans of $num_loans renewed");

    $smarty->assign('renewed_loans', $renewed_loans);
    $smarty->assign('num_loans', $num_loans);
    $smarty->assign('loans', $processed_loans); 
    $html_body = $smarty->fetch('email/renewbyemail.html');
    $message->setBody($html_body, 'text/html');
    $plain_body = html_entity_decode(strip_tags($html_body));
    $message->addPart($plain_body, 'text/plain');

    $log_mails->write($message->toString());

    $msgId = $message->getHeaders()->get('Message-ID');
    $message_id = $msgId->getId();
    // Write email to file
    if ($email_logger->writeToFile($message_id,
				   $html_body) > 0) {
      $log->write("Wrote email body to file: $message_id");
    }


    echo $sent_result = $mailer->send($message);
    $log->write("Mail sent: $sent_result");
    // break;
    if ($sent_result) {
      $email_log_id = $wtw->logEmail($message_id, $from_email_address);

      foreach ($processed_loans as $p) {
	
	$renew_result = 'F';
	if ($p['result'] == ALEPH_OK) {
	  $renew_result = 'S';
	}
	$renew_logger->logEmailRenew($p['loan_number'],
				     $bor_id,
				     $renew_result,
				     '',
				     date('Ymd', $p['due']),
				     $email_log_id);
      }

    }

  }
  else {
    // TODO: Send user notice. No loans found
    

  }

  // Should we store the incoming email, or should we just delete it?
  if (EMAIL_RENEW_SAVE_INCOMING) {
    // Create mailbox if not exists
    $folders = imap_listmailbox($mbox, '{' . EMAIL_RENEW_IMAP_SERVER . '}',
				"*");
    $archive_mbox = '{'. EMAIL_RENEW_IMAP_SERVER . '}' .
       EMAIL_RENEW_SAVE_INCOMING_IN_MAILBOX;
    if (!in_array($archive_mbox, $folders)) {
      @imap_createmailbox($mbox, imap_utf7_encode($archive_mbox));
    }
    imap_mail_move($mbox, $msgno, EMAIL_RENEW_SAVE_INCOMING_IN_MAILBOX);
  }
  else {
    imap_delete($mbox, $msgno);
  }
}

imap_expunge($mbox);
imap_close($mbox);

oci_close($db_conn);

$lock->release();
?>
