#!/usr/local/bin/php
<?php
/**
*
* checkHoldShelfSMS.php
* Checks the status of sent hold shelf SMS
* Marks every SMS as success or failure, or postpones marking if no status
* could be fetched
* When an SMS is marked as failed, an email is
* sent to the library with information about the 
* SMS and which loans the SMS applied to
*
*/
require_once(dirname(__FILE__) . '/../config.inc.php');
require_once(CLASS_PATH . 'WTW_Aleph.class.php');
require_once(CLASS_PATH . 'SMSRespons.class.php');
require_once(SWIFT_LIB);
require_once(CLASS_PATH . 'Smarty_Aleph.class.php');
require_once(CLASS_PATH . 'Lock.class.php');
require_once(CLASS_PATH . 'Logger.class.php');
require_once(ALEPH_XSERVICES_LIB);


$log = new Logger(LOG_DIR . basename(__FILE__, '.php') . '.log');

// Lock mechanism to prevent multiple instances of this job 
// running in parallell
$lock = new Lock(basename(__FILE__, '.php'));
if ($lock->isActive()) {
  $log->write("Lock is set. Another process is running. Aborting.");
  exit;
}
$lock->set();

$db_conn = ocilogon(ORACLE_USERNAME, ORACLE_PASSWORD);
  
$wtw = new WTW_Aleph($db_conn);
$sr = new SMSRespons(SMSRESPONS_USERNAME, SMSRESPONS_PASSWORD);
$smarty = new Smarty_Aleph();
$aleph = new AlephXService();

$transport = Swift_MailTransport::newInstance();
$mailer = Swift_Mailer::newInstance($transport);
  
// Could not get ÒÒÒ to work in subject and adress with utf-8
// Possible Swift bug?
Swift_Preferences::getInstance()->setCharset('iso-8859-1');

// Fetch all messages that don't have a status (NULL)
$messages = $wtw->getHoldShelfSMSWithoutStatus();
$log->write("Processing: " . count($messages));
$i = -1;

$failures = array();

foreach($messages as $message) {
  $i++;

  echo "id: " . $message['ID'] . ", wtw-id: " . $message['MESSAGE_ID'] . "\n";
  echo "age: " . $message['AGE'] . "\n";
  $status = $sr->getStatus(trim($message['MESSAGE_ID']));
  echo "status: " . $status . "\n";
  
  if($status == "SUCCESS") {
    // Mark as success
    echo "Setting status to 'S'\n";
    $log->write("Setting message " . $message['ID'] . " (" . $message['MESSAGE_ID'] . ") to success");
    $wtw->setHoldShelfSMSStatus($message['ID'],'S');
    
  } else if($status != "NULL" || $message['AGE'] > 0) {
    // Mark as failed
    echo "Setting status to 'F'\n";
    $log->write("Setting message " . $message['ID'] . " (" . $message['MESSAGE_ID'] . ") to failed");
    $wtw->setHoldShelfSMSStatus($message['ID'],'S');
    $failures[] = $message;
    
  } else {
    // Postpone decision
    echo "Doing nothing\n";
    $log->write("Postponing message " . $message['ID'] . " (" . $message['MESSAGE_ID'] . ") because status is NULL");
  }
}

// Process failed SMS
// Send en email to the library for each
foreach($failures as $failure) {
  $log->write("Sending failure email for " . $failure['ID']);
  $message = Swift_Message::newInstance();

  try {    
    $message->setFrom(array(CHECKSMS_FROM_ADDRESS => CHECKSMS_FROM_NAME));
    $message->setReturnPath(CHECKSMS_RETURN_PATH);
    $message->setSubject("Varsling om SMS som ikke kom frem");
    $message->setTo(CHECKSMS_TO);
      
    // Fetch title and author for all notifications in SMS  
    $loans = $wtw->getLastHoldShelfLoanBySMS($failure['ID']);
    $loandata = array();

    //$borId = $wtw->getBorIdByLoanNr($loans[0]);
    $borId = $failure['BOR_ID'];
    $borNr = $wtw->getBorNrFromBorId($borId);
    $smarty->assign('borNr',$borNr);   
    foreach($loans as $loan) {

      $doc_number = substr($loan, 0, 9);
      $doc_norXX = $aleph->findDocumentLKR($doc_number, ALEPH_ADM_DB);
      $doc = $aleph->findDocument($doc_norXX['b'], $doc_norXX['l']);
      $due = strtotime($wtw->getDueDateByLoanNr($loan));
      
      $loandata[] = array("title" => $doc->title, "author" => $doc->author);
      $log->write("BorNr: " . $borNr . ", Due: " . $due . ", Title: " . $doc->title . ", " . $doc->author); 
    }

    
    print_r($loandata);
    $smarty->assign('mobile',$failure['RECIPIENT']);  
    $smarty->assign('loans',$loandata);  
    $html_body  = $smarty->fetch('email/checkholdshelfsms.html');
    $plain_body = $smarty->fetch('email/checkholdshelfsms.plain');
      
    $message->setBody($html_body, 'text/html');
    $message->addPart($plain_body, 'text/plain');  

    // Send email to library
    $sent_result = $mailer->send($message);
    if(!$sent_result) {
      $log->write("Couldn't send failure email for " . $failure['ID']);  
    }
  } catch(Exception $e) {
    $log->write("Couldn't send failure email for " . $failure['ID']);
  }

  unset($message);
}


?>
