#!/usr/local/bin/php
<?php
/**
*
* overdueEmail.php
* Sends email to remind borrowers that their loans are overdue
*
* Description of steps in job:
* 1. Define letters which are sent for loans that are a certain
* number of days overdue
* 2. For each letter fetch borrowers with loans that are a certain
* number of days overdue
* 2.1 For each borrower, if he has an email address, fetch all loans and create email 
* 2.1.1 Send email
* 2.1.2 Tag loans in Aleph database
*
*/
require_once(dirname(__FILE__) . '/../config.inc.php');
require_once(CLASS_PATH . 'WTW_Aleph.class.php');
require_once(CLASS_PATH . 'SMSRespons.class.php');
require_once(SWIFT_LIB);
require_once(CLASS_PATH . 'Smarty_Aleph.class.php');
require_once(ALEPH_XSERVICES_LIB);
require_once(CLASS_PATH . 'Lock.class.php');
require_once(CLASS_PATH . 'Logger.class.php');
require_once(CLASS_PATH . 'EmailLogger.class.php');


// Only send reminders to borrowers with one of the following statuses:
$for_bor_status = explode(',',FOR_BOR_STATUS);

// Lock mechanism to prevent multiple instances of this job 
// running in parallell
$lock = new Lock(basename(__FILE__, '.php'));
if ($lock->isActive()) {
  exit("Lock is set. Another process is running. Aborting.");
}
$lock->set();

// Specify one or more letters (messages) that should be sent 
// after a certain number of days overdue
$letters[] = array('number' => 1,
		   'days_overdue' => 1,
		   'email_template_html' => 'overduemessage_1.html',
		   'email_template_plain' => 'overduemessage_1.plain');
$letters[] = array('number' => 2,
                   'days_overdue' => 14,
                   'email_template_html' => 'overduemessage_2.html',
		   'email_template_plain' => 'overduemessage_2.plain');


$email_logger = new EmailLogger(EMAIL_LOG_DIR . date('Y/m/d'));

$db_conn = ocilogon(ORACLE_USERNAME, ORACLE_PASSWORD);

$wtw = new WTW_Aleph($db_conn);
$aleph = new AlephXService();
 

foreach ($letters as $letter) {
  $log = new Logger(LOG_DIR . basename(__FILE__, '.php') .
		    '_letter' . $letter['number'] . '.log');
  $log_mails = new Logger(LOG_DIR . basename(__FILE__, '.php') .
			  '_mails_' . date('Y-m-d') . 
			  '_letter' . $letter['number'] . '.log');
  
  $transport = Swift_MailTransport::newInstance();
  $mailer = Swift_Mailer::newInstance($transport);
  
  // Could not get ��� to work in subject and adress with utf-8
  // Possible Swift bug?
  Swift_Preferences::getInstance()->setCharset('iso-8859-1');
     
  $start_time = microtime(true);
  $bors = $wtw->getOverdueBorsEmail($letter['number'],
				    $letter['days_overdue'],
				    $for_bor_status);

  $num_overdue = count($bors);
  echo "$num_overdue\n";
  
  if ($bors && $num_overdue > 0) {
    $log->write("Starting to process $num_overdue records.");
    
    $stmt = $wtw->prepareOverdueQueryEmail($letter['number'],
					   $letter['days_overdue'],
					   $for_bor_status);
    $stmt_ill = $wtw->prepareOverdueQueryEmail($letter['number'],
					       $letter['days_overdue'],
					       $for_bor_status,
					       'ILL');
    $smarty = new Smarty_Aleph();
    $i=0;
    $num_sent=0;    
    foreach ($bors as $b) {
      // echo "Getting borInfo()\n";
      $xservice_bor = $aleph->borInfo($b['Z36_ID'], '', ALEPH_ADM_DB);
      $to_email = (string) $xservice_bor->address->email;

      // Validate email
      if (!WTW_Aleph::isEmail($to_email)) {
	if (!empty($to_email)) {
	  $log->write("Invalid email for {$b['Z36_ID']}: $to_email");
	}
	continue;
      }
      
      // echo "$to_email - {$b['Z36_ID']}\n";
      // REMOVE THIS: START
      //if (!in_array($to_email, array('christian@wtw.no',
	//			     'laverton@wtw.no',
	//			     'christian@secureit.no'))) {
	//continue;
      //}
      echo "$to_email - {$b['Z36_ID']}\n";
      echo "\nDO IT!\n";
      // END

      // This could have been replaced by X-service call,
      // but it doesn't contain info from our WTW notify tables.
      echo "getLoans Local\n";
      $loans_local = $wtw->getLoans($b['Z36_ID'], $stmt);
      echo "getLoans Ill\n";
      $loans_ill = $wtw->getLoans($b['Z36_ID'], $stmt_ill);
      
      // print_r($loans_local);
      // print_r($loans_ill);
      
      $loans = array_merge($loans_local, $loans_ill);
      
      print_r($loans);

      $num_loans = count($loans);
      echo "Num: $num_loans\n";

      // Convert to unixtime
      for ($j=0; $j<count($loans); $j++) {
	$loans[$j]['Z36_DUE_DATE_UNIXTIME'] =
	   strtotime($loans[$j]['Z36_DUE_DATE']);
      }
      
      $bor_nr = $wtw->getBorNrFromBorId($b['Z36_ID']);
      $smarty->assign('bor_nr', $bor_nr);
      $smarty->assign('bor', $b);
      $smarty->assign('user', $xservice_bor);
      $smarty->assign('loans', $loans);
      $html_body = $smarty->fetch('email/' . $letter['email_template_html']);
      $plain_body = $smarty->fetch('email/' . $letter['email_template_plain']);

      $sent_result = false; 
      try {
	$message = Swift_Message::newInstance();
	$message->setFrom(array(EMAIL_RENEW_ADDRESS =>
				EMAIL_GENERAL_ADDRESS_NAME));
	$message->setReturnPath(EMAIL_RETURN_PATH);
	$message->setSubject("Du har $num_loans l�n som skulle v�rt levert.");
	$message->setTo($to_email);
	
	$msgId = $message->getHeaders()->get('Message-ID');
	$message_id = $msgId->getId();
	
	$message->setBody($html_body, 'text/html');
	$message->addPart($plain_body, 'text/plain');
	
	$log_mails->write($message->toString());

	
	// Write email to file
	if ($email_logger->writeToFile($message_id,
				 $html_body) > 0) {
	  $log->write("Wrote email body to file: $message_id");
	}


	if (!EMAIL_OVERDUE_DISABLE_SENDING) {
	  $sent_result = $mailer->send($message);
	  
	  if ($sent_result) {
	    
	    if (true) {
	      // Log this email
	      $email_log_id = $wtw->logEmail($message_id, $to_email);
	      
	      // Insert into notification log
	      if ($email_log_id) {
		$wtw->tagEmailOverdueNotificationSent($email_log_id, $b,
						      $loans,
						      $letter['number']);
	      }
	      
	      // Update Z36 with new Z36_LETTER_DATE and Z36_LETTER_NUMBER
	      // Also make a notice on how letter was sent
	      foreach ($loans as $l) {
                $note = $letter['number'] . ".purring: $to_email";
		$affected_rows = $wtw->tagZ36LetterSent($l['Z36_REC_KEY'],date('Ymd'),$note);
		if ($affected_rows == 0) {
		  $log->write("tagZ36LetterSent() for " .
			      "{$l['Z36_REC_KEY']} failed");
		}
		else if ($affected_rows == 1) {
		  $log->write("tagZ36LetterSent() for " .
			      "{$l['Z36_REC_KEY']} success");
		}
		else if ($affected_rows > 1) {
		  $log->write("tagZ36LetterSent() for " .
			      "{$l['Z36_REC_KEY']} affected more than 1 row.");
		}
	      }
	    }

	    $num_sent++;
	  }
	  
	}
	
	echo "Til: $to_email\n";
	echo "Message-id: $message_id\n";
	echo "Sent: $sent_result\n";
	$log->write("To: $to_email, " .
		    "Message-id: $message_id, " .
		    "Sent: $sent_result");
	
      }
      catch (Exception $e) {
	$log->write("FAILED! To: $to_email, " .
		    "Message-id: $message_id, " .
		    "Sent: $sent_result");
	echo $e->getTraceAsString();
      }
      
      // Free the message object
      unset($message);
      
      $i++;
    }
    echo "Num: $i\n";
    
    $log->write("Done. $num_sent of $i messages sent. Time used: " .
		((float) microtime(true) - (float) $start_time));
  }
  else {
    echo "Nothing to do...\n";
    $log->write("Nothing to do...");
  }
}

oci_close($db_conn);

$lock->release();
?>
