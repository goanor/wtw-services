#!/root/php/php-5.2.6/sapi/cli/php
<?php
require_once(dirname(__FILE__) . '/../../config.inc.php');
require_once(CLASS_PATH . 'WTW_Aleph.class.php');


$db_conn = ocilogon(ORACLE_USERNAME, ORACLE_PASSWORD);

$wtw = new WTW_Aleph($db_conn);

$loan_nr = $argv[1];
if (!empty($loan_nr)) {
  $bor_id = $wtw->getBorIdByBorNr($loan_nr);
}

echo "$loan_nr --> $bor_id\n";
?>