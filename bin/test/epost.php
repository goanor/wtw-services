#!/root/php/php-5.2.6/sapi/cli/php
<?php
require_once(dirname(__FILE__) . '/../config.inc.php');
require_once(SWIFT_LIB);

$transport = Swift_MailTransport::newInstance();
$mailer = Swift_Mailer::newInstance($transport);

$message = Swift_Message::newInstance(); 
$message->setSubject('Svampnisse');
$message->setFrom(array('christian@wtw.no' => 'Christian Aune Thomassen'));
$message->setTo(array('christian@secureit.no' => 'Christian'));
$message->setReturnPath('christian@wtw.no');

$headers = $message->getHeaders();
$headers->addTextHeader('X-Loan-Number', '12345');

//Or set it after like this
$message->setBody('<html><body><div style="background-color:green;:100%">My amazing horse with eye ball of laks</div></body></html>', 'text/html');

//Add alternative parts with addPart()
$message->addPart('My amazing horse with eye ball of laks', 'text/plain');


$numSent = $mailer->batchSend($message);
echo "$numSent\n";

?>