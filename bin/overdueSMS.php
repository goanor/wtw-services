#!/usr/local/bin/php
<?php
/**
*
* overdueSMS.php
* Sends SMS to remind borrowers that their loans are overdue
*
* Description of steps in job:
* 1. Define letters which are sent for loans that are a certain
* number of days overdue
* 2. For each letter fetch borrowers with loans that are a certain
* number of days overdue
* 2.1 For each borrower, if he does not have an email address
* but has a mobile number, fetch all loans and create SMS
* 2.1.1 Send SMS
* 2.1.2 Tag loans in Aleph database
*
*/
require_once(dirname(__FILE__) . '/../config.inc.php');
require_once(CLASS_PATH . 'WTW_Aleph.class.php');
require_once(dirname(__FILE__) . '/../lib/WtwSmsConnect/wtwsmssend.class.php');
require_once(SWIFT_LIB);
require_once(CLASS_PATH . 'Smarty_Aleph.class.php');
require_once(ALEPH_XSERVICES_LIB);
require_once(CLASS_PATH . 'Lock.class.php');
require_once(CLASS_PATH . 'Logger.class.php');

// Only send reminders to borrowers with one of the following statuses:
$for_bor_status = explode(',',FOR_BOR_STATUS);

// Lock mechanism to prevent multiple instances of this job 
// running in parallell
$lock = new Lock(basename(__FILE__, '.php'));
if ($lock->isActive()) {
  exit("Lock is set. Another process is running. Aborting.");
}
$lock->set();

// Specify one or more letters (messages) that should be sent 
// after a certain number of days overdue
$letters = array();
$letters[] = array('number' => 1,
		   'days_overdue' => 1,
		   'sms_template' => 'overduemessage_1.plain');


$db_conn = ocilogon(ORACLE_USERNAME, ORACLE_PASSWORD);
  
$wtw = new WTW_Aleph($db_conn);
$aleph = new AlephXService();
$smarty = new Smarty_Aleph();

foreach ($letters as $letter) {

  // general logging
  $log = new Logger(LOG_DIR . basename(__FILE__, '.php') .
		    '_letter' . $letter['number'] . '.log');

  // log each message                    
  $log_sms = new Logger(LOG_DIR . basename(__FILE__, '.php') .
			  '_sms_' . date('Y-m-d') . 
			  '_letter' . $letter['number'] . '.log');
  
    
  $start_time = microtime(true);
  // Retrieve all borrowers that have overdue loans and the right status
  $bors = $wtw->getOverdueBorsSms($letter['number'],
				    $letter['days_overdue'],
				    $for_bor_status);
  print_r($bors);

  $num_bors = count($bors);
  echo "$num_bors\n";
  
  if ($bors && $num_bors > 0) {
    $log->write("Starting to process $num_bors records.");

    // prepare statement for fetching all overdue local loans
    $stmt = $wtw->prepareOverdueQuerySms($letter['number'],
					   $letter['days_overdue'],
					   $for_bor_status);
                                           
    // prepare statement for fetching all overdue remote loans                                           
    $stmt_ill = $wtw->prepareOverdueQuerySms($letter['number'],
					       $letter['days_overdue'],
					       $for_bor_status,
					       'ILL');
    $i = -1;
    $num_sent = 0;
    
    foreach ($bors as $b) {
      $i++;

      echo "Getting borInfo()\n";
      // Retrieve borrower information through Aleph Xservice
      $xservice_bor = $aleph->borInfo($b['Z36_ID'], '', ALEPH_ADM_DB);
      $to_email = (string) $xservice_bor->address->email;
      $to_mobile = (string) $xservice_bor->address->mobilephone;

      // Check if borrower has a valid email address
      // If so, we don't send SMS
      if (!empty($to_email) && WTW_Aleph::isEmail($to_email)) {
        $log->write("Borrower has email: {$b['Z36_ID']}: $to_email");
        continue;
      } else {
        $log->write("Borrower does not have email: {$b['Z36_ID']}");
      }

      // Check if borrowser has valid mobile
      // If not, we can't send anything
      if(!WTW_Aleph::isMobile($to_mobile)) {
        if(!empty($to_mobile)) {
	  $log->write("Invalid mobilenumber for {$b['Z36_ID']}: $to_mobile");
        }
        continue;
      }
      
      echo "$to_mobile - {$b['Z36_ID']}\n";
      // REMOVE THIS: START
      //if ($to_mobile != '95187147' && $to_mobile != '93213659' && $to_mobile != '98809145') {
	// continue;
      //}
      // END

      // This could have been replaced by an X-service call,
      // but then we would have to check against our WTW 
      // notify tables afterwards 
      
      echo "getLoans Local\n";
      // Retrieve all local loans
      $loans_local = $wtw->getLoans($b['Z36_ID'], $stmt);
      echo "getLoans Ill\n";
      // Retrieve all remote loans
      $loans_ill = $wtw->getLoans($b['Z36_ID'], $stmt_ill);
      
      $loans = array_merge($loans_local, $loans_ill);
      
      print_r($loans);

      $num_loans = count($loans);
      echo "Num: $num_loans\n";
    
      // Convert to unixtime
      for ($j=0; $j<count($loans); $j++) {
	$loans[$j]['Z36_DUE_DATE_UNIXTIME'] =
	   strtotime($loans[$j]['Z36_DUE_DATE']);
      }
      
      // Fetch borrower number
      $bor_nr = $wtw->getBorNrFromBorId($b['Z36_ID']);
      $smarty->assign('bor_nr', $bor_nr);
      $smarty->assign('bor', $b);
      $smarty->assign('user', $xservice_bor);
      $smarty->assign('loans', $loans);
      // Fetch letter template
      $message = $smarty->fetch('sms/' . $letter['sms_template']);
         
      if($message) {

        $log_sms->write("To: $to_mobile, Message: $message");

        // Try sending the SMS unless sending is disbled
	$serviceCode = SMS_GAS_CODE_ZERO;
	if (OVERDUE_SMS_TARIFF > 0) {
	  $serviceCode = SMS_GAS_CODE;
	}


        if (!SMS_OVERDUE_DISABLE_SENDING &&
	    $sms_id = WtwSmsSend::sendSms(SMS_GATEWAY, SMS_CODEWORD,
					  SMS_API_KEY, '',
					  '47', $to_mobile,
					  $message,
					  OVERDUE_SMS_TARIFF,
					  $serviceCode, SMS_TEXT_DESC)) {
	  if ($sms_id) {
	    // Log this sms
            $sms_log_id = $wtw->logSms($sms_id, $to_mobile, $message);
	    
	    // Insert into notification log
	    if ($sms_log_id) {
	      $wtw->tagSMSOverdueNotificationSent($sms_log_id, $b, $loans, $letter['number']);
	    }
	    
	    // Update Z36 with new Z36_LETTER_DATE and Z36_LETTER_NUMBER
	    // Also make a notice on how letter was sent
	    foreach ($loans as $l) {
              $note = $letter['number'] . ".purring: $to_mobile";
	      $affected_rows = $wtw->tagZ36LetterSent($l['Z36_REC_KEY'],date('Ymd'),$note);
	      if ($affected_rows == 0) {
	        $log->write("tagZ36LetterSent() for {$l['Z36_REC_KEY']} failed");
	      } else if ($affected_rows == 1) {
	        $log->write("tagZ36LetterSent() for {$l['Z36_REC_KEY']} success");
	      } else if ($affected_rows > 1) {
	        $log->write("tagZ36LetterSent() for {$l['Z36_REC_KEY']} affected more than 1 row.");
	      }
	    }
	  

	    $num_sent++;
	  } else {
            $log->write("Sending SMS to $to_mobile failed");
          }
	      
        }

      }
     
    }
    echo "Num: $i\n";
    
    $log->write("Done. $num_sent of $i messages sent. Time used: " .
		((float) microtime(true) - (float) $start_time));
  }
  else {
    echo "Nothing to do...\n";
    $log->write("Nothing to do...");
  } 
}

oci_close($db_conn);

$lock->release();
?>
