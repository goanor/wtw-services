<?php
// Custom PHP-errorlogging
ini_set('error_reporting', E_ALL ^ E_NOTICE);
error_reporting(E_ALL ^ E_NOTICE);
ini_set('log_errors', true);
ini_set('html_errors', false);
ini_set('error_log', dirname(__FILE__) . '/log/phperror.log');
ini_set('display_errors', true);
ini_set('date.timezone', 'Europe/Oslo');
ini_set('memory_limit', '512M');
// Config
// Check the setting of safe_mode_allowed_env_vars =
// it must be empty to allow for putenv
putenv('ORACLE_HOME=/exlibris/app/oracle/product/102');
putenv('ORACLE_SID=aleph18');
// For at ��� skal funke skikkelig m� NLS_LANG settes riktig:
putenv('NLS_LANG=NORWEGIAN_NORWAY.WE8ISO8859P1');

define('ORACLE_USERNAME', 'WTW');
define('ORACLE_PASSWORD', 'WTW');

define('SMSRESPONS_USERNAME', 'aleph');
define('SMSRESPONS_PASSWORD', 'AlleP45z');

define('DAYS_BEFORE_DUE', 3);

define('CLASS_PATH', dirname(__FILE__) . '/classes/');
define('LIB_PATH', dirname(__FILE__) . '/lib/');
define('SWIFT_LIB', LIB_PATH . 'Swift/lib/swift_required.php');
define('SMARTY_LIB', LIB_PATH . 'Smarty/libs/Smarty.class.php');
define('SMARTY_TEMPLATE_DIR', dirname(__FILE__) . '/templates/');
define('SMARTY_TEMPLATE_C_DIR', dirname(__FILE__) . '/cache/template_c/');
define('ALEPH_XSERVICES_LIB', LIB_PATH . 'AlephXServices/AlephXService.class.php');

define('LOG_DIR', dirname(__FILE__) . '/log/');
define('EMAIL_LOG_DIR', dirname(__FILE__) . '/log/email/');
define('TMP_DIR', dirname(__FILE__) . '/tmp/');

define('Z103_LKR_LIBRARY', 'NOR01');

// Item-status for fjernl�n
// Definert i /exlibris/aleph/u16_1/nor50/tab/tab15.nor
define('TAB15_ILL_ITEM_STATUS', 11);

define('EMAIL_PRE_DUE_WITH_PREFERENCES_ONLY', false);
define('EMAIL_PRE_DUE_DISABLE_SENDING', false);

define('EMAIL_OVERDUE_DISABLE_SENDING', false);
define('SMS_OVERDUE_DISABLE_SENDING', false);

define('HOLD_SHELF_LETTERS_DIR', '/exlibris/aleph/u18_1/nor50/print/wtw/');
define('HOLD_SHELF_LETTERS_SEND_DIR', '/exlibris/aleph/u18_1/nor50/print/');
//define('HOLD_SHELF_LETTERS_POST_STORE_DIR',
//       '/exlibris/aleph/u16_1/nor50/print/hhs/post/');
//define('HOLD_SHELF_LETTERS_SMS_STORE_DIR',
//       '/exlibris/aleph/u16_1/nor50/print/hhs/sms/');
//define('HOLD_SHELF_LETTERS_EMAIL_STORE_DIR',
//       '/exlibris/aleph/u16_1/nor50/print/hhs/email/');
define('HOLD_SHELF_LETTERS_LOG_DIR',dirname(__FILE__) . '/log/holdshelf/');

define('HOLD_SHELF_FROM_NAME', 'S�lvberget');
define('HOLD_SHELF_FROM_ADDRESS','biblioteket@stavanger-kulturhus.no');
define('HOLD_SHELF_RETURN_PATH','biblioteket@stavanger-kulturhus.no');

define('ALEPH_USR_DB', 'USR01');
define('ALEPH_ADM_DB', 'NOR50');
define('ALEPH_LIB_DB', 'NOR01');
define('ALEPH_ILL_DB', 'NOR20');

define('EMAIL_RENEW_ADDRESS', 'forny@stavanger-kulturhus.no');
define('EMAIL_GENERAL_ADDRESS', 'biblioteket@stavanger-kulturhus.no');
define('EMAIL_GENERAL_ADDRESS_NAME', 'S�lvberget');
define('EMAIL_RETURN_PATH', 'biblioteket@stavanger-kulturhus.no');


define('CHECKSMS_FROM_ADDRESS', 'biblioteket@stavanger-kulturhus.no');
define('CHECKSMS_FROM_NAME', 'S�lvberget');
define('CHECKSMS_RETURN_PATH', 'biblioteket@stavanger-kulturhus.no');
define('CHECKSMS_TO', 'biblioteket@stavanger-kulturhus.no');
//define('CHECKSMS_TO', 'laverton@wtw.no');

// define('EMAIL_RENEW_IMAP_SERVER', '217.68.125.166:143');
// define('EMAIL_RENEW_IMAP_USER', 'Forny');
// define('EMAIL_RENEW_IMAP_PASS', 'forS95ny');
define('EMAIL_RENEW_IMAP_SERVER', 'imap.svgkomm.svgdrift.no:143');
define('EMAIL_RENEW_IMAP_USER', 'fe10464');
// define('EMAIL_RENEW_IMAP_PASS', 'forS95ny');
define('EMAIL_RENEW_IMAP_PASS', 'So12mmer');
define('EMAIL_RENEW_SAVE_INCOMING', true);
define('EMAIL_RENEW_SAVE_INCOMING_IN_MAILBOX', 'INBOX.Arkiv');

define('ALEPH_OK', 1);
define('ALEPH_RENEW_ITEM_NOT_FOUND', 2);
define('ALEPH_RENEW_WILL_BE_SAME_DATE', 3);
define('ALEPH_RENEW_MAX_LIMIT_REACHED', 4);
define('ALEPH_RENEW_ITEM_DECLARED_LOST', 5);
define('ALEPH_RENEW_GENERAL_ERROR', 6);

define('FOR_BOR_STATUS',"'01', '02', '03', '06', '10', '11', '14', '30', '32'");

define('PREDUE_SMS_TARIFF', 0);
define('HOLDSHELF_SMS_TARIFF', 0);
define('OVERDUE_SMS_TARIFF', 0);

define('SMS_CODEWORD', 'SBFORNY');
define('SMS_GATEWAY', '2470');
define('SMS_API_KEY', 'Hdifh87ADJKFasd');
define('SMS_GAS_CODE', '05004'); // parking
define('SMS_GAS_CODE_ZERO', '89001'); // zero charged
define('SMS_TEXT_DESC', 'S�lvberget');

?>
