<?php


class EmailLogger {

  private $logdir = '';

  function __construct($dir) {
    $this->log_dir = $dir;
  }


  public function writeToFile($email_id,
			      $email_body) {
    // Generate directory to store file in
    // if not exists
    $file_name = $this->log_dir . '/' . $email_id;
    if ($this->mkdir()) {
      return file_put_contents($file_name,
			       $email_body);
    }

    return false;
  }


  private function mkdir() {

    if (!file_exists($this->log_dir) || !is_dir($this->log_dir)) {
      return mkdir($this->log_dir, 0777, true); // recursive
    }

    return true;
  }

}

?>