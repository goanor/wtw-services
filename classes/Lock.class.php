<?php

class Lock {
  
  private $_lockfile;
  private $_fp;

  function __construct($name) {
    $this->_lockfile = TMP_DIR . $name . '.lck';
    $this->_fp = fopen($this->_lockfile, "w+");
  }

  public function set() {
    return flock($this->_fp, LOCK_EX | LOCK_NB);
  }
 
  public function release() {
    flock($this->_fp, LOCK_UN); // release the lock
    fclose($this->_fp);
  }

  public function isActive() {
    if (!flock($this->_fp, LOCK_EX | LOCK_NB)) return true;
    
    return false;
  }

}


?>