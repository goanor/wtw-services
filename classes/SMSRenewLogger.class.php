<?php

/**
* Class SMSRenewLogger
*
*
*/
class SMSRenewLogger {

  private $dbConn = null;

  function __construct($dbConn) {
    $this->dbConn = $dbConn;
  }
 
  
  public function logSMSRenew($sms_id,$recipient,$tariff,$message_text,
			      $bor_id, $renews) {
    $wtw = new WTW_Aleph($this->dbConn);
    // insert log entry in WTW_SMS_LOG
    $sms_id = $wtw->logSMS($sms_id, $recipient, $message_text, $tariff);

    if($sms_id != 0) {
      // insert log entry in WTW_SMS_RENEW_LOG for each loan
      foreach($renews as $r) {
        $q = "INSERT INTO WTW.WTW_SMS_RENEW_LOG " .
           "(LOAN_NUMBER, BOR_ID, SENT_DATETIME, RESULT, RESULT_MESSAGE, NEW_DUE_DATE, SMS_LOG_ID) " .
           "VALUES " .
           "(:LOAN_NUMBER, :BOR_ID, SYSDATE, :RESULT, :RESULT_MESSAGE," .
	   "TO_DATE(:NEW_DUE_DATE, 'YYYYMMDD'), :SMS_LOG_ID)";
        $stmt = oci_parse($this->dbConn, $q);

        oci_bind_by_name($stmt, ':LOAN_NUMBER',
                         $r['loan_number'], -1);
        oci_bind_by_name($stmt, ':BOR_ID',
                         $bor_id, -1);
        oci_bind_by_name($stmt, ':RESULT',
                         $r['result'], -1);
        oci_bind_by_name($stmt, ':RESULT_MESSAGE',
                         $r['result_message'], -1);
        oci_bind_by_name($stmt, ':NEW_DUE_DATE',
                         date('Ymd', $r['due_date']), -1);
        oci_bind_by_name($stmt, ':SMS_LOG_ID',
                         trim($sms_id), -1);    

        oci_execute($stmt);
        
      }
      oci_free_statement($stmt);
    }
  }
    

}

?>
