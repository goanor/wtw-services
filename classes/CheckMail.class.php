<?php

class CheckMail {
  function getImageList($this_part, &$parts, $part_no = '') {
    switch ($this_part->type) {
    case TYPEMULTIPART:
      $mime_type = "multipart";
    
    $numparts = count($this_part->parts);
    if ($part_no != '') $nextpart_no = $part_no . '.';
    for ($i=0; $i<$numparts; $i++) {
      Mail::getImageList($this_part->parts[$i], $parts, $nextpart_no . ($i+1));
    }
    break;
    case TYPETEXT:
      $mime_type = "text";
    break;
    case TYPEMESSAGE:
      $mime_type = "message";
    break;
    case TYPEAPPLICATION:
      $mime_type = "application";
    break;
    case TYPEAUDIO:
      $mime_type = "audio";
    break;
    case TYPEIMAGE:
      $mime_type = "image";
    break;
    case TYPEVIDEO:
      $mime_type = "video";
    break;
    case TYPEMODEL:
      $mime_type = "model";
    break;
    default:
      $mime_type = "unknown";
  }
    
    $full_mime_type = $mime_type."/".strtolower($this_part->subtype);
    
    // echo "$part_no $full_mime_type <br>";
    
    // Decide what you what to do with this part
    // If you want to show it, figure out the encoding and echo away
    if (in_array($full_mime_type,
		 array('image/jpeg', 'image/pjpeg', 'image/jpg'))) {
      
      $this_part->partnumber = $part_no;
      $parts[] = $this_part;
  }
}
  
  function grabImage($mbox, $msgno, $partnumber, $encoding)
    {
      
      $image = imap_fetchbody($mbox, $msgno, $partnumber);
      
      switch ($encoding) {
      case ENCBASE64:
	$image = imap_base64($image);
    break;
      case ENCQUOTEDPRINTABLE:
	$image = imap_qprint($image);
      break;
      default:
	return false;
      }
           
      return $image;
    }
  
  
}

?>