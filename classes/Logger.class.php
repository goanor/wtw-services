<?php

class Logger {

  private $logfile = '';
  private $id_prefix = '';
  private $id = '';
  private $fp = null;

  function __construct($logfile, $id_prefix = '') {
    $this->logfile = $logfile;
    $this->id_prefix = $id_prefix;

    // Generate id
    $this->id = uniqid($this->id_prefix);

    $this->openLog();
  }

  function __destruct() {
    $this->closeLog();
  }

  private function openLog() {
    $this->fp = fopen($this->logfile, 'a');
  }

  private function closeLog() {
    fclose($this->fp);
  }

  public function write($msg) {
    if ($this->fp) {
      $compmsg = '[' . date("r") ."] [{$this->id}] {$msg}\n";
      fwrite($this->fp, $compmsg);
      return true;
    }
    return false;
  }

}
?>