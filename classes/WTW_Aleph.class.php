<?php

/*

Class WTW_Aleph

This class handles a major part of the functionality
in Aleph Mobile Services, specifically everything related
to database queries

Copyright 2009 - WapTheWeb AS
Christian Aune Thomassen
christian.thomassen@wtw.no
+47 932 13 659
*/

class WTW_Aleph
{
  
  private $dbConn = null;
  private $sms = null;

  function __construct($dbConn) {
    $this->dbConn = $dbConn;
  }

  /**
   * Set the SMSRespons member variable
   */
  function setSMSRespons($sr) {
    $this->sms = $sr;
  }


  public static function splitSessionId($session_id) {
    $session_split = array();
    if (strpos($session_id, '-') !== false) {
      $session_split = explode('-', $session_id);
    }
    else {
      $session_split[0] = $session_id;
      $session_split[1] = '';
    }

    return $session_split;
  }
    
  /**
   * NOT IN USE
   */
  public function getBorEmailByBorId($bor_id) {

    /*
When the system determines which address record to use, if there is more than
one record with relevant valid from-to dates, the system chooses the record
type 02 with the highest sequence number. Remember, when sending notices
to the patron, the system first searches for an address of type ü02ý where the
date of the notice falls within the valid from-to period. If the system does not
find a suitable 02 address, the system then searches for a suitable 01 address.
If no address is found, no address is printed.
  */
    
    $q = 
       //"SELECT Z304_REC_KEY FROM (" .
       "SELECT Z304_REC_KEY FROM USR01.Z304 " .
       "WHERE Z304_DATE_FROM <= TO_CHAR(sysdate, 'YYYYMMDD') " .
       "AND Z304_DATE_TO > TO_CHAR(sysdate, 'YYYYMMDD') " .
       "AND SUBSTR(Z304_REC_KEY, 1, 12) = $field_name " .
       "ORDER BY " .
       "DECODE(Z304_ADDRESS_TYPE,  2, 1, 1, 2, 3) ASC, Z304_REC_KEY DESC";
       
       //") " .
       //"WHERE rownum = 1";
    
    return $q;
  }

  /**
   * Get borrower info based on borrower id (e.g. STV00012345)
   * Returns an associative array with borrower information
   * or false if not found
   *
   * @param string $bor_id Borrower ID
   * @return array
   */
  public function getBorInfo($bor_id) {

    if (!$this->dbConn) return false;

    $q = "SELECT * FROM " . ALEPH_USR_DB . ".Z304 " .
       "WHERE SUBSTR(Z304_REC_KEY, 1, 12) = :BOR_ID " .
       "ORDER BY Z304_REC_KEY DESC";
    echo $q;
    echo $bor_id;

    $stmt = oci_parse($this->dbConn, $q);
    oci_bind_by_name($stmt, ':BOR_ID', $bor_id, -1);

    oci_execute($stmt);

    $row = oci_fetch_assoc($stmt);
    if (!empty($row) && is_array($row) && count($row) > 0) {
      return $row;
    }
    else {
      $row = false;
    }

    oci_free_statement($stmt);
    return $row;
  }

  /**
   * Get borrower id with session id.
   * We use timeout from www_server.conf (variable: www_a_s_time_out).
   * Currently 7200 secs.
   *
   * @param string $session_id Session ID
   * @return string
   */
  public function getBorIdBySessionId($session_id) {
    
    if (!$this->dbConn) return false;
    
    $session = WTW_Aleph::splitSessionId($session_id);
    $bor_id = false;

    $session_time_out = time() - 7200;

    $q = "SELECT Z63_BOR_ID FROM VIR01.Z63 " .
       "WHERE Z63_REC_KEY = :SESSION_ID " .
       "AND Z63_TIME >= $session_time_out";
    
    $stmt = oci_parse($this->dbConn, $q);
    oci_bind_by_name($stmt, ':SESSION_ID', $session[0], -1);
    
    oci_execute($stmt);
    
    while ($row = oci_fetch_assoc($stmt)) {
      if (!empty($row['Z63_BOR_ID'])) {
	$bor_id = $row['Z63_BOR_ID'];
      }
    }
    
    oci_free_statement($stmt);
    
    return $bor_id;
  }

  /**
   * Get the notification preferences for borrower.
   * Returns an associative array of preferences
   *
   * @param string $bor_id Borrower ID
   * @return array
   */
  public function getBorNotifications($bor_id) {
    
    if (!$this->dbConn) return false;

    $notifications = false;

    $q = "SELECT SMS_PRE_DUE, SMS_PRE_DUE_DAYS_BEFORE, SMS_HOLD_SHELF, " .
       "EMAIL_PRE_DUE, EMAIL_PRE_DUE_DAYS_BEFORE " .
       "FROM WTW.WTW_BOR_NOTIFICATIONS " .
       "WHERE BOR_ID = :BOR_ID";

    $stmt = oci_parse($this->dbConn, $q);
    oci_bind_by_name($stmt, ':BOR_ID', $bor_id, -1);

    oci_execute($stmt);

    while ($row = oci_fetch_assoc($stmt)) {
      if (is_array($row)) {
	$notifications = $row;
      }
    }

    oci_free_statement($stmt);

    return $notifications;
  }

  /**
   * Store borrower's notification preferences
   * Returns the number of rows affected
   *
   * @param string $bor_id Borrower ID
   * @param array $settings_array Preferences
   *
   * @return int
   */
  public function setBorNotifications($bor_id, $settings_array) {

    if (!$this->dbConn) return false;

    $sms_pre_due = 'N';
    if ($settings_array['sms_pre_due'] == 'Y') {
      $sms_pre_due = 'Y';
    }

    $email_pre_due = 'N';
    if ($settings_array['email_pre_due'] == 'Y') {
      $email_pre_due = 'Y';
    }

    $sms_pre_due_days_before = 3;
    if (in_array($settings_array['sms_pre_due_days_before'],
		 array(1,2,3))) {
      $sms_pre_due_days_before = $settings_array['sms_pre_due_days_before'];
    }

    $email_pre_due_days_before = 3;
    if (in_array($settings_array['email_pre_due_days_before'],
                 array(1,2,3))) {
      $email_pre_due_days_before = $settings_array['email_pre_due_days_before'];
    }

    $sms_hold_shelf = 'N';
    if ($settings_array['sms_hold_shelf'] == 'Y') {
      $sms_hold_shelf = 'Y';
    }

    if (!$this->getBorNotifications($bor_id)) {
      // INSERT
      $q = "INSERT INTO WTW.WTW_BOR_NOTIFICATIONS " .
	 "(BOR_ID, SMS_PRE_DUE, SMS_PRE_DUE_DAYS_BEFORE, " .
	 "EMAIL_PRE_DUE, EMAIL_PRE_DUE_DAYS_BEFORE, SMS_HOLD_SHELF) " .
	 "VALUES " .
	 "(:BOR_ID, :SMS_PRE_DUE, :SMS_PRE_DUE_DAYS_BEFORE, " .
	 ":EMAIL_PRE_DUE, :EMAIL_PRE_DUE_DAYS_BEFORE, :SMS_HOLD_SHELF)";
    }
    else {
      // UPDATE
      $q = "UPDATE WTW.WTW_BOR_NOTIFICATIONS " .
	 "SET " .
	 "SMS_PRE_DUE = :SMS_PRE_DUE, " .
	 "SMS_PRE_DUE_DAYS_BEFORE = :SMS_PRE_DUE_DAYS_BEFORE, " .
	 "EMAIL_PRE_DUE = :EMAIL_PRE_DUE, " .
         "EMAIL_PRE_DUE_DAYS_BEFORE = :EMAIL_PRE_DUE_DAYS_BEFORE, " .
	 "SMS_HOLD_SHELF = :SMS_HOLD_SHELF " .
	 "WHERE BOR_ID = :BOR_ID";
    }

    $stmt = oci_parse($this->dbConn, $q);
    oci_bind_by_name($stmt, ':BOR_ID',
                     $bor_id, -1);
    oci_bind_by_name($stmt, ':SMS_PRE_DUE',
		     $sms_pre_due, -1);
    oci_bind_by_name($stmt, ':SMS_PRE_DUE_DAYS_BEFORE',
		     $sms_pre_due_days_before, -1);
    oci_bind_by_name($stmt, ':EMAIL_PRE_DUE',
                     $email_pre_due, -1);
    oci_bind_by_name($stmt, ':EMAIL_PRE_DUE_DAYS_BEFORE',
                     $email_pre_due_days_before, -1);
    oci_bind_by_name($stmt, ':SMS_HOLD_SHELF',
		     $sms_hold_shelf, -1);

    oci_execute($stmt);
    $num_rows = oci_num_rows($stmt);
    oci_free_statement($stmt);

    return $num_rows;

  }

  /**
   * Get all borrowers that should receive a predue notification
   * Returns an array of borrowers. Each borrowers
   * is an associative array with Z36_ID, number of loans
   * and the number of days before due
   *
   * @param string $type SMS or EMAIL
   * @return array
   */
  public function getPreDueBors($type = 'SMS', $for_bor_status = array()) {
    
    if (!$this->dbConn) return false;    

    // Get all loaners which has loans with due date +SMS_PRE_DUE_DAYS_BEFORE
    // and have SMS_PRE_DUE = 'Y'
    // and have a valid norwegian cell phone number
    // and not have recieved notification for current item before.
    if ($type == 'SMS') {
      $q = "SELECT Z36_ID, COUNT(*) as ANTALL, " .
	 "SMS_PRE_DUE_DAYS_BEFORE " .
	 "FROM NOR50.Z36, " .
	 "WTW.WTW_BOR_NOTIFICATIONS WBN " .
	 "WHERE Z36_ID = WBN.BOR_ID " .
	 // "AND (Z36_DUE_DATE - TO_CHAR(sysdate, 'YYYYMMDD')) = " .
	 "AND TO_DATE(Z36_DUE_DATE, 'YYYYMMDD') - TO_DATE(TO_CHAR(sysdate, 'YYYYMMDD'), 'YYYYMMDD') = " .
	 "WBN.SMS_PRE_DUE_DAYS_BEFORE " .
	 "AND WBN.SMS_PRE_DUE = 'Y' " .
	 "AND Z36_RETURNED_DATE = 0 " .
	 "AND NOT EXISTS ( " .
         "  SELECT W.LOAN_NUMBER FROM WTW_SMS_NOTIFY W" .
         "  WHERE W.LOAN_NUMBER = Z36.Z36_NUMBER " .
         "  AND Z36.Z36_DUE_DATE = TO_CHAR(W.DUE_DATE, 'YYYYMMDD') " .
         "  AND NOTIFICATION = 'P') " .
	 "GROUP BY Z36_ID, SMS_PRE_DUE_DAYS_BEFORE";
    }
    // Get all loaners which has loans with due date +EMAIL_PRE_DUE_DAYS_BEFORE
    // and WTW_BOR_NOTIFICATIONS.SMS_PRE_DUE = 'Y'
    // or does not have a record in WTW_BOR_NOTIFICATIONS
    // in that case use 3 days as default
    // If user has record and SMS_PRE_DUE = 'N' skip email
    else {
      $q = "SELECT Z36_ID, EMAIL_PRE_DUE_DAYS_BEFORE FROM " .
	 "(SELECT Z36_ID, EMAIL_PRE_DUE_DAYS_BEFORE, " .
	 "Z36_DUE_DATE, Z36_NUMBER " .
	 "FROM NOR50.Z36 " .
	 "LEFT OUTER JOIN " .
	 "WTW.WTW_BOR_NOTIFICATIONS WBN " .
	 "ON NOR50.Z36.Z36_ID = WBN.BOR_ID " .
	 "WHERE " .
	 "TO_DATE(Z36_DUE_DATE, 'YYYYMMDD') - TO_DATE(TO_CHAR(sysdate, 'YYYYMMDD'), 'YYYYMMDD') = " .
	 "WBN.EMAIL_PRE_DUE_DAYS_BEFORE " .
	 "AND WBN.EMAIL_PRE_DUE = 'Y' " .
	 "AND Z36_RETURNED_DATE = 0 ";

      if (!EMAIL_PRE_DUE_WITH_PREFERENCES_ONLY) {
	$q .= "UNION ALL " .
	   "SELECT Z36_ID, " . DAYS_BEFORE_DUE .
	   " as EMAIL_PRE_DUE_DAYS_BEFORE, Z36_DUE_DATE, Z36_NUMBER " .
	   "FROM NOR50.Z36 " .
	   "LEFT OUTER JOIN " .
	   "WTW.WTW_BOR_NOTIFICATIONS WBN " .
	   "ON NOR50.Z36.Z36_ID = WBN.BOR_ID " .
	   "JOIN NOR50.Z305 ON Z305_REC_KEY = CONCAT(Z36_ID, 'NOR50') " .
	   "WHERE " .
	   // "(Z36_DUE_DATE - TO_CHAR(sysdate, 'YYYYMMDD')) = " . 
	   "TO_DATE(Z36_DUE_DATE, 'YYYYMMDD') - TO_DATE(TO_CHAR(sysdate, 'YYYYMMDD'), 'YYYYMMDD') = " .
	   DAYS_BEFORE_DUE .
	   " AND WBN.EMAIL_PRE_DUE IS NULL " .
	   "AND Z36_RETURNED_DATE = 0";

	if (!empty($for_bor_status) && count($for_bor_status) > 0) {
	  $q .= "AND Z305_BOR_STATUS IN(" .
	     implode(',', $for_bor_status) . ") ";
	}

      }
      $q .= ") " .
	 "WHERE NOT EXISTS ( " .
         "  SELECT W.LOAN_NUMBER FROM WTW_EMAIL_NOTIFY W" .
         "  WHERE W.LOAN_NUMBER = Z36_NUMBER " .
         "  AND Z36_DUE_DATE = TO_CHAR(W.DUE_DATE, 'YYYYMMDD') " .
         "  AND NOTIFICATION = 'P') " .
	 "GROUP BY Z36_ID, EMAIL_PRE_DUE_DAYS_BEFORE";
    }

    echo $q;

    $stmt = oci_parse($this->dbConn, $q);
    oci_execute($stmt);

    $bors = false;
    while ($row = oci_fetch_assoc($stmt)) {
      if (is_array($row)) {
        $bors[] = $row;
      }
    }

    oci_free_statement($stmt);

    return $bors;
  }
  
  /**
   * Validate email address
   *
   * @param string email address
   * @return boolean
   *
   */
  public static function isEmail($email) {
    
    if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $email)) {
      return false;
    }
    
    return true;
  }

  /**
   * Validate mobile number. Only 8-digit numbers starting with 4 or 9 are valid
   *
   * @param string mobile number
   * @return boolean
   */
  public static function isMobile($nr) {

    if (empty($nr) ||
	strlen($nr) !== 8 ||
	(substr($nr, 0, 1) != 4 &&
	 substr($nr, 0, 1) != 9)) {
      return false;
    }
    
    return true;
  }

  /**
   * Get all borrowers that should receive an overdue notification by email
   * Only returns borrowers with a certain status (see $for_bor_status)
   * and who have loans with a certain letter number (see $letter_number)
   * and that are a certain number of days overdue (see $days_overdue)
   * Returns an array of borrowers, where each borrowers is an associative
   * array with Z36_ID, number of loans and borrower status
   * 
   * @param int $letter_number Which letter number we are going to send
   * @param int $days_overdue The number of days a loan must be overdue to
   * receive a notification
   * @param array $for_bor_status list of borrower statuses
   *
   */
  public function getOverdueBorsEmail($letter_number = 1,
				      $days_overdue = 1,
				      $for_bor_status = array()) {
    if (!$this->dbConn) return false;
    
    if ($letter_number == 0) return false;
    
    if ($days_overdue <= 0) return false;
    
    $q = "SELECT Z36_ID, " .
       "COUNT(*) as ANTALL, Z305_BOR_STATUS " .    
       "FROM NOR50.Z36, NOR50.Z305 " .
       "WHERE " .
       "TO_DATE(Z36_DUE_DATE, 'YYYYMMDD') - TO_DATE(TO_CHAR(sysdate, 'YYYYMMDD'), 'YYYYMMDD') <= -{$days_overdue} " .
       "AND Z36_RETURNED_DATE = 0 " .
       "AND Z36_LETTER_NUMBER = $letter_number - 1 " .
       "AND Z36_STATUS = 'A' " .
       "AND Z305_REC_KEY = CONCAT(Z36_ID, 'NOR50') ";
 
    if (!empty($for_bor_status) && count($for_bor_status) > 0) {
      $q .= "AND Z305_BOR_STATUS IN(" . 
	 implode(',', $for_bor_status) . ") ";
    }

    $q .= "AND NOT EXISTS ( " .
       "  SELECT W.LOAN_NUMBER FROM WTW_EMAIL_NOTIFY W" .
       "  WHERE W.LOAN_NUMBER = Z36_NUMBER " .
       "  AND Z36_DUE_DATE = TO_CHAR(W.DUE_DATE, 'YYYYMMDD') " .
       "  AND LETTER_NUMBER = $letter_number " .
       "  AND NOTIFICATION = 'O') " .
       "GROUP BY Z36_ID, Z305_BOR_STATUS";
    
    echo $q;
    
    $stmt = oci_parse($this->dbConn, $q);
    oci_execute($stmt);
    
    $bors = false;
    while ($row = oci_fetch_assoc($stmt)) {
      if (is_array($row)) {
        $bors[] = $row;
      }
    }
    
    oci_free_statement($stmt);

    return $bors;
    

  }

  /**
   * Get all borrowers that should receive an overdue notification by SMS
   * Only returns borrowers with a certain status (see $for_bor_status)
   * and who have loans with a certain letter number (see $letter_number)
   * and that are a certain number of days overdue (see $days_overdue)
   * Returns an array of borrowers, where each borrowers is an associative
   * array with Z36_ID, number of loans and borrower status
   * 
   * @param int $letter_number Which letter number we are going to send
   * @param int $days_overdue The number of days a loan must be overdue to
   * receive a notification
   * @param array $for_bor_status list of borrower statuses
   *
   */
  public function getOverdueBorsSms($letter_number = 1,
				      $days_overdue = 1,
				      $for_bor_status = array()) {
    if (!$this->dbConn) return false;
    
    if ($letter_number == 0) return false;
    
    if ($days_overdue <= 0) return false;
    
    $q = "SELECT Z36_ID, " .
       "COUNT(*) as ANTALL, Z305_BOR_STATUS " .    
       "FROM NOR50.Z36, NOR50.Z305 " .
       "WHERE " .
       "TO_DATE(Z36_DUE_DATE, 'YYYYMMDD') - TO_DATE(TO_CHAR(sysdate, 'YYYYMMDD'), 'YYYYMMDD') <= -{$days_overdue} " .
       "AND Z36_RETURNED_DATE = 0 " .
       "AND Z36_LETTER_NUMBER = $letter_number - 1 " .
       "AND Z36_STATUS = 'A' " .
       "AND Z305_REC_KEY = CONCAT(Z36_ID, 'NOR50') ";
 
    if (!empty($for_bor_status) && count($for_bor_status) > 0) {
      $q .= "AND Z305_BOR_STATUS IN(" . 
	 implode(',', $for_bor_status) . ") ";
    }

    $q .= "AND NOT EXISTS ( " .
       "  SELECT W.LOAN_NUMBER FROM WTW_SMS_NOTIFY W" .
       "  WHERE W.LOAN_NUMBER = Z36_NUMBER " .
       "  AND Z36_DUE_DATE = TO_CHAR(W.DUE_DATE, 'YYYYMMDD') " .
       "  AND LETTER_NUMBER = $letter_number " .
       "  AND NOTIFICATION = 'O') " .
       "GROUP BY Z36_ID, Z305_BOR_STATUS";
    
    echo $q;
    
    $stmt = oci_parse($this->dbConn, $q);
    oci_execute($stmt);
    
    $bors = false;
    while ($row = oci_fetch_assoc($stmt)) {
      if (is_array($row)) {
        $bors[] = $row;
      }
    }
    
    oci_free_statement($stmt);

    return $bors;
    

  }

  /**
   * Prepares the query which is used to fetch all
   * the borrower's remote loans which should be included
   * in the predue notification
   *
   * @param string $type SMS or EMAIL
   * @return statement handle or FALSE
   */
  public function preparePreDueLoansQueryIll($type = 'SMS',
					     $for_bor_status = array()) {
    if (!$this->dbConn) return false;

    if ($type == 'SMS') {
      $q = "SELECT DISTINCT Z36_ID, Z36_REC_KEY, " .
         "Z13_TITLE, Z13_AUTHOR, Z36_NUMBER, " .
         "Z36_DUE_DATE " .
         "FROM NOR50.Z36, " .
         ALEPH_ILL_DB . ".Z13, NOR50.Z103, WTW.WTW_BOR_NOTIFICATIONS WBN " .
         "WHERE Z36_ID = WBN.BOR_ID " .
         // "AND (Z36_DUE_DATE - TO_CHAR(sysdate, 'YYYYMMDD')) = " .
         "AND TO_DATE(Z36_DUE_DATE, 'YYYYMMDD') - TO_DATE(TO_CHAR(sysdate, 'YYYYMMDD'), 'YYYYMMDD') = " .
         "WBN.SMS_PRE_DUE_DAYS_BEFORE " .
         "AND WBN.SMS_PRE_DUE = 'Y' " .
         "AND Z36_RETURNED_DATE = 0 " .
         "AND Z36_ID = :BOR_ID " .
	 "AND Z36_ITEM_STATUS = '" . TAB15_ILL_ITEM_STATUS . "' "  .
         "AND Z13_REC_KEY = Z103_LKR_DOC_NUMBER " .
         "AND Z103_LKR_LIBRARY = '" . ALEPH_ILL_DB . "' " .
         "AND SUBSTR(Z36_REC_KEY,1,9) = SUBSTR(Z103_REC_KEY,6,9) " .
         "AND NOT EXISTS ( " .
         "  SELECT W.LOAN_NUMBER FROM WTW_SMS_NOTIFY W" .
         "  WHERE W.LOAN_NUMBER = Z36.Z36_NUMBER " .
         "  AND Z36.Z36_DUE_DATE = TO_CHAR(W.DUE_DATE, 'YYYYMMDD') " .
         "  AND NOTIFICATION = 'P')";
    }
    else {
      $q = "SELECT DISTINCT Z36_ID, Z36_REC_KEY, " .
         "Z13_TITLE, Z13_AUTHOR, Z36_NUMBER, " .
         "Z36_DUE_DATE FROM " .
         "(SELECT Z36_ID, Z36_REC_KEY, Z36_NUMBER, Z36_DUE_DATE " .
         "FROM NOR50.Z36 " .
         "LEFT OUTER JOIN " .
         "WTW.WTW_BOR_NOTIFICATIONS WBN " .
         "ON NOR50.Z36.Z36_ID = WBN.BOR_ID " .
         "WHERE " .
         // "(Z36_DUE_DATE - TO_CHAR(sysdate, 'YYYYMMDD')) = " .
         "TO_DATE(Z36_DUE_DATE, 'YYYYMMDD') - TO_DATE(TO_CHAR(sysdate, 'YYYYMMDD'), 'YYYYMMDD') = " .
         "WBN.EMAIL_PRE_DUE_DAYS_BEFORE " .
         "AND WBN.EMAIL_PRE_DUE = 'Y' " .
         "AND Z36_RETURNED_DATE = 0 " .
	 "AND Z36_ITEM_STATUS = '" . TAB15_ILL_ITEM_STATUS . "' "  .
         "AND Z36_ID = :BOR_ID ";

      if (!EMAIL_PRE_DUE_WITH_PREFERENCES_ONLY) {
        $q .= "UNION ALL " .
           "SELECT Z36_ID, Z36_REC_KEY, Z36_NUMBER, Z36_DUE_DATE " .
           "FROM NOR50.Z36 " .
           "LEFT OUTER JOIN " .
           "WTW.WTW_BOR_NOTIFICATIONS WBN " .
           "ON NOR50.Z36.Z36_ID = WBN.BOR_ID " .
	   "JOIN NOR50.Z305 ON Z305_REC_KEY = CONCAT(Z36_ID, 'NOR50') " .
           "WHERE " .
           // "(Z36_DUE_DATE - TO_CHAR(sysdate, 'YYYYMMDD')) = " .
           "TO_DATE(Z36_DUE_DATE, 'YYYYMMDD') - TO_DATE(TO_CHAR(sysdate, 'YYYYMMDD'), 'YYYYMMDD') = " .
           DAYS_BEFORE_DUE .
           " AND WBN.EMAIL_PRE_DUE IS NULL " .
           "AND Z36_RETURNED_DATE = 0 " .
	   "AND Z36_ITEM_STATUS = '" . TAB15_ILL_ITEM_STATUS . "' "  .
           "AND Z36_ID = :BOR_ID ";

	if (!empty($for_bor_status) && count($for_bor_status) > 0) {
          $q .= "AND Z305_BOR_STATUS IN(" .
             implode(',', $for_bor_status) . ") ";
        }

      }

      $q .= "), " .
         ALEPH_ILL_DB . ".Z13, NOR50.Z103 " .
         "WHERE Z13_REC_KEY = Z103_LKR_DOC_NUMBER " .
         "AND Z103_LKR_LIBRARY = '" . ALEPH_ILL_DB . "' " .
         "AND SUBSTR(Z36_REC_KEY,1,9) = SUBSTR(Z103_REC_KEY,6,9) " .
         "AND NOT EXISTS ( " .
         "  SELECT W.LOAN_NUMBER FROM WTW_EMAIL_NOTIFY W" .
         "  WHERE W.LOAN_NUMBER = Z36_NUMBER " .
         "  AND Z36_DUE_DATE = TO_CHAR(W.DUE_DATE, 'YYYYMMDD') " .
         "  AND NOTIFICATION = 'P')";
    }

    echo $q . "\n\n";

    return oci_parse($this->dbConn, $q);
  }

  /**
   * Prepares the query which is used to fetch all
   * the borrower's local loans which should be included
   * in the predue notification
   *
   * @param string $type SMS or EMAIL
   * @return statement handle or FALSE
   */
  public function preparePreDueLoansQuery($type = 'SMS',
					  $for_bor_status = array()) {
    if (!$this->dbConn) return false;

    if ($type == 'SMS') {
      $q = "SELECT DISTINCT Z36_ID, Z36_REC_KEY, " .
	 "Z13_TITLE, Z13_AUTHOR, Z36_NUMBER, " .
	 "Z36_DUE_DATE " .
	 "FROM NOR50.Z36, " .
	 "NOR01.Z13, NOR50.Z103, WTW.WTW_BOR_NOTIFICATIONS WBN " .
	 "WHERE Z36_ID = WBN.BOR_ID " .
	 // "AND (Z36_DUE_DATE - TO_CHAR(sysdate, 'YYYYMMDD')) = " .
	 "AND TO_DATE(Z36_DUE_DATE, 'YYYYMMDD') - TO_DATE(TO_CHAR(sysdate, 'YYYYMMDD'), 'YYYYMMDD') = " .
	 "WBN.SMS_PRE_DUE_DAYS_BEFORE " .
	 "AND WBN.SMS_PRE_DUE = 'Y' " .
	 "AND Z36_RETURNED_DATE = 0 " .
	 "AND Z36_ITEM_STATUS <> '" . TAB15_ILL_ITEM_STATUS . "' "  .
	 "AND Z36_ID = :BOR_ID " .
	 "AND Z13_REC_KEY = Z103_LKR_DOC_NUMBER " .
	 "AND Z103_LKR_LIBRARY = '" . Z103_LKR_LIBRARY . "' " .
	 "AND SUBSTR(Z36_REC_KEY,1,9) = SUBSTR(Z103_REC_KEY,6,9) " .
	 "AND NOT EXISTS ( " .
	 "  SELECT W.LOAN_NUMBER FROM WTW_SMS_NOTIFY W" .
	 "  WHERE W.LOAN_NUMBER = Z36.Z36_NUMBER " .
	 "  AND Z36.Z36_DUE_DATE = TO_CHAR(W.DUE_DATE, 'YYYYMMDD') " .
	 "  AND NOTIFICATION = 'P')";
    }
    else {
      $q = "SELECT DISTINCT Z36_ID, Z36_REC_KEY, " .
         "Z13_TITLE, Z13_AUTHOR, Z36_NUMBER, " .
         "Z36_DUE_DATE FROM " .
         "(SELECT Z36_ID, Z36_REC_KEY, Z36_NUMBER, Z36_DUE_DATE " .
         "FROM NOR50.Z36 " .
         "LEFT OUTER JOIN " .
         "WTW.WTW_BOR_NOTIFICATIONS WBN " .
         "ON NOR50.Z36.Z36_ID = WBN.BOR_ID " .
         "WHERE " .
         // "(Z36_DUE_DATE - TO_CHAR(sysdate, 'YYYYMMDD')) = " .
	 "TO_DATE(Z36_DUE_DATE, 'YYYYMMDD') - TO_DATE(TO_CHAR(sysdate, 'YYYYMMDD'), 'YYYYMMDD') = " .
         "WBN.EMAIL_PRE_DUE_DAYS_BEFORE " .
         "AND WBN.EMAIL_PRE_DUE = 'Y' " .
         "AND Z36_RETURNED_DATE = 0 " .
	 "AND Z36_ITEM_STATUS <> '" . TAB15_ILL_ITEM_STATUS . "' "  .
	 "AND Z36_ID = :BOR_ID ";

      if (!EMAIL_PRE_DUE_WITH_PREFERENCES_ONLY) {
	$q .= "UNION ALL " .
	   "SELECT Z36_ID, Z36_REC_KEY, Z36_NUMBER, Z36_DUE_DATE " .
	   "FROM NOR50.Z36 " .
	   "LEFT OUTER JOIN " .
	   "WTW.WTW_BOR_NOTIFICATIONS WBN " .
	   "ON NOR50.Z36.Z36_ID = WBN.BOR_ID " .
	   "JOIN NOR50.Z305 ON Z305_REC_KEY = CONCAT(Z36_ID, 'NOR50') " .
	   "WHERE " .
	   // "(Z36_DUE_DATE - TO_CHAR(sysdate, 'YYYYMMDD')) = " .
	   "TO_DATE(Z36_DUE_DATE, 'YYYYMMDD') - TO_DATE(TO_CHAR(sysdate, 'YYYYMMDD'), 'YYYYMMDD') = " .
	   DAYS_BEFORE_DUE .
	   " AND WBN.EMAIL_PRE_DUE IS NULL " .
	   "AND Z36_RETURNED_DATE = 0 " .
	   "AND Z36_ITEM_STATUS <> '" . TAB15_ILL_ITEM_STATUS . "' "  .
	   "AND Z36_ID = :BOR_ID ";

	if (!empty($for_bor_status) && count($for_bor_status) > 0) {
          $q .= "AND Z305_BOR_STATUS IN(" .
             implode(',', $for_bor_status) . ") ";
        }

      }

      $q .= "), " .
	 "NOR01.Z13, NOR50.Z103 " .
	 "WHERE Z13_REC_KEY = Z103_LKR_DOC_NUMBER " .
	 "AND Z103_LKR_LIBRARY = '" . Z103_LKR_LIBRARY . "' " .
	 "AND SUBSTR(Z36_REC_KEY,1,9) = SUBSTR(Z103_REC_KEY,6,9) " .
	 "AND NOT EXISTS ( " .
	 "  SELECT W.LOAN_NUMBER FROM WTW_EMAIL_NOTIFY W" .
	 "  WHERE W.LOAN_NUMBER = Z36_NUMBER " .
	 "  AND Z36_DUE_DATE = TO_CHAR(W.DUE_DATE, 'YYYYMMDD') " .
	 "  AND NOTIFICATION = 'P')";
	 
      /*
      $q = "SELECT Z36_ID, Z36_REC_KEY, Z304_TELEPHONE_3, " .
         "Z304_EMAIL_ADDRESS, Z13_TITLE, Z13_AUTHOR, Z36_NUMBER, " .
         "Z36_DUE_DATE " .
         "FROM NOR50.Z36, USR01.Z304, " .
         "NOR01.Z13, NOR50.Z103, WTW.WTW_BOR_NOTIFICATIONS WBN " .
         "WHERE Z36_ID = WBN.BOR_ID " .
         "AND (Z36_DUE_DATE - TO_CHAR(sysdate, 'YYYYMMDD')) = " .
         "WBN.SMS_PRE_DUE_DAYS_BEFORE " .
         "AND WBN.SMS_PRE_DUE = 'Y' " .
         "AND Z36_RETURNED_DATE = 0 " .
         "AND Z36_ID = :BOR_ID " .
         "AND Z36_ID = SUBSTR(Z304_REC_KEY, 1, 12) " .
         "AND Z13_REC_KEY = Z103_LKR_DOC_NUMBER " .
         "AND Z103_LKR_LIBRARY = '" . Z103_LKR_LIBRARY . "' " .
         "AND SUBSTR(Z36_REC_KEY,1,9) = SUBSTR(Z103_REC_KEY,6,9) " .
         "AND (Z304_TELEPHONE_3 LIKE '9%' OR  Z304_TELEPHONE_3 LIKE '4%') " .
         "AND LENGTH(Z304_TELEPHONE_3) = 8 " .
         "AND NOT EXISTS ( " .
         "  SELECT W.LOAN_NUMBER FROM WTW_NOTIFY_PRE_DUE W" .
         "  WHERE W.LOAN_NUMBER = Z36.Z36_NUMBER " .
	 "  AND Z36.Z36_DUE_DATE = TO_CHAR(W.DUE_DATE, 'YYYYMMDD')) ";
      
      */
      echo $q;
    }

    echo $q . "\n\n";

    return oci_parse($this->dbConn, $q);
  }

  /**
   * Prepares the query which is used to fetch all the
   * borrower's loans of a certain type that will be
   * included in the overdue notification sent by email
   *
   * @param int $letter_number which letter number will be sent
   * @param int $days_overdue minimum number of days each loan must be overdue
   * @param int $for_bor_status list of borrowers statuses
   * @param string $type Type of loan. LOCAL or ILL (remote)
   * @return statement handle or FALSE
   */
 
  public function prepareOverdueQueryEmail($letter_number = 1,
					   $days_overdue = 1,
					   $for_bor_status = array(),
					   $type = 'LOCAL') {
    
    if (!$this->dbConn) return false;

    if ($letter_number == 0) return false;

    if ($days_overdue <= 0) return false;
    
    $item_status_operator = '<>';
    $link_db = Z103_LKR_LIBRARY;
    $optimizer = '/*+ USE_NL(Z36 Z304 Z103 Z13) */';
    // If fjernlån
    if ($type == 'ILL') {
      $link_db = ALEPH_ILL_DB;
      $item_status_operator = '=';
      $optimizer = '';
    }
    

    $q = "SELECT $optimizer DISTINCT Z36_ID, Z36_REC_KEY, " .
       "Z305_BOR_STATUS, Z13_TITLE, Z13_AUTHOR, Z36_NUMBER, " .
       "Z36_DUE_DATE " .
       "FROM NOR50.Z36, NOR50.Z305, NOR50.Z103, {$link_db}.Z13 Z13 " .
       "WHERE " .
       "Z36_ID = :BOR_ID " .
       "AND SUBSTR(Z36_REC_KEY,1,9) = SUBSTR(Z103_REC_KEY,6,9) " .
       "AND Z13_REC_KEY = Z103_LKR_DOC_NUMBER " .
       "AND Z103_LKR_LIBRARY = '$link_db' " .
       "AND TO_DATE(Z36_DUE_DATE, 'YYYYMMDD') - TO_DATE(TO_CHAR(sysdate, 'YYYYMMDD'), 'YYYYMMDD') <= -{$days_overdue} " .
       "AND Z36_RETURNED_DATE = 0 " .
       "AND Z36_LETTER_NUMBER = $letter_number - 1 " .
       "AND Z36_STATUS = 'A' " .
       "AND Z36_ITEM_STATUS $item_status_operator '" .
       TAB15_ILL_ITEM_STATUS . "' "  .
       "AND Z305_REC_KEY = CONCAT(Z36_ID, 'NOR50') ";

    if (!empty($for_bor_status) && count($for_bor_status) > 0) {
      $q .= "AND Z305_BOR_STATUS IN(" .
         implode(',', $for_bor_status) . ") ";
    }

    $q .= "AND NOT EXISTS ( " .
       "  SELECT W.LOAN_NUMBER FROM WTW_EMAIL_NOTIFY W" .
       "  WHERE W.LOAN_NUMBER = Z36_NUMBER " .
       "  AND Z36_DUE_DATE = TO_CHAR(W.DUE_DATE, 'YYYYMMDD') " .
       "  AND LETTER_NUMBER = $letter_number " .
       "  AND NOTIFICATION = 'O') ";

    /*
    $q = "SELECT $optimizer " .
       "DISTINCT Z36_ID, Z36_REC_KEY, Z304_TELEPHONE_3, " .
       "Z304_EMAIL_ADDRESS, Z13_TITLE, Z13_AUTHOR, Z36_NUMBER, " .
       "Z36_DUE_DATE ". 
       "FROM NOR50.Z36, USR01.Z304, NOR50.Z103, {$link_db}.Z13 Z13 " .
       "WHERE " .
       "Z36_ID = :BOR_ID " .
       "AND Z36_ID = SUBSTR(Z304_REC_KEY, 1, 12) " .
       "AND SUBSTR(Z36_REC_KEY,1,9) = SUBSTR(Z103_REC_KEY,6,9) " .
       "AND Z13_REC_KEY = Z103_LKR_DOC_NUMBER " .
       "AND Z103_LKR_LIBRARY = '$link_db' " .
       "AND CEIL(TO_DATE(Z36_DUE_DATE, 'YYYYMMDD') - sysdate) <= -{$days_overdue} " .
       "AND Z36_RETURNED_DATE = 0 " .
       "AND Z36_LETTER_NUMBER = $letter_number - 1 " .
       "AND Z36_STATUS = 'A' " .
       "AND Z36_ITEM_STATUS $item_status_operator '" .
       TAB15_ILL_ITEM_STATUS . "' "  .
       "AND LENGTH(Z304_EMAIL_ADDRESS) >= 6 " .
       "AND Z304_EMAIL_ADDRESS LIKE '%@%' " .
       "AND NOT EXISTS ( " .
       "  SELECT W.LOAN_NUMBER FROM WTW_EMAIL_NOTIFY W" .
       "  WHERE W.LOAN_NUMBER = Z36_NUMBER " .
       "  AND Z36_DUE_DATE = TO_CHAR(W.DUE_DATE, 'YYYYMMDD') " .
       "  AND LETTER_NUMBER = $letter_number " .
       "  AND NOTIFICATION = 'O') ";
    */
    echo $q . "\n\n";

    return oci_parse($this->dbConn, $q);
  }


  /**
   * Prepares the query which is used to fetch all the
   * borrower's loans of a certain type that will be
   * included in the overdue notification sent by SMS
   *
   * @param int $letter_number which letter number will be sent
   * @param int $days_overdue minimum number of days each loan must be overdue
   * @param int $for_bor_status list of borrowers statuses
   * @param string $type Type of loan. LOCAL or ILL (remote)
   * @return statement handle or FALSE
   */
  public function prepareOverdueQuerySms($letter_number = 1,
					   $days_overdue = 1,
					   $for_bor_status = array(),
					   $type = 'LOCAL') {
    
    if (!$this->dbConn) return false;

    if ($letter_number == 0) return false;

    if ($days_overdue <= 0) return false;
    
    $item_status_operator = '<>';
    $link_db = Z103_LKR_LIBRARY;
    $optimizer = '/*+ USE_NL(Z36 Z304 Z103 Z13) */';
    // If fjernlån
    if ($type == 'ILL') {
      $link_db = ALEPH_ILL_DB;
      $item_status_operator = '=';
      $optimizer = '';
    }
    

    $q = "SELECT $optimizer DISTINCT Z36_ID, Z36_REC_KEY, " .
       "Z305_BOR_STATUS, Z13_TITLE, Z13_AUTHOR, Z36_NUMBER, " .
       "Z36_DUE_DATE " .
       "FROM NOR50.Z36, NOR50.Z305, NOR50.Z103, {$link_db}.Z13 Z13 " .
       "WHERE " .
       "Z36_ID = :BOR_ID " .
       "AND SUBSTR(Z36_REC_KEY,1,9) = SUBSTR(Z103_REC_KEY,6,9) " .
       "AND Z13_REC_KEY = Z103_LKR_DOC_NUMBER " .
       "AND Z103_LKR_LIBRARY = '$link_db' " .
       "AND TO_DATE(Z36_DUE_DATE, 'YYYYMMDD') - TO_DATE(TO_CHAR(sysdate, 'YYYYMMDD'), 'YYYYMMDD') <= -{$days_overdue} " .
       "AND Z36_RETURNED_DATE = 0 " .
       "AND Z36_LETTER_NUMBER = $letter_number - 1 " .
       "AND Z36_STATUS = 'A' " .
       "AND Z36_ITEM_STATUS $item_status_operator '" .
       TAB15_ILL_ITEM_STATUS . "' "  .
       "AND Z305_REC_KEY = CONCAT(Z36_ID, 'NOR50') ";

    if (!empty($for_bor_status) && count($for_bor_status) > 0) {
      $q .= "AND Z305_BOR_STATUS IN(" .
         implode(',', $for_bor_status) . ") ";
    }

    $q .= "AND NOT EXISTS ( " .
       "  SELECT W.LOAN_NUMBER FROM WTW_SMS_NOTIFY W" .
       "  WHERE W.LOAN_NUMBER = Z36_NUMBER " .
       "  AND Z36_DUE_DATE = TO_CHAR(W.DUE_DATE, 'YYYYMMDD') " .
       "  AND LETTER_NUMBER = $letter_number " .
       "  AND NOTIFICATION = 'O') ";


    echo $q . "\n\n";

    return oci_parse($this->dbConn, $q);
  }
    
  /**
   * Executes the given statement for retrieving
   * all loans that should be included in the predue
   * notification for the given borrower
   * Returns an array of loans
   * 
   * @param int $bor_id Borrower ID
   * @param object $stmt Prepared statement
   * @return array
   */
  public function getPreDueLoans($bor_id, $stmt) {
    
    if (!$this->dbConn) return false;
    
    oci_bind_by_name($stmt, ':BOR_ID', $bor_id, -1);
    $start_t = microtime(true);
    oci_execute($stmt);
    echo $end_t = microtime(true) - $start_t;
    $loans = array();
    while ($row = oci_fetch_assoc($stmt)) {
      if (is_array($row)) {
        $loans[] = $row;
      }
    }
    return $loans;
  }

  /**
   *    
   */
  public function getLoans($bor_id, $stmt) {
    return $this->getPreDueLoans($bor_id, $stmt);
  }

  /**
   * Free a prepared statement
   */
  public function freePreDueLoansQuery($stmt) {
    oci_free_statement($stmt);    
  }
  
  /**
   *
   */
  public function freeLoansQuery($stmt) {
    return $this->freePreDueLoansQuery($stmt);
  }

  /**
   * This function generates a predue notification email
   * containing the given loans for the given borrower
   * Returns the HTML body for the email
   *
   * @param array $bor Borrower information
   * @param array $loans Loans
   * @return string
   */
  public function generatePreDueEmail($bor, $loans) {

    $num_loans = count($loans);
    if ($num_loans == 0) return false;

    $html_body =
       '<html>' .
       ' <head></head>' .
       ' <body>';

    $html_body .= "<p>F&oslash;lgende $num_loans l&aring;n forfaller om {$bor['SMS_PRE_DUE_DAYS_BEFORE']} dag(er):</p>\n\n";

    $html_body .= "<ul>\n";
    foreach ($loans as $l) {
      $html_body .= "<li>" . htmlentities($l['Z13_TITLE']) . "</li>\n";
    }
    $html_body .= "</ul>\n";

    $html_body .= "</body></html>";
    return $html_body;
  }


  public function tagSMSOverdueNotificationSent($sms_log_id, $bor, $loans, $letter_number) {
    return $this->tagSMSNotificationSent($sms_log_id, $bor, $loans, 'O', $letter_number);
  } 

  public function tagSMSPreDueNotificationSent($sms_log_id, $bor, $loans) {
    return $this->tagSMSNotificationSent($sms_log_id, $bor, $loans, 'P');
  }

  /**
   * Tag all notified loans contained in the SMS
   *
   * @param int $sms_log_id
   * @param array $bor Borrower information (NOT USED!)
   * @param array $loans Loans contained in the SMS
   * @param string Notification type (P (predue) or O (overdue))
   * @param int $letter_number   
   *
   */
  public function tagSMSNotificationSent($sms_log_id, $bor, $loans, $notification = 'P', $letter_number = NULL) {

    if (!$this->dbConn) return false;
    
    $q = "INSERT INTO WTW.WTW_SMS_NOTIFY " .
       "(LOAN_NUMBER, DUE_DATE, NOTIFICATION, LETTER_NUMBER, " .
       "SMS_LOG_ID, CREATED_DATETIME) " .
       "VALUES " .
       "(:LOAN_NUMBER, TO_DATE(:DUE_DATE, 'YYYYMMDD'), " .
       ":NOTIFICATION, :LETTER_NUMBER, :SMS_LOG_ID, SYSDATE)";

    $stmt = oci_parse($this->dbConn, $q);    
      
    foreach ($loans as $l) {
      oci_bind_by_name($stmt, ':LOAN_NUMBER',
		       $l['Z36_NUMBER'], -1);      
      oci_bind_by_name($stmt, ':DUE_DATE',
                       $l['Z36_DUE_DATE'], -1);
      oci_bind_by_name($stmt, ':NOTIFICATION',
                       $notification, -1);
      oci_bind_by_name($stmt, ':LETTER_NUMBER',
                       $letter_number, -1);
      oci_bind_by_name($stmt, ':SMS_LOG_ID',
                       $sms_log_id, -1);
      oci_execute($stmt);
    }
    
    oci_free_statement($stmt);
  }

  /**
   * This function logs a sent SMS to the WTW_SMS_LOG table
   *
   * @param string $sms_id Unique SMS identifier (from SMS system)
   * @param string $recipient Mobile number of recipient
   *
   */
  public function logSMS($sms_id, $recipient, $message_text,
			 $tariff = 0, $originator = 2097) {

    $q = "INSERT INTO WTW.WTW_SMS_LOG " .
       "(ORIGINATOR, RECIPIENT, MESSAGE_TEXT, TARIFF, " .
       "SENT, SENT_DATETIME, MESSAGE_ID) " .
       "VALUES " .
       "(:ORIGINATOR, :RECIPIENT, :MESSAGE_TEXT, " .
       ":TARIFF, 'Y', SYSDATE, :MESSAGE_ID)";
    $stmt = oci_parse($this->dbConn, $q);

    oci_bind_by_name($stmt, ':ORIGINATOR',
		     $originator, -1);
    oci_bind_by_name($stmt, ':RECIPIENT',
                     $recipient, -1);
    oci_bind_by_name($stmt, ':MESSAGE_TEXT',
                     $message_text, -1);
    oci_bind_by_name($stmt, ':TARIFF',
                     $tariff, -1);
    oci_bind_by_name($stmt, ':MESSAGE_ID',
                     trim($sms_id), -1);

    if (oci_execute($stmt)) {
      oci_free_statement($stmt);
      // Return the inserted ID
      $q = "SELECT WTW_SMS_LOG_SEQ.currval as ID FROM dual";
      $stmt = oci_parse($this->dbConn, $q);
      oci_execute($stmt);

      $row = oci_fetch_assoc($stmt);
      print_r($row);
      oci_free_statement($stmt);
      return $row['ID'];
    }
    else {
      oci_free_statement($stmt);
      // Failure
      return 0;
    }
  }

  public function tagEmailPreDueNotificationSent($email_log_id, $bor, $loans) {
    return $this->tagEmailNotificationSent($email_log_id, $bor, $loans,
					   'P', NULL);
  }

  public function tagEmailOverdueNotificationSent($email_log_id, $bor,
						  $loans, $letter_number) {
    return $this->tagEmailNotificationSent($email_log_id, $bor, $loans,
                                           'O', $letter_number);
  }

  /**
   * Tag all notified loans contained in the emaill
   *
   * @param int $email_log_id
   * @param array $bor Borrower information (NOT USED!)
   * @param array $loans Loans contained in the email
   * @param string Notification type (P (predue) or O (overdue))
   * @param int $letter_number   
   *
   */
  public function tagEmailNotificationSent($email_log_id, $bor, $loans,
					   $notification = 'P',
					   $letter_number = NULL) {
    if (!$this->dbConn) return false;

    $q = "INSERT INTO WTW.WTW_EMAIL_NOTIFY " .
       "(LOAN_NUMBER, DUE_DATE, NOTIFICATION, LETTER_NUMBER, " .
       "EMAIL_LOG_ID, CREATED_DATETIME) " .
       "VALUES " .
       "(:LOAN_NUMBER, TO_DATE(:DUE_DATE, 'YYYYMMDD'), " .
       ":NOTIFICATION, :LETTER_NUMBER, :EMAIL_LOG_ID, SYSDATE)";

    $stmt = oci_parse($this->dbConn, $q);

    foreach ($loans as $l) {
      oci_bind_by_name($stmt, ':LOAN_NUMBER',
                       $l['Z36_NUMBER'], -1);
      oci_bind_by_name($stmt, ':DUE_DATE',
                       $l['Z36_DUE_DATE'], -1);
      oci_bind_by_name($stmt, ':NOTIFICATION',
                       $notification, -1);
      oci_bind_by_name($stmt, ':LETTER_NUMBER',
                       $letter_number, -1);
      oci_bind_by_name($stmt, ':EMAIL_LOG_ID',
                       $email_log_id, -1);
      oci_execute($stmt);
    }

    oci_free_statement($stmt);
  }

  /**
   * Update the loan's notification data (letter sent data)
   * Increments the letter number, sets the letter date and sets the note
   * Return number of rows affected
   *
   * @param string $z36_rec_key loan rec key
   * @param string $letter_date Letter date
   * @param string $note Note regarding notification
   * @return in
   */
  public function tagZ36LetterSent($z36_rec_key, $letter_date, $note = '') {
    if (!$this->dbConn) return false;

    // What about RECALL_DATE? Does it matter?

    if (empty($z36_rec_key) || empty($letter_date)) return false;

    if(strlen($note) > 30) {
        $note = substr($note,0,30);
    }

    $q = "UPDATE NOR50.Z36 " .
       "SET " .
       "Z36_LETTER_NUMBER = Z36_LETTER_NUMBER + 1, " .
       "Z36_LETTER_DATE = :LETTER_DATE, " .
       "Z36_NOTE_2 = :NOTE " .
       "WHERE Z36_REC_KEY = :Z36_REC_KEY";
    
    $stmt = oci_parse($this->dbConn, $q);
    oci_bind_by_name($stmt, ':Z36_REC_KEY',
		     $z36_rec_key, -1);
    oci_bind_by_name($stmt, ':LETTER_DATE',
		     $letter_date, -1);
    oci_bind_by_name($stmt, ':NOTE',
		     $note, -1);
   
    oci_execute($stmt);
    $num_rows = oci_num_rows($stmt);
    oci_free_statement($stmt);
    
    return $num_rows;
  }

  /**
   * NOT IN USE
   */
  public function setZ36Note($z36_rec_key, $note, $noteNumber = 2) {
    if (!$this->dbConn) return false;

    if (empty($z36_rec_key) || empty($note)) return false;

    if($noteNumber != 1 && $noteNumber != 2) return false;

    if(strlen($note) > 30) {
        $note = substr($note,0,30);
    }

    $q = "UPDATE NOR50.Z36 SET ";

    if($noteNumber == 1) {
      $q .= "Z36_NOTE_1=:NOTE "; 
    } else {
      $q .= "Z36_NOTE_2=:NOTE "; 
    }
    $q .= "WHERE Z36_REC_KEY = :Z36_REC_KEY";
    
    $stmt = oci_parse($this->dbConn, $q);
    oci_bind_by_name($stmt, ':Z36_REC_KEY',
		     $z36_rec_key, -1);
    oci_bind_by_name($stmt, ':NOTE',
		     $note, -1);
    
    oci_execute($stmt);
    $num_rows = oci_num_rows($stmt);
    oci_free_statement($stmt);
    
    return $num_rows;
  }


  /**
   *
   *
   *
   */
  public function getLastNotifiedLoansByMsisdn($msisdn) {
    
    // Vi trenger Z36_REC_KEY

    $q = "SELECT Z36_REC_KEY " .
       "FROM WTW.WTW_SMS_NOTIFY N, (" . 
       "SELECT ID FROM " .
       "(SELECT " .
       "ROW_NUMBER() OVER (ORDER BY CREATED_DATETIME DESC) AS rownumber, " .
       "ID " .
       "FROM WTW.WTW_SMS_LOG " .
       "WHERE RECIPIENT = :RECIPIENT) " .
       "WHERE rownumber = 1) L, NOR50.Z36 Z36 " .
       "WHERE N.SMS_LOG_ID = L.ID " .
       "AND Z36_NUMBER = N.LOAN_NUMBER";
    
    $stmt = oci_parse($this->dbConn, $q);
    
    oci_bind_by_name($stmt, ':RECIPIENT',
		     $msisdn, -1);
    
    oci_execute($stmt);
    
    $loans = array();
    while ($row = oci_fetch_assoc($stmt)) {
      if (is_array($row)) {
        $loans[] = $row['Z36_REC_KEY'];
      }
    }
    
    oci_free_statement($stmt);
    
    return $loans;
  }

  /**
   * Get a loan's due date
   *
   * @param string $loan_nr the loan number (Z36_REC_KEY)
   * @return string
   */
  public function getDueDateByLoanNr($loan_nr) {

    if (!$this->dbConn) return false;

    $q = "SELECT Z36_DUE_DATE FROM NOR50.Z36 " .
       "WHERE Z36_REC_KEY = :LOAN_NR";

    $stmt = oci_parse($this->dbConn, $q);

    oci_bind_by_name($stmt, ':LOAN_NR',
                     $loan_nr, -1);

    oci_execute($stmt);
    $row = oci_fetch_assoc($stmt);

    if (!empty($row['Z36_DUE_DATE'])) {
      oci_free_statement($stmt);

      return $row['Z36_DUE_DATE'];
    }

    oci_free_statement($stmt);
    return false;

  }

  /**
   * Get borrower ID by Z36_REC_KEY
   *
   * @param $loan_nr Loan number (Z36_REC_KEY)
   * @return string
   */
  public function getBorIdByLoanNr($loan_nr) {

    if (!$this->dbConn) return false;

    $q = "SELECT Z36_ID FROM NOR50.Z36 " .
       "WHERE Z36_REC_KEY = :LOAN_NR";
    
    $stmt = oci_parse($this->dbConn, $q);

    oci_bind_by_name($stmt, ':LOAN_NR',
                     $loan_nr, -1);
    
    oci_execute($stmt);
    $row = oci_fetch_assoc($stmt);

    if (!empty($row['Z36_ID'])) {
      oci_free_statement($stmt);

      return $row['Z36_ID'];
    }

    oci_free_statement($stmt);
    return false;

  }

  public function getLoanNrByRecKey($recKey) {

    if (!$this->dbConn) return false;

    $q = "SELECT Z36_NUMBER FROM NOR50.Z36 " .
       "WHERE Z36_REC_KEY = :REC_KEY";
    
    $stmt = oci_parse($this->dbConn, $q);

    oci_bind_by_name($stmt, ':REC_KEY',
                     $recKey, -1);
    
    oci_execute($stmt);
    $row = oci_fetch_assoc($stmt);

    if (!empty($row['Z36_NUMBER'])) {
      oci_free_statement($stmt);

      return $row['Z36_NUMBER'];
    }

    oci_free_statement($stmt);
    return false;

  }

  
  /**
   * Get the status of a loan
   *
   * @param string $loan_nr Loan number (Z36_REC_KEY)
   * @return string
   */
  public function getItemStatusByLoanNr($loan_nr) {

    if (!$this->dbConn) return false;

    $q = "SELECT Z36_ITEM_STATUS FROM NOR50.Z36 " .
       "WHERE Z36_REC_KEY = :LOAN_NR";

    $stmt = oci_parse($this->dbConn, $q);

    oci_bind_by_name($stmt, ':LOAN_NR',
                     $loan_nr, -1);

    oci_execute($stmt);
    $row = oci_fetch_assoc($stmt);

    if (!empty($row['Z36_ITEM_STATUS'])) {
      oci_free_statement($stmt);

      return $row['Z36_ITEM_STATUS'];
    }

    oci_free_statement($stmt);
    return false;

  }

  
  public function getLastNotifiedLoansByEmailId($id) {
    return $this->getLastNotifiedLoansByEmail($id, 'ID');
  }

  public function getLastNotifiedLoansByEmailAddress($address) {
    return $this->getLastNotifiedLoansByEmail($address, 'EMAIL');
  }

  /**
   * Get all loans that were notified in an email notification
   * If type ID, the given value is used to search MESSAGE_ID
   * If type EMAIL, the given value is used to search RECIPIENT
   *
   * @param string $val Value to check for
   * @param string $type Type, ID or EMAIL
   * @return array
   */
  public function getLastNotifiedLoansByEmail($val, $type = 'ID') {

    $val = trim($val);
    // Vi trenger Z36_REC_KEY

    $q = "SELECT Z36_REC_KEY " .
       "FROM WTW.WTW_EMAIL_NOTIFY N, (" .
       "SELECT ID FROM " .
       "(SELECT " .
       "ROW_NUMBER() OVER (ORDER BY CREATED_DATETIME DESC) AS rownumber, " .
       "ID " .
       "FROM WTW.WTW_EMAIL_LOG " .
       "WHERE ";

    if ($type == 'EMAIL') {
      $q .= "RECIPIENT = :VAL) ";
    }
    else {
      $q .= "MESSAGE_ID = :VAL) ";
    }
    $q .= "WHERE rownumber = 1) L, NOR50.Z36 Z36 " .
       "WHERE N.EMAIL_LOG_ID = L.ID " .
       "AND Z36_NUMBER = N.LOAN_NUMBER";

    $stmt = oci_parse($this->dbConn, $q);

    oci_bind_by_name($stmt, ':VAL',
                     $val, -1);

    oci_execute($stmt);

    $loans = array();
    while ($row = oci_fetch_assoc($stmt)) {
      if (is_array($row)) {
        $loans[] = $row['Z36_REC_KEY'];
      }
    }

    oci_free_statement($stmt);

    return $loans;
  }


  public function getLastNotifiedLoansBySMSId($id) {
    return $this->getLastNotifiedLoansBySMS($id, 'ID');
  }

  public function getLastNotifiedLoansByMobile($mobile) {
    return $this->getLastNotifiedLoansBySMS($mobile, 'MOBILE');
  }
  
  /**
   * Get all loans that were notified in an SMS notification
   * If type ID, the given value is used to search ID
   * If type MOBILE, the given value is used to search RECIPIENT
   *
   * @param string $val Value to check for
   * @param string $type Type, ID or EMAIL
   * @return array
   */
  public function getLastNotifiedLoansBySMS($val, $type = 'ID') {

    $val = trim($val);
    // Vi trenger Z36_REC_KEY

    $q = "SELECT Z36_REC_KEY " .
       "FROM WTW.WTW_SMS_NOTIFY N, (" .
       "SELECT ID FROM " .
       "(SELECT " .
       "ROW_NUMBER() OVER (ORDER BY CREATED_DATETIME DESC) AS rownumber, " .
       "ID " .
       "FROM WTW.WTW_SMS_LOG " .
       "WHERE ";

    if ($type == 'MOBILE') {
      $q .= "RECIPIENT = :VAL) ";
    }
    else {
      $q .= "ID = :VAL) ";
    }
    $q .= "WHERE rownumber = 1) L, NOR50.Z36 Z36 " .
       "WHERE N.SMS_LOG_ID = L.ID " .
       "AND Z36_NUMBER = N.LOAN_NUMBER";

    $stmt = oci_parse($this->dbConn, $q);

    oci_bind_by_name($stmt, ':VAL',
                     $val, -1);

    oci_execute($stmt);

    $loans = array();
    while ($row = oci_fetch_assoc($stmt)) {
      if (is_array($row)) {
        $loans[] = $row['Z36_REC_KEY'];
      }
    }

    oci_free_statement($stmt);

    return $loans;
  }


   /**
   * Get the last loan that was notified from hold shelf in an SMS notification
   * If type ID, the given value is used to search ID
   * If type MOBILE, the given value is used to search RECIPIENT
   *
   * @param string $val Value to check for
   * @param string $type Type, ID or EMAIL
   * @return array
   */
  public function getLastHoldShelfLoanBySMS($val, $type = 'ID') {

    $val = trim($val);
    // Vi trenger Z36_REC_KEY

    $q = "SELECT Z37_REC_KEY " .
       "FROM WTW.WTW_HOLDSHELF_SMS N, (" .
       "SELECT ID FROM " .
       "(SELECT " .
       "ROW_NUMBER() OVER (ORDER BY SENT_DATETIME DESC) AS rownumber, " .
       "ID " .
       "FROM WTW.WTW_HOLDSHELF_SMS " .
       "WHERE ";

    if ($type == 'MOBILE') {
      $q .= "RECIPIENT = :VAL) ";
    }
    else {
      $q .= "ID = :VAL) ";
    }
    $q .= "WHERE rownumber = 1) L, NOR50.Z37 Z37 " .
       "WHERE N.ID = L.ID " .
       "AND Z37.Z37_REC_KEY = N.HOLDSHELF_ID";

    $stmt = oci_parse($this->dbConn, $q);

    oci_bind_by_name($stmt, ':VAL',
                     $val, -1);

    oci_execute($stmt);

    $loans = array();
    while ($row = oci_fetch_assoc($stmt)) {
      if (is_array($row)) {
        $loans[] = $row['Z37_REC_KEY'];
      }
    }

    oci_free_statement($stmt);

    return $loans;
  }
 

  /**
   * Log a sent email to the WTW_EMAIL_LOG table
   * Returns the inserted ID or 0 if failed
   *
   * @param string $email_id Email ID
   * @param string $recipient Recipient email address
   *
   * @return int
   */
  public function logEmail($email_id, $recipient) {

    $q = "INSERT INTO WTW.WTW_EMAIL_LOG " .
       "(RECIPIENT, SENT, SENT_DATETIME, MESSAGE_ID) " .
       "VALUES " .
       "(:RECIPIENT, 'Y', SYSDATE, :MESSAGE_ID)";
    $stmt = oci_parse($this->dbConn, $q);
    
    oci_bind_by_name($stmt, ':RECIPIENT',
                     $recipient, -1);
    oci_bind_by_name($stmt, ':MESSAGE_ID',
                     $email_id, -1);
    
    if (oci_execute($stmt)) {
      oci_free_statement($stmt);
      // Return the inserted ID
      $q = "SELECT WTW_EMAIL_LOG_SEQ.currval as ID FROM dual";
      $stmt = oci_parse($this->dbConn, $q);
      oci_execute($stmt);

      $row = oci_fetch_assoc($stmt);
      print_r($row);
      oci_free_statement($stmt);
      return $row['ID'];
    }
    else {
      oci_free_statement($stmt);
      // Failure
      return 0;
    }
  }


  /**
   *
   *
   *
   */
  public function getOverdueBors() {

    if (!$this->dbConn) return false;

    $q = "SELECT Z36_ID, COUNT(*) as ANTALL, Z304_TELEPHONE_3, " .
       "Z304_EMAIL_ADDRESS, Z36_DUE_DATE " .
       "FROM NOR50.Z36, USR01.Z304 " .
       "WHERE " .
       // "(Z36_DUE_DATE - TO_CHAR(sysdate, 'YYYYMMDD')) = -1 " .
       "FLOOR(TO_DATE(Z36_DUE_DATE, 'YYYYMMDD') - sysdate) = -1 " .
       "AND Z36_RETURNED_DATE = 0 " .
       // Should not have recieved letter before
       "AND Z36_LETTER_NUMBER = '00' " .
       "AND Z36_ID = SUBSTR(Z304_REC_KEY, 1, 12) " .
       "AND (((Z304_TELEPHONE_3 LIKE '9%' OR  Z304_TELEPHONE_3 LIKE '4%') " .
       "AND LENGTH(Z304_TELEPHONE_3) = 8) " .
       "OR LENGTH(TRIM(Z304_EMAIL_ADDRESS)) IS NOT NULL) " .
      
       // "AND Z36_ID = 'STV000106678' " .
       // "AND Z36_ID = 'STV000055021' " . 
 /*
       "AND NOT EXISTS ( " .
       "  SELECT W.LOAN_NUMBER FROM WTW_NOTIFY_PRE_DUE W" .
       "  WHERE W.LOAN_NUMBER = Z36.Z36_NUMBER " .
       "  AND Z36.Z36_DUE_DATE = TO_CHAR(W.DUE_DATE, 'YYYYMMDD')) " .
    */
       "GROUP BY Z36_ID, Z36_DUE_DATE, Z304_TELEPHONE_3, Z304_EMAIL_ADDRESS";
    
    echo "$q\n";

    $stmt = oci_parse($this->dbConn, $q);
    oci_execute($stmt);

    $bors = array();
    while ($row = oci_fetch_assoc($stmt)) {
      if (is_array($row)) {
        $bors[] = $row;
      }
    }

    oci_free_statement($stmt);

    return $bors;
  }

  /**
   * Get all hold shelf letters by reading
   * them from disc in the hold shelf letter directory
   * Returns an array of filenames
   *
   * @return array
   */
  public static function getHoldShelfLetterFiles() {
    
    $dir = HOLD_SHELF_LETTERS_DIR;

    $files = array();
    if (is_dir($dir)) {
      if ($dh = opendir($dir)) {
        while (($file = readdir($dh)) !== false) {
	  if (is_file($dir . $file) && $file != '.' && $file != '..') {
	    $files[] = $file;
	  }
        }
        closedir($dh);
      }
    }
    
    return $files;
  }


  /**
   *
   * @param string $letterfile Letter filename
   *
   */
  public function getHoldShelfLetters($letter_files) {
    
    $letters = array();
    
    foreach($letter_files as $letter_file) {
        // Read hold shelf letter from disc
        $xml_data = file_get_contents(HOLD_SHELF_LETTERS_DIR . $letter_file);
        $xml = simplexml_load_string($xml_data);
        
        $letterdata = array();
        $letterdata['letter_file']      = $letter_file;
	$letterdata['bor_id']           = $xml->{'section-01'}->{'z37-id'};
        $letterdata['email']            = $xml->{'section-01'}->{'email-address'};
        $letterdata['mobnr']            = $xml->{'section-01'}->{'z302-telephone-3'};

        $letterdata['author']           = $xml->{'section-01'}->{'z13-author'};
        $letterdata['title']            = $xml->{'section-01'}->{'z13-title'};
        $letterdata['pickup_location']  = $xml->{'section-01'}->{'z37-pickup-location'};
        $letterdata['end_hold_date']    = $xml->{'section-01'}->{'z37-end-hold-date'};
	$letterdata['request_date']     = $xml->{'section-01'}->{'z37-request-date'};
        
        $letterdata['hold_shelf_id']    = $xml->{'section-01'}->{'z37-doc-number'} 
                                        . $xml->{'section-01'}->{'z37-item-sequence'} 
                                        . $xml->{'section-01'}->{'z37-sequence'};
        
        foreach($letterdata as $key => $val) {
            $letterdata[$key] = trim((string)$val);
        }
        
        $letters[] = $letterdata;
    }

    return $letters;

  }
    
  public function holdShelfLetterProcessed($letter_file) {
    echo "letter: " . $letter_file . "\n";
    $q  = "SELECT sysdate FROM dual ";
    $q .= "WHERE EXISTS (SELECT ID FROM WTW.WTW_HOLDSHELF_EMAIL WHERE FILENAME=:FILENAME) ";
    $q .= "OR EXISTS (SELECT ID FROM WTW.WTW_HOLDSHELF_SMS WHERE FILENAME=:FILENAME) ";
    $q .= "OR EXISTS (SELECT ID FROM WTW.WTW_HOLDSHELF_POST WHERE FILENAME=:FILENAME) ";
    
    $stmt = oci_parse($this->dbConn, $q);
    
    oci_bind_by_name($stmt, ':FILENAME',
                     $letter_file, -1);

    oci_execute($stmt);
    $row = oci_fetch_assoc($stmt);
    oci_free_statement($stmt);
    
    if(!empty($row)) {
      return true;
    }

    return false;
  }
  
  /**
   *
   *
   */
  public function logHoldShelfEmail($letter_file,$hold_shelf_id,$bor_id,$recipient,$message_id) {
    $q = "INSERT INTO WTW.WTW_HOLDSHELF_EMAIL" .
       "(FILENAME,HOLDSHELF_ID,BOR_ID,RECIPIENT,SENT_DATETIME,MESSAGE_ID) " .
       "VALUES " .
       "(:FILENAME,:HOLDSHELF_ID,:BOR_ID,:RECIPIENT, SYSDATE, :MESSAGE_ID)";
    $stmt = oci_parse($this->dbConn, $q);
    
    oci_bind_by_name($stmt, ':FILENAME',
                     $letter_file, -1);
    oci_bind_by_name($stmt, ':HOLDSHELF_ID',
                     $hold_shelf_id, -1);
    oci_bind_by_name($stmt, ':BOR_ID',
                     $bor_id, -1);
    oci_bind_by_name($stmt, ':RECIPIENT',
                     $recipient, -1);
    oci_bind_by_name($stmt, ':MESSAGE_ID',
                     $message_id, -1);
    
    if (oci_execute($stmt)) {
      oci_free_statement($stmt);
      // Return the inserted ID
      $q = "SELECT WTW_HOLDSHELF_EMAIL_SEQ.currval as ID FROM dual";
      $stmt = oci_parse($this->dbConn, $q);
      oci_execute($stmt);

      $row = oci_fetch_assoc($stmt);
      print_r($row);
      oci_free_statement($stmt);
      return $row['ID'];
    }
    else {
      oci_free_statement($stmt);
      // Failure
      return 0;
    }
  }
  
  /**
   *
   *
   */
  public function logHoldShelfSMS($letter_file, $hold_shelf_id, $bor_id, $recipient, $message_id, $message_text, $tariff = 0, $originator = 2097) {
    $q = "INSERT INTO WTW.WTW_HOLDSHELF_SMS" .
       "(FILENAME,HOLDSHELF_ID,BOR_ID,ORIGINATOR,RECIPIENT,MESSAGE_TEXT,TARIFF,SENT_DATETIME,MESSAGE_ID) " .
       "VALUES " .
       "(:FILENAME, :HOLDSHELF_ID, :BOR_ID, :ORIGINATOR, :RECIPIENT, :MESSAGE_TEXT, :TARIFF, SYSDATE, :MESSAGE_ID)";
    $stmt = oci_parse($this->dbConn, $q);
    
    oci_bind_by_name($stmt, ':FILENAME',
                     $letter_file, -1);
    oci_bind_by_name($stmt, ':HOLDSHELF_ID',
                     $hold_shelf_id, -1);
    oci_bind_by_name($stmt, ':BOR_ID',
                     $bor_id, -1);                    
    oci_bind_by_name($stmt, ':ORIGINATOR',
                     $originator, -1);
    oci_bind_by_name($stmt, ':RECIPIENT',
                     $recipient, -1);
    oci_bind_by_name($stmt, ':MESSAGE_TEXT',
                     $message_text, -1);
    oci_bind_by_name($stmt, ':TARIFF',
                     $tariff, -1);                    
    oci_bind_by_name($stmt, ':MESSAGE_ID',
                     $message_id, -1);
    
    if (oci_execute($stmt)) {
      oci_free_statement($stmt);
      // Return the inserted ID
      $q = "SELECT WTW_HOLDSHELF_SMS_SEQ.currval as ID FROM dual";
      $stmt = oci_parse($this->dbConn, $q);
      oci_execute($stmt);

      $row = oci_fetch_assoc($stmt);
      print_r($row);
      oci_free_statement($stmt);
      return $row['ID'];
    }
    else {
      oci_free_statement($stmt);
      // Failure
      return 0;
    }
  }

  /**
   *
   *
   */
  public function logHoldShelfPost($letter_file, $hold_shelf_id, $bor_id) {
    $q = "INSERT INTO WTW.WTW_HOLDSHELF_POST " .
       "(FILENAME, HOLDSHELF_ID, BOR_ID, SENT_DATETIME) " .
       "VALUES " .
       "(:FILENAME, :HOLDSHELF_ID, :BOR_ID, SYSDATE)";
    $stmt = oci_parse($this->dbConn, $q);
    
    oci_bind_by_name($stmt, ':FILENAME',
                     $letter_file, -1);
    oci_bind_by_name($stmt, ':HOLDSHELF_ID',
                     $hold_shelf_id, -1);                    
    oci_bind_by_name($stmt, ':BOR_ID',
                     $bor_id, -1);                    
   
    if (oci_execute($stmt)) {
      oci_free_statement($stmt);
      // Return the inserted ID
      $q = "SELECT WTW_HOLDSHELF_POST_SEQ.currval as ID FROM dual";
      $stmt = oci_parse($this->dbConn, $q);
      oci_execute($stmt);

      $row = oci_fetch_assoc($stmt);
      print_r($row);
      oci_free_statement($stmt);
      return $row['ID'];
    }
    else {
      oci_free_statement($stmt);
      // Failure
      return 0;
    }

  }
  
  /**
   *
   *
   *
   *
   */
  public function getBorIdByBorNr($bor_nr) {

    // Strip NOR50 from end of REC_KEY, and ignore 01 at beginning
    $q = "SELECT Z308_ID " .
//TRIM(SUBSTR(Z308_REC_KEY, 3, 15)) as BOR_NR " .
       "FROM USR01.Z308 " .
       "WHERE " .
       "SUBSTR(Z308_REC_KEY, 3, LENGTH(:BOR_NR)) = :BOR_NR " .
       "AND SUBSTR(Z308_REC_KEY, 1, 2) = '01'";

    // Make Bor_id uppercase
    $bor_id = strtoupper($bor_id);

    $stmt = oci_parse($this->dbConn, $q);
    oci_bind_by_name($stmt, ':BOR_NR',
                     $bor_id, -1);

    oci_execute($stmt);

    $bors = array();
    while ($row = oci_fetch_assoc($stmt)) {
      if (is_array($row)) {
        $bors[] = $row;
      }
    }

    oci_free_statement($stmt);

    if (!empty($bors[0]['Z308_ID'])) {
      return $bors[0]['Z308_ID'];
    }
    return false;
  }


  public function getBorNrFromBorId($bor_id) {

    // Strip NOR50 from end of REC_KEY, and ignore 01 at beginning
    $q = "SELECT TRIM(SUBSTR(Z308_REC_KEY, 3, 15)) as BOR_NR " .
       "FROM USR01.Z308 " .
       "WHERE " .
       "Z308_ID = :BOR_ID " . 
       "AND SUBSTR(Z308_REC_KEY, 1, 2) = '01'";

    // Make Bor_id uppercase
    $bor_id = strtoupper($bor_id);
    
    $stmt = oci_parse($this->dbConn, $q);
    oci_bind_by_name($stmt, ':BOR_ID',
                     $bor_id, -1);
    
    oci_execute($stmt);
    
    $bors = array();
    while ($row = oci_fetch_assoc($stmt)) {
      if (is_array($row)) {
        $bors[] = $row;
      }
    }

    oci_free_statement($stmt);

    if (!empty($bors[0]['BOR_NR'])) {
      return $bors[0]['BOR_NR'];
    }
    return false;
  }
 

  public function forgotPassword($bor_id) {
    
    $q = "SELECT Z308_ID, Z304_TELEPHONE_3, Z308_VERIFICATION " .
       "FROM USR01.Z308, USR01.Z304 " .
       "WHERE Z308_ID = SUBSTR(Z304_REC_KEY, 1, 12) " .
       "AND (Z304_TELEPHONE_3 LIKE '9%' OR  " .
       "  Z304_TELEPHONE_3 LIKE '4%') " .
       "AND LENGTH(Z304_TELEPHONE_3) = 8 " .
       // Z308_REC_KEY may look like this: 00STV000000003      NOR50
       // REMOVE last part (NOR50) and trim spaces before matching bor id
       " AND TRIM(SUBSTR(Z308_REC_KEY, 1, 17)) = '01' || :BOR_ID";
       //" AND SUBSTR(Z308_REC_KEY, 1, LENGTH('01' || :BOR_ID)) = '01' || :BOR_ID";

    // Make Bor_id uppercase
    $bor_id = strtoupper($bor_id);


    $stmt = oci_parse($this->dbConn, $q);
    oci_bind_by_name($stmt, ':BOR_ID',
                     $bor_id, -1);

    oci_execute($stmt);
    
    $bors = array();
    while ($row = oci_fetch_assoc($stmt)) {
      if (is_array($row)) {
        $bors[] = $row;
      }
    }

    oci_free_statement($stmt);
    
    return $bors[0];
  }

public function forgotBorrowID($bor_nr) {
    
    $q = "SELECT TRIM(substr(Z308_REC_KEY,3,20)) BOR_ID " .
        "FROM USR01.Z308, USR01.Z304 " .
        "WHERE Z308_ID = SUBSTR(Z304_REC_KEY, 1, 12) " .
        "AND Z304_TELEPHONE_3 IS NOT NULL " .
        "AND TRIM(SUBSTR(Z308_REC_KEY, 1, 2)) = '01' " .
        "AND Z304_TELEPHONE_3 = :BOR_NR " .
        "GROUP BY Z308_ID, Z308_REC_KEY, Z304_TELEPHONE_3";

    // Check bor_nr
    $bor_nr = strtoupper($bor_nr);

    $stmt = oci_parse($this->dbConn, $q);
    oci_bind_by_name($stmt, ':BOR_NR',
                     $bor_nr, -1);

    oci_execute($stmt);
    
    $bors = array();
    while ($row = oci_fetch_assoc($stmt)) {
      if (is_array($row)) {
        $bors[] = $row['BOR_ID'];
      }
    }

    oci_free_statement($stmt);
    
    return $bors;
  }

  public function getSMSWithoutStatus() {
    $q = "SELECT ID,MESSAGE_ID,RECIPIENT,FLOOR(sysdate - SENT_DATETIME) as AGE FROM WTW_SMS_LOG WHERE STATUS IS NULL";
    $stmt = oci_parse($this->dbConn, $q);
    oci_execute($stmt);

    $messages = array();
    while($row = oci_fetch_assoc($stmt)) {
      if(is_array($row)) {
        $messages[] = $row;
      }

    }

    oci_free_statement($stmt);

    return $messages;

  }


  
  public function setSMSStatus($id,$status = 'S') {
    $q = "UPDATE WTW_SMS_LOG SET STATUS=:STATUS WHERE ID=:ID";
    $stmt = oci_parse($this->dbConn, $q);
    oci_bind_by_name($stmt,':STATUS',$status,-1);
    oci_bind_by_name($stmt,':ID',$id,-1);

    oci_execute($stmt);

    oci_free_statement($stmt);
  }

  public function getHoldShelfSMSWithoutStatus() {
    $q = "SELECT ID,MESSAGE_ID,HOLDSHELF_ID,BOR_ID,RECIPIENT,FLOOR(sysdate - SENT_DATETIME) as AGE FROM WTW_HOLDSHELF_SMS WHERE STATUS IS NULL";
    $stmt = oci_parse($this->dbConn, $q);
    oci_execute($stmt);

    $messages = array();
    while($row = oci_fetch_assoc($stmt)) {
      if(is_array($row)) {
        $messages[] = $row;
      }

    }

    oci_free_statement($stmt);

    return $messages;

  }


  public function setHoldShelfSMSStatus($id,$status = 'S') {
    $q = "UPDATE WTW_HOLDSHELF_SMS SET STATUS=:STATUS WHERE ID=:ID";
    $stmt = oci_parse($this->dbConn, $q);
    oci_bind_by_name($stmt,':STATUS',$status,-1);
    oci_bind_by_name($stmt,':ID',$id,-1);

    oci_execute($stmt);

    oci_free_statement($stmt);
  }



}
?>
