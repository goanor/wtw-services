<?php
/*!
 * Klasse for parsing og henting av MMS og/eller SMS
 * fra SMSRespons sitt XML interface
 *
 * Christian Aune Thomassen
 * 2006 - WapTheWeb AS
 */

class SMSRespons {

  var $XMLFile  = 'http://www.smsrespons.no/xml.php';
  var $SMSoutURL  = 'http://www.smsrespons.no/sendsmshttp.php';
  var $SMSstatusURL = 'http://www.smsrespons.no/getstatus.php';
  var $codeword = '';
  var $password = '';
  var $type = '';
  var $beforeDate = '';
  var $afterDate = '';
  var $offset = '';
  var $limit = '';
  var $from = '';
  var $notfrom = '';
  var $id = '';
  var $fromid = '';
  var $requireimage = false;

  var $content;

  var $activeTag = '';
  var $smsCount = 0;
  var $mmsCount = 0;
  var $errorCount = 0;
		
  var $version = '1.1';

  /*!
   * 
   * \param $codword Kodeordet du vil benytte
   * \param $password Passord i klartekst tilh�rende kodeord
   */
  function SMSRespons($codeword, $password)
    {
      $this->codeword = $codeword;
      $this->password = $password;
    }


  /*!
   * Funksjon for � hente innholdet. Denne m� kalles tilslutt etter
   * at eventuelle filtere/restriksjoner er satt
   *
   * \return Returnerer en instans av Content
   */
  function getContent()
    {
      $this->content = new Content();
      if (!$contents = file_get_contents($this->makeURL($this->XMLFile))) {
	trigger_error("Could get content of $this->XMLFile.",
		      E_USER_WARNING);
	return FALSE;
      }

      $this->parseXML($contents);

      return $this->content;


    }

  /*!
   * Funksjon for � sette om man �nsker � hente mms eller sms
   * Standardinstillinger er b�de mms og sms, men ved � benytte 
   * denne kan man velge en av delene.
   * \param $type gyldige verdier er mms eller sms
   */
  function setType($type)
    {
      $this->type = $type;
    }

  /*!
   *
   * \param $date
   */
  function setBeforeDate($date)
    {
      $this->beforeDate = $date;
    }

  /*!
   *
   * \param $date
   */
  function setAfterDate($date)
    {
      $this->afterDate = $date;
    }

  /*!
   *
   * \param
   */
  function setFrom($numbers)
    {
      $this->from = $numbers;
    }

  /*!
   *
   * \param $numbers
   */
  function setNotFrom($numbers)
    {
      $this->notfrom = $numbers;
    }

  /*!
   *
   * \param $offset
   */
  function setOffset($offset)
    {
      $this->offset = $offset;
    }

  /*!
   *
   * \param
   */
  function setLimit($limit)
    {
      $this->limit = $limit;
    }

  /*!
   *
   * \param $file
   */
  function setXMLFile($file)
    {
      $this->XMLFile = $file;
    }

  /*!
   *
   * \param $id
   */
  function setID($id)
    {
      $this->id = $id;
    }
		
  /*
   * \param $id
   */
  function setFromID($id)
    {
      $this->fromid = $id;
    }		

  /*!
   *
   * \param $flag
   */
  function setRequireImage($flag)
    {
      $this->requireimage = $flag;
    }

  /*!
   *
   * \return true eller false hvis det er registrert feil.
   */
  function hasErrors()
    {
      if (count($this->errors > 0))
	return true;
      else
	return false;
    }


  /*!
   *
   * \param $XMLfile
   */
  // PRIVATE functions not to be called directly
  function makeURL($XMLfile)
    {
      $url = "{$XMLfile}?c=" . urlencode($this->codeword) . "&p=" . md5($this->password);

      // Now add more arguments if they are set
      if (!empty($this->type)) {
	$url .= "&type=$this->type";
      }

      if (!empty($this->beforeDate)) {
	$url .= "&before=$this->beforeDate";
      }

      if (!empty($this->afterDate)) {
	$url .= "&after=$this->afterDate";
      }

      if (!empty($this->offset)) {
	$url .= "&offset=$this->offset";
      }

      if (!empty($this->limit)) {
	$url .= "&limit=$this->limit";
      }

      if (!empty($this->from)) {
	$url .= "&from=$this->from";
      }

      if (!empty($this->notfrom)) {
	$url .= "&notfrom=$this->notfrom";
      }

      if (!empty($this->id)) {
	$url .= "&id=$this->id";
      }
      if (!empty($this->fromid)) {
	$url .= "&fromid=$this->fromid";
      }			

      if ($this->requireimage) {
	$url .= "&requireimage=1";
      }

      return $url;
    }


  /*!
   *
   * \param $XML
   */
  function parseXML($XML) {

    $xml_parser = xml_parser_create('ISO-8859-1');
    xml_parser_set_option($xml_parser, XML_OPTION_CASE_FOLDING, false);
    xml_parse_into_struct($xml_parser, $XML, $arr_vals);
    xml_parser_free($xml_parser);

    foreach ($arr_vals as $tag) {
      if ($tag['type'] == 'open') {
	switch(strtoupper($tag['tag'])) {
	case 'SMS':
	  $this->content->sms[$this->smsCount] = new SMS();
	$this->activeTag = $tag['tag'];
	break;
	case 'MMS':
	  $this->content->mms[$this->mmsCount] = new MMS();
	$this->activeTag = $tag['tag'];
	break;
	case 'ERRORS':
	  $this->activeTag = $tag['tag'];
	break;
	}
      }

      if ($tag['type'] == 'complete') {
	switch(strtoupper($this->activeTag)) {
	case 'SMS':
	  $this->content->sms[$this->smsCount]->{$tag['tag']} = $tag['value'];
	  break;
	case 'MMS':
	  $this->content->mms[$this->mmsCount]->{$tag['tag']} = $tag['value'];
	  break;
	case 'ERRORS':
	  $this->content->errors[$this->errorCount] = $tag['value'];
	break;
	}
      }

      if ($tag['type'] == 'close') {
	switch(strtoupper($tag['tag'])) {
	case 'SMS':
	  $this->smsCount++;
	$this->activeTag = '';
	break;
	case 'MMS':
	  $this->mmsCount++;
	$this->activeTag = '';
	break;
	case 'ERRORS':
	  $this->errorCount++;
	$this->activeTag = '';
	break;
	}
      }
    }

    $this->content->numSMS = $this->smsCount;
    $this->content->numMMS = $this->mmsCount;
    $this->content->numErrors = $this->errorCount;

    return TRUE;
  }

		
		
  function sendSMS($to, $message, $NOK_price, $disable_from = false,
		   $originator = '2097')
    {
      $url = "{$this->SMSoutURL}?c=" . urlencode($this->codeword) . "&p=" . md5($this->password);
      $url .= "&to=$to";
      $url .= '&text=' . urlencode($message);
      $url .= "&price=$NOK_price";
      if ($disable_from) {
	$url .= "&disable_from=1";
      }
      if (!empty($originator) && $originator != '2097') {
	$url .= '&originator=' . urlencode($originator);
      }
      if (!$id = file_get_contents($url)) {
	trigger_error("No ID returned from $url.",
		      E_USER_WARNING);
	return FALSE;
      }
      return $id;
    }
		
		
  function getStatus($id)
    {
      $url = "{$this->SMSstatusURL}?c=" . urlencode($this->codeword) . "&p=" . md5($this->password);
      $url .= "&id=$id";
      if (!$status = file_get_contents($url)) {
	trigger_error("Could get status from $url.",
		      E_USER_WARNING);
	return FALSE;
      }
      return $status;
    }
		
}


/*!
 * Klasse som holder en SMS
 *
 */
class SMS {
  var $id = '';
  var $originator = '';
  var $datetime = '';
  var $unixtime = '';
  var $text = '';
}

/*!
 * Klasse som holder en MMS
 *
 */
class MMS {
  var $id = '';
  var $originator = '';
  var $datetime = '';
  var $unixtime = '';
  var $text = '';
  var $image = '';
  var $thumbnail = '';
}

/*!
 * Klasse som holder resultatet av parsingen av XML dokumentet
 * Det er en instans av denne som returneres av SMSRespons::getContent()
 */
class Content {
  var $sms = '';
  var $mms = '';
  var $errors = '';
  var $numSMS = '';
  var $numMMS = '';
  var $numErrors = '';
}

?>
