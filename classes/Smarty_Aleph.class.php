<?php
require_once(SMARTY_LIB);


class Smarty_Aleph extends Smarty {


  function __construct() {
    $this->Smarty();

    
    $this->template_dir = SMARTY_TEMPLATE_DIR;
    $this->compile_dir  = SMARTY_TEMPLATE_C_DIR;
    // $this->config_dir   = '/web/www.example.com/guestbook/configs/';
    // $this->cache_dir    = ;

    $this->caching = false;
  }

}
?>