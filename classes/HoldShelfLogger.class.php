<?php


class HoldShelfLogger {

  private $sourceDir = '';
  private $targetDir = '';

  function __construct($sourceDir,$targetDir) {
    $this->sourceDir = $sourceDir;
    $this->targetDir = $targetDir;
  }


  public function copyFile($filename) {
    // Generate directory to store file in
    // if not exists


    if ($this->mkdir()) {
        $fromFile = $this->sourceDir . $filename;
        $toFile = $this->targetDir . '/' . $filename;

        if (copy($fromFile,$toFile)) {
	  return true;
	}
    }
    return false;
  }

  public function unlinkSourceFile($filename) {
    $fromFile = $this->sourceDir . $filename;
    if (unlink($fromFile)) {
      return true;
    }
    else {
      return false;
    }
  }


  private function mkdir() {

    if (!file_exists($this->targetDir) || !is_dir($this->targetDir)) {
      return mkdir($this->targetDir, 0777, true); // recursive
    }

    return true;
  }

}

?>
