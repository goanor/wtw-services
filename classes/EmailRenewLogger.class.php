<?php

class EmailRenewLogger {
  private $dbConn = null;
  
  function __construct($dbConn) {
    $this->dbConn = $dbConn;
  }
  

  /*
SQL-WTW> desc wtw_email_renew_log;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 ID                                        NOT NULL NUMBER
 LOAN_NUMBER                               NOT NULL CHAR(9)
 BOR_ID                                    NOT NULL CHAR(12)
 SENT_DATETIME                             NOT NULL DATE
 RESULT                                    NOT NULL CHAR(1)
 RESULT_MESSAGE                                     VARCHAR2(1280)
 NEW_DUE_DATE                                       DATE
 EMAIL_LOG_ID                                       NUMBER
  */  
  public function logEmailRenew($loan_number, $bor_id, $result,
				$result_message, $new_due_date,
				$email_log_id) {
    
    if (!$this->dbConn) return false;

    $q = "INSERT INTO WTW.WTW_EMAIL_RENEW_LOG " .
       "(LOAN_NUMBER, BOR_ID, SENT_DATETIME, RESULT, " .
       "RESULT_MESSAGE, NEW_DUE_DATE, EMAIL_LOG_ID) " .
       "VALUES " .
       "(:LOAN_NUMBER, :BOR_ID, SYSDATE, :RESULT, " .
       ":RESULT_MESSAGE, TO_DATE(:NEW_DUE_DATE, 'YYYYMMDD'), " .
       ":EMAIL_LOG_ID)";

    $stmt = oci_parse($this->dbConn, $q);
    
    oci_bind_by_name($stmt, ':LOAN_NUMBER',
		     $loan_number, -1);
    oci_bind_by_name($stmt, ':BOR_ID',
		     $bor_id, -1);
    oci_bind_by_name($stmt, ':RESULT',
		     $result, -1);
    oci_bind_by_name($stmt, ':RESULT_MESSAGE',
		     $result_message, -1);
    oci_bind_by_name($stmt, ':NEW_DUE_DATE',
                     $new_due_date, -1);
    oci_bind_by_name($stmt, ':EMAIL_LOG_ID',
		     $email_log_id, -1);
    oci_execute($stmt);

    oci_free_statement($stmt);

  }

}


?>